package com.jv;

import org.apache.log4j.Logger;



public class JThreadGroup {

	private String name;

	private ControllableRunnable runnable;
	private Logger log = Logger.getLogger(this.getClass().getName());
	private boolean closed;

	private int count;

	private int currentcount;

	private void this_run() throws InterruptedException {
		while (!closed) {
			if (currentcount < count) {
				newThread();
			} else {
				synchronized (this) {
					this.wait(500);
				}
			}
		}
	}

	private void threadRunnable() {
		currentcount++;
		try {
			runnable.run();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			currentcount--;
		}
	}

	private void newThread() throws InterruptedException {
		log.info("newThread " + name);
		Thread t = new Thread(new Runnable() {

			public void run() {
				threadRunnable();
			}

		}, name + " count: " + currentcount);
		t.start();
		synchronized (this) {
			this.wait(500);
		}
	}

	public JThreadGroup(int count, ControllableRunnable runnable, String string) {
		this.runnable = runnable;
		this.name = string;
		this.count = count;
		//
		Thread t = new Thread(new Runnable() {
			public void run() {
				try {
					this_run();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}, "JThreadGroup " + name);
		t.start();
	}

	public void close() {
		closed = true;
	}

}
