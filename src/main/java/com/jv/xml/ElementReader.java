package com.jv.xml;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.util.Stack;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;

public class ElementReader {
	private Logger log = Logger.getLogger(this.getClass());
	private Stack<XMLElement> elements = new Stack<XMLElement>();
	private boolean valid = true;
	private SAXParserFactory fact;

	private boolean snapping;
	private String firstsnap_element;
	private boolean snapping_closed;

	public ElementReader(String schemaurl) throws SAXException, IOException {
		this(new URL(schemaurl));
	}

	public ElementReader(URL schemaurl) throws SAXException, IOException {
		this(schemaurl.openStream());
	}

	public ElementReader(InputStream schemastream) throws SAXException {
		if (schemastream == null) {
			log.error("schemastream null");
		} else {

			// schemastream = new VerboseInputStream(schemastream);

			fact = SAXParserFactory.newInstance();
			// fact.setValidating(true);
			SchemaFactory sfact = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = sfact.newSchema(new StreamSource(schemastream));
			fact.setSchema(schema);
			fact.setNamespaceAware(true);
			//
			clearElements();
		}
	}

	private void this_startElement(String localName, Attributes attributes) {
		log.debug("startElement " + localName);
		if (snapping) {
			if (firstsnap_element == null) {
				firstsnap_element = localName;
			}
		}
		//
		XMLElement e = getCurrentElement().newChild(localName);
		for (int i = 0; i < attributes.getLength(); i++) {
			String qname = attributes.getQName(i);
			String value = attributes.getValue(i);
			log.debug("attrib " + qname + " value:" + value + "   ce:" + e);
			e.addParameter(qname, value);
		}
		elements.push(e);
	}

	private XMLElement getCurrentElement() {
		return elements.lastElement();
	}

	private void this_endElement(String localName) {
		log.debug("endElement " + localName);
		// stops snapping if ending element is same as first element after start
		// of snapping
		if (snapping) {
			if (localName.equals(firstsnap_element)) {
				snapping_closed = true;
			}
		}
		elements.pop();
	}

	private void this_string(String string) {
		// log.debug("string length:" + string.length() + " \"" + string +
		// "\"");
		string = string.trim();
		if (string.length() > 0) {
			log.debug("string trimmed length:" + string.length() + " \"" + string + "\"");
		}
		if (string.length() > 0) {
			getCurrentElement().addText(string);
		}
	}

	public XMLElement parse(Reader r, boolean oneelement) {
		// log.info("parse " + r);
		try {
			//
			clearElements();
			//
			XMLReader xmlr = getXMLReader();
			Reader reader;
			if (oneelement) {
				reader = startSnapping(r);
			} else {
				snapping = false;
				reader = r;
			}
			InputSource inputSource;
			inputSource = new InputSource(reader);

			xmlr.parse(inputSource);
			if (valid) {
				return getCurrentElement().elements().iterator().next();
			} else {
				Logger.getLogger(this.getClass()).error("XML is not valid ");
				return null;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		return null;
	}

	// first element after snapping is started is marked as firstsnap_element
	// Reader will return -1 when firstsnap_element is ended (with
	// this_endelement).
	// Snapreader will read bytes to next '>' char and not further. This is to
	// make sure that reading won't be blocked by reading after the ending
	// element.
	private Reader startSnapping(Reader r) {
		Reader reader;
		snapping = true;
		firstsnap_element = null;
		snapping_closed = false;
		reader = new SnapReader(r);
		return reader;
	}

	private XMLReader getXMLReader() throws ParserConfigurationException, SAXException {
		javax.xml.parsers.SAXParser parser = fact.newSAXParser();
		XMLReader xmlr = parser.getXMLReader();
		MyErrorHandler ehandler = new MyErrorHandler();
		xmlr.setErrorHandler(ehandler);
		xmlr.setContentHandler(new MyHandler());
		return xmlr;
	}

	private void clearElements() {
		elements.clear();
		elements.push(new XMLElement("base"));
	}

	private class SnapReader extends Reader {
		private Reader r;

		public SnapReader(Reader r) {
			super(r);
			this.r = r;
		}

		
		public int read(char[] cbuf, int off, int len) throws IOException {
			log.debug("snap reading len:" + len + " off:" + off + " snapping:" + snapping + " snapclosed:"
					+ snapping_closed);
			int index = off;
			int count = 0;
			//
			if (snapping && snapping_closed) {
				log.info("snapping and snapping is closed... returning -1");
				return -1;
			}
			//
			while (index < cbuf.length) {
				if (snapping && snapping_closed) {
					break;
				}
				int b = r.read();
				// log.info("snapreader got byte:" + (char)b);
				if (b == -1)
					break;
				cbuf[index++] = (char) b;
				count++;
				if (b == '>') {
					break;
				}
			}
			if (count == 0) {
				return -1;
			}
			log.debug("snap read returning " + count + " asstring " + new String(cbuf, off, count));
			return count;
		}

		
		public void close() throws IOException {
			log.error("snapreader close");
		}
	}

	private class MyErrorHandler implements ErrorHandler {
		public void error(SAXParseException exception) throws SAXException {
			valid = false;
			log.error(exception);
			throw new RuntimeException("error " + exception);
		}

		public void fatalError(SAXParseException exception) throws SAXException {
			valid = false;
			log.error(exception);
			throw new RuntimeException("error " + exception);
		}

		public void warning(SAXParseException exception) throws SAXException {
			valid = false;
			log.error(exception);
			throw new RuntimeException("error " + exception);
		}
	}

	private class MyHandler extends DefaultHandler {
		
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			this_startElement(localName, attributes);
		}

		
		public void endElement(String uri, String localName, String qName) throws SAXException {
			this_endElement(localName);
		}

		
		public void characters(char[] ch, int start, int length) throws SAXException {
			this_string(new String(ch, start, length));
		}
	}
}
