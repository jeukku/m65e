package com.jv.xml;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import base64.Base64Coder;

public class XMLElement {
	private Map<String, String> ps = new HashMap<String, String>();
	private String data;
	private List<XMLElement> os = new ArrayList<XMLElement>();
	private int type = ELEMENT_TAG;
	private final static int ELEMENT_TAG = 1;
	private final static int ELEMENT_TEXT = 2;

	public XMLElement(String string) {
		data = string;
	}

	public void addBase64Text(String text) {
		XMLElement ro = new XMLElement(new String(Base64Coder.encode(text.getBytes())));
		ro.type = XMLElement.ELEMENT_TEXT;
		os.add(ro);
	}

	public void addText(String text) {
		XMLElement ro = new XMLElement(text);
		ro.type = XMLElement.ELEMENT_TEXT;
		os.add(ro);
	}

	public void addParameter(String string, String string2) {
		ps.put(string, string2);
	}

	StringBuffer print(StringBuffer sb) {
		if (type == ELEMENT_TAG) {
			sb.append("<" + data);
			Iterator ip = ps.keySet().iterator();
			while (ip.hasNext()) {
				String key = (String) ip.next();
				String value = (String) ps.get(key);
				sb.append(" " + key + "=\"" + value + "\"");
			}
			sb.append(">");
			printChildren(sb);
			sb.append("</" + data + ">");
		} else {
			sb.append(data);
		}
		return sb;
	}

	private StringBuffer printChildren(StringBuffer sb) {
		Iterator<XMLElement> io = os.iterator();
		while (io.hasNext()) {
			XMLElement ro = io.next();
			ro.print(sb);
		}
		return sb;
	}

	public void write(Writer w) throws IOException {
		String s = print(new StringBuffer()).toString();
		// Logger.getLogger(this.getClass()).info("write " + s);
		w.write(s);
	}

	public void writeChildren(Writer w) throws IOException {
		String s = printChildren(new StringBuffer()).toString();
		// Logger.getLogger(this.getClass()).info("writeCh " + s);
		w.write(s);
	}

	public List<XMLElement> elements() {
		return os;
	}

	public String getName() {
		return data;
	}

	public String toString() {
		if (type == ELEMENT_TAG) {
			return "tag:" + data;
		} else {
			return "text:" + data;
		}
	}

	public String getChildrenAsString() {
		if (type == ELEMENT_TAG) {
			return printChildren(new StringBuffer()).toString();
		} else {
			return data;
		}
	}

	public String getAsString() {
		return print(new StringBuffer()).toString();
	}

	public String getParam(String string) {
		return ps.get(string);
	}

	public XMLElement getChild(String string) {
		for (XMLElement e : elements()) {
			if (e.getName().equals(string))
				return e;
		}
		return null;
	}

	public XMLElement newChild(String string) {
		XMLElement e = new XMLElement(string);
		os.add(e);
		return e;
	}

	public boolean hasChildElements() {
		boolean b = false;
		for (XMLElement child : os) {
			if (child.type == XMLElement.ELEMENT_TAG) {
				b = true;
				break;
			}
		}
		return b;
	}
}
