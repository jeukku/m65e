package com.jv.swing;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.LayoutManager;

public class MaxWidthLayout implements LayoutManager {
	private GridBagLayout layout = new GridBagLayout();
	private int leftpadding;
	
	public void addLayoutComponent(String name, Component comp) {
		layout.addLayoutComponent(name, comp);
	}

	public void layoutContainer(Container parent) {
		int count = 0;
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.weightx = 1.0;
		Insets insets = new Insets(0,leftpadding,0,0);
		gbc.insets = insets;
		//
		for(Component c:parent.getComponents()) {
			gbc.gridy = count++;
			layout.setConstraints(c, gbc);			
		}
		layout.layoutContainer(parent);
	}

	public Dimension minimumLayoutSize(Container parent) {
		return layout.maximumLayoutSize(parent);
	}

	public Dimension preferredLayoutSize(Container parent) {
		return layout.preferredLayoutSize(parent);
	}

	public void removeLayoutComponent(Component comp) {
		layout.removeLayoutComponent(comp);
	}

	public void setLeftPadding(int i) {
		leftpadding = i;
	}
}
