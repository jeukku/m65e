package com.jv.io;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class PacketInputStream extends DataInputStream {
	public PacketInputStream(InputStream is) {
		super(is);
	}

	public int readPacketLength() throws IOException {
		return readInt();
	}

	public void endPacket() throws IOException {
		read(); // check
	}

	public String readString() throws IOException {
		int length = readShort();
		byte bs[] = new byte[length];
		read(bs);
		return new String(bs);
	}

	public String readID() throws IOException {
		byte bs[] = new byte[4];
		read(bs);
		return new String(bs);
	}
}
