package com.jv.io;

import java.io.IOException;
import java.io.OutputStream;

import org.apache.log4j.Logger;

/*******************************************************************************
 * File: VerboseOutputStream.java created Oct 25, 2002 12:57:21 PM by vilmunju
 */
public class VerboseOutputStream extends OutputStream {
	OutputStream os;

	private Logger logger = Logger.getLogger(this.getClass());

	public void write(int b) throws IOException {
		// if(b=='\r') com.jv.GOutput.print("#");
		// if(b=='\n') com.jv.GOutput.print("%");
		logger.debug("vbos " + ByteBuffer.byteInfo(b));
		os.write(b);
	}

	public VerboseOutputStream(OutputStream nos) {
		os = nos;
	}
}
