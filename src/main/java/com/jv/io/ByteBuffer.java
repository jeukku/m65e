/*
 * Created on 30.10.2004
 * by Juuso
 */
package com.jv.io;

import java.io.IOException;

import org.apache.log4j.Logger;

/**
 * @author juuso
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class ByteBuffer {
	private short bs[] = new short[1000];

	private int iread;

	private int iwrite;

	private boolean closed;

	// private OutputStream bos;
	// private InputStream bis;
	private String name;

	final private static boolean debug = true;

	private Logger log = Logger.getLogger(this.getClass().getName());

	public void close() {
		synchronized (this) {
			closed = true;
			notify();
		}
	}

	public ByteBuffer() {
		name = "DefaultName";
	}

	public ByteBuffer(String nname) {
		name = nname;
	}

	synchronized public int available() throws IOException {
		if (closed)
			return 0;
		//
		int a;
		if (iwrite >= iread) {
			a = iwrite - iread;
		} else {
			a = iwrite + (bs.length - iread); // TODO?
		}
		if (closed && a == 0)
			throw new IOException("ByteBuffer closed");
		return a;
	}

	public void write(int b) throws IOException {
		if (closed) {
			throw new IOException("ByteBuffer closed");
		}
		b = b & 0xff;
		if (debug)
			log.debug(this + "W" + byteInfo(b) + " avail:" + available());
		synchronized (bs) {
			bs[iwrite++] = (byte) b;
			if (iwrite >= bs.length)
				iwrite = 0;
			if (iread == iwrite)
				grow();
			bs.notify();
		}
	}

	public synchronized void write(byte bs[]) throws IOException {
		for (int i = 0; i < bs.length; i++)
			write(bs[i]);
	}

	public int read() throws IOException {
		while (available() == 0) {
			if (closed)
				return -1;
			log.debug(this + " read wait " + iread);
			synchronized (bs) {
				try {
					bs.wait(100);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		int b = bs[iread++] & 0xff;
		if (debug) {
			log.debug(this + "R" + byteInfo(b) + " avail:" + available());
		}
		//
		if (iread >= bs.length)
			iread = 0;
		return b;
	}

	static public String byteInfo(int b) {
		String info;
		if (b == '\n') {
			info = "newline";
		} else if (b == '\r') {
			info = "return";
		} else if (Character.isDefined((char) b)) {
			info = "" + (char) b;
		} else
			info = "d" + b;
		return "(" + b + ")[" + info + "]";
	}

	// private int bufferread() throws IOException {
	// return read();
	// }
	//
	// private void bufferwrite(int c) throws IOException {
	// write(c);
	// }
	private void grow() {
		int ns = (int) (bs.length * 2f);
		if (ns == bs.length)
			ns += 10;
		log.debug("growing buffer " + bs.length + " -> " + ns);
		grow(ns);
	}

	private synchronized void grow(int nsize) {
		short nbs[] = new short[nsize];
		int index = iread;
		int nindex = 0;
		if (bs.length > 0) {
			if (iread == iwrite) {
				while (index < bs.length)
					nbs[nindex++] = bs[index++];
				index = 0;
				while (index < iwrite)
					nbs[nindex++] = bs[index++];
			} else
				throw new RuntimeException("iread!=iwrite en tie mtit? teh?");
		}
		// for(int i=0; i<nindex; i++) System.out.println("bs " + nbs[i]);
		iread = 0;
		iwrite = nindex;
		bs = nbs;
	}

	// public synchronized OutputStream getOutputStream() {
	// if (bos == null) {
	// bos = new ThisOutputStream();
	// }
	// return bos;
	// }
	//
	// public InputStream getInputStream() {
	// if (bis == null) {
	// bis = new ThisInputStream();
	// }
	// return bis;
	// }
	// private class ThisInputStream extends InputStream {
	// public int read() throws IOException {
	// return bufferread();
	// }
	//
	// public int available() throws IOException {
	// // log.errorln("avail closed? " + closed );
	// return this_available();
	// }
	//
	// public int read(byte[] cbuf, int off, int len) throws IOException {
	// int count = 0;
	// for (int i = 0; i < len; i++) {
	// cbuf[off + i] = (byte) read();
	// count++;
	// }
	// return count;
	// }
	//
	// public void close() {
	// // TODO
	// log.errorln("TODO close");
	// }
	// }
	// private class ThisOutputStream extends OutputStream {
	// public void write(int c) throws IOException {
	// bufferwrite(c);
	// }
	//
	// public void write(byte[] cbuf, int off, int len) throws IOException {
	// for (int i = 0; i < len; i++)
	// write(cbuf[off + i]);
	// }
	//
	// public void close() {
	// // TODO
	// // GOutput.println(getClass().getName() + " close");
	// }
	//
	// public void flush() {
	// try {
	// log.errorln("BB.ThisOS flush avail:" + available());
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }
	// }
	public String toString() {
		try {
			return "BB[" + name + "]a:" + available();
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return "ByteBuffer[" + name + "] a:" + ioe;
		}
	}

	// private int this_available() throws IOException {
	// return available();
	// }
	public String content() {
		StringBuffer s = new StringBuffer();
		for (int i = 0; i < bs.length; i++) {
			if (i == iread)
				s.append("r");
			if (i == iwrite)
				s.append("w");
			s.append(Integer.toHexString(bs[i]));
			if (i == iread)
				s.append("r");
			if (i == iwrite)
				s.append("w");
		}
		return s.toString();
	}
}