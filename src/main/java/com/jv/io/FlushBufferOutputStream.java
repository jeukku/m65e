package com.jv.io;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class FlushBufferOutputStream extends OutputStream {

	private static final long WAITTIME = 20;

	private OutputStream os;

	private boolean closed;

	private long lastwrite;

	public FlushBufferOutputStream(OutputStream outputStream, String name) {
		this.os = new BufferedOutputStream(outputStream);
		Thread t = new Thread(new Runnable() {
			public void run() {
				this_run();
			}
		}, "stream " + getClass().getName() + " name:" + name);
		t.start();
	}

	private void this_run() {
		try {
			while (!closed) {
				synchronized (os) {
					os.wait(WAITTIME);
				}
				if (System.currentTimeMillis() - lastwrite > WAITTIME) {
					os.flush();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	public void close() throws IOException {
		closed = true;
		os.close();
	}

	
	public void write(int b) throws IOException {
		lastwrite = System.currentTimeMillis();
		os.write(b);
	}

}
