package com.jv.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class StreamsConnector {
	public StreamsConnector(InputStream inputStream, OutputStream outputStream,
			InputStream inputStream2, OutputStream outputStream2) {
		StreamConnector sc1 = new StreamConnector(inputStream, outputStream2);
		StreamConnector sc2 = new StreamConnector(inputStream2, outputStream);
		sc1.start();
		sc2.start();
	}

	public static class StreamConnector extends Thread {
		private InputStream is;
		private OutputStream os;

		public StreamConnector(InputStream is2, OutputStream os1) {
			this.is = is2;
			this.os = os1;
			setName("streamConnector " + this);
		}

		
		public void run() {
			try {
				while (true) {
					int b;
					b = is.read();
					if (b < 0) {
						os.close();
						break;
					}
					os.write(b);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
