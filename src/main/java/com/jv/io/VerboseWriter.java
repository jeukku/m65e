/*
 * Created on 4.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.jv.io;

import java.io.IOException;
import java.io.Writer;

import org.apache.log4j.Logger;

/**
 * @author juuso
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class VerboseWriter extends Writer {
	private Writer w;

	private Logger logger = Logger.getLogger(this.getClass());

	/**
	 * 
	 */
	public VerboseWriter(Writer nw) {
		w = nw;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.io.Writer#write(char[], int, int)
	 */
	public void write(char[] arg0, int arg1, int arg2) throws IOException {
		logger.debug("Vwrite " + arg0.toString());
		w.write(arg0, arg1, arg2);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.io.Writer#flush()
	 */
	public void flush() throws IOException {
		w.flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.io.Writer#close()
	 */
	public void close() throws IOException {
		w.close();
	}
}
