/*
 * Created on Dec 14, 2004
 * by Juuso
 */
package com.jv.io.query;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

/**
 * @author juuso
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class Query {
	private static final long serialVersionUID = 1L;

	private String name;

	private Hashtable params = new Hashtable();

	public Query(String n) {
		name = n;
	}

	public String getParam(String name) {
		return (String) params.get(name);
	}

	public void setParam(String name, Object value) {
		params.put(name, value);
	}

	public String getName() {
		return name;
	}

	// public void setName(String name) {
	// this.name = name;
	// }
	public Iterator getParamNames() {
		return params.keySet().iterator();
	}

	public void setParams(Hashtable params) {
		this.params = params;
	}

	/*
	 * (non-Javadoc)s.g
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		String s = "Query " + name;
		Enumeration keys = params.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			Object value = params.get(key);
			s += " " + key + "=" + value;
		}
		return s;
	}
}