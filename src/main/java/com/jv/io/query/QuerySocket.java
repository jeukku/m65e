/*
 * Created on Dec 18, 2004 by Juuso
 */
package com.jv.io.query;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Iterator;

import org.apache.log4j.Logger;

import com.jv.io.LineReader;

public class QuerySocket {
	private InputStream is;

	private OutputStream os;

	//
	private static final String BGNQ = "BGNQ";

	private static final String ENDQ = "ENDQ";

	private Logger log = Logger.getLogger(this.getClass());

	public boolean available() throws IOException {
		// log.errorln(is + " avail " + is.available());
		return is.available() > 0;
	}

	public synchronized void write(Query q) {
		try {
			writeln(BGNQ);
			writeln(q.getName());
			Iterator i = q.getParamNames();
			while (i.hasNext()) {
				String param = (String) i.next();
				String value = q.getParam(param);
				writeln(param);
				writeln(value);
			}
			writeln(ENDQ);
			os.flush();
			log.debug("QS wrote " + q);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	private void writeln(String s) throws IOException {
		os.write(s.getBytes());
		os.write("\n".getBytes());
	}

	public Query read() {
		try {
			LineReader lr = new LineReader(is);
			String line = "";
			while (!line.equals(BGNQ)) {
				line = lr.readLine();
				// Stream is closed
				if (line == null) {
					return null;
				}
			}
			String name = lr.readLine();
			Query q = new Query(name);
			line = lr.readLine();
			while (!line.equals(ENDQ)) {
				String param = line;
				String value = lr.readLine();
				q.setParam(param, value);
				line = lr.readLine();
			}
			log.debug("QS read " + q);
			return q;
		} catch (IOException ioe) {
			ioe.printStackTrace();
			return null;
		}
	}

	public QuerySocket(String name, final InputStream is, final OutputStream os) {
		log.debug("QuerySocket cons " + name);
		this.is = is;
		this.os = os;
	}
}