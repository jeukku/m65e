package com.jv.io;

import java.io.IOException;
import java.io.InputStream;

public class StringStreamCompare {
	private InputStream is;

	private StringBuffer sb = new StringBuffer();

	public StringStreamCompare(InputStream stream) {
		is = stream;
	}

	public boolean compare(String string) throws IOException {
		for (int i = 0; i < string.length(); i++) {
			int b = is.read();
			sb.append((char) b);
			if (b != string.charAt(i))
				return false;
		}
		return true;
	}

	
	public String toString() {
		return getClass().getName() + " compared buffer " + sb;
	}
}
