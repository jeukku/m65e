package com.jv.io;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class PacketOutputStream {
	ByteArrayOutputStream baos = new ByteArrayOutputStream();

	DataOutputStream pdos = new DataOutputStream(baos);

	String id;

	DataOutputStream os;

	public PacketOutputStream(OutputStream os) {
		this.os = new DataOutputStream(os);
	}

	public void close() throws IOException {
		os.close();
	}

	public void writeCount(int count) throws IOException {
		pdos.writeInt(count);
	}

	public void writeByte(byte b) throws IOException {
		pdos.writeByte(b);
	}

	public void writeShort(short s) throws IOException {
		pdos.writeShort(s);
	}

	public void writeString(String s) throws IOException {
		pdos.writeShort(s.length());
		pdos.writeBytes(s);
	}

	public void startPacket(String id) throws IOException {
		baos = new ByteArrayOutputStream();
		pdos = new DataOutputStream(baos);
		this.id = id;
	}

	public void endPacket() throws IOException {
		byte bs[] = new byte[4];
		bs[0] = (byte) id.charAt(0);
		bs[1] = (byte) id.charAt(1);
		bs[2] = (byte) id.charAt(2);
		bs[3] = (byte) id.charAt(3);
		os.write(bs);
		os.writeInt(baos.size());
		os.write(baos.toByteArray());
		pdos.writeByte(0);
	}

	public void writePacket(String id, int content[]) throws IOException {
		startPacket(id);
		for (int i = 0; i < content.length; i++)
			pdos.write(content[i]);
		endPacket();
	}
}
