/*
 * Created on 5.4.2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.jv.io;

import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;

/**
 * @author juuso
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class LineReader {
	private InputStream is;

	private Logger log = Logger.getLogger(this.getClass().getName());

	public LineReader(InputStream is) {
		this.is = is;
	}

	public String readLine() throws IOException {
		if (is == null)
			return null;
		StringBuffer sb = new StringBuffer();
		while (true) {
			int b = is.read();
			if (b == -1) {
				log.debug("readline got -1 ");
				log.debug("is avail " + is.available());
				if (sb.length() == 0)
					return null;
				break;
			}
			// log.errorln("readL " + b + "("+(char)b+")");
			if (b == '\r') {
				// just ignore 
			}else if (b == '\n')
				break;
			else
				sb.append((char) b);
		}
		log.debug("LINE: " + sb);
		return sb.toString();
	}
}
