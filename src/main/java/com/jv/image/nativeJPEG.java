package com.jv.image;

public class nativeJPEG {
	public native void write(byte bs[], int length, int components);

	public native void write();

	static {
		System.loadLibrary("jpeg");
	}

	public static void main(String args[]) {
		nativeJPEG njpeg = new nativeJPEG();
		njpeg.write();
		// njpeg.write(bs, 10, 3);
	}
}
