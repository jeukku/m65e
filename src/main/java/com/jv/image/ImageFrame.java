package com.jv.image;

import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.WindowEvent;

public class ImageFrame extends Frame {
	private static final long serialVersionUID = 1L;

	Image img;

	ImageFrame(int pix[], int w, int h) {
		java.awt.image.MemoryImageSource source = new java.awt.image.MemoryImageSource(
				w, h, pix, 0, w);
		img = createImage(source);
		try {
			jbInit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void paint(Graphics g) {
		// g.drawImage(img, getSize().width/10, getSize().height/10,
		// getSize().width*8/10, getSize().height*8/10, this);
		g.drawImage(img, 0, 0, this);
	}

	public ImageFrame() {
		try {
			jbInit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void jbInit() throws Exception {
		setSize(256, 256);
		this.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				this_windowClosing(e);
			}
		});
	}

	void this_windowClosing(WindowEvent e) {
		dispose();
	}
}
