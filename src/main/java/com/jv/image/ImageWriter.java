package com.jv.image;

public interface ImageWriter {
	void write(String filename) throws java.io.IOException;

	void write(java.io.OutputStream os) throws java.io.IOException;
}
