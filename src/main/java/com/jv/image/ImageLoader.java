package com.jv.image;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.PixelGrabber;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


public class ImageLoader {
	public Image getImage(InputStream is) throws IOException {
		if (is == null)
			return null;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int b = is.read();
		while (b != -1) {
			baos.write(b);
			b = is.read();
		}
		
		baos.close();
		byte bs[] = baos.toByteArray();
		return Toolkit.getDefaultToolkit().createImage(bs);

		
		// if(filename.endsWith("jpg") ||
		// filename.endsWith("jpeg") ||
		// filename.endsWith("gif")) {
		// }
		// else if(filename.endsWith("png")) {
		// try {
		// PNG png = new PNG();
		// return png.getImage(new ByteArrayInputStream(bs));
		// } catch(java.io.IOException ioe) {
		// ioe.printStackTrace();
		// }
		// }
		//
		// return null;
	}

	public Image getImage(String filename, InputStream is) throws IOException {
		return getImage(is);
	}

	public Image getImage(String filename) {
		try {
			return getImage(filename, new FileInputStream(filename));
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		return null;
	}

	public int[] getPix(Image img) {
		while (img.getWidth(null) < 0 || img.getHeight(null) < 0) {
			synchronized (img) {
				try {
					img.wait(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		int w = img.getWidth(null);
		int h = img.getHeight(null);
		int pix[] = new int[w * h];
		PixelGrabber pg = new PixelGrabber(img, 0, 0, w, h, pix, 0, w);
		try {
			pg.grabPixels();
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}
		return pix;
	}

	public int[] getPix(String filename) {
		Image img = getImage(filename);
		return getPix(img);
	}
}
