package com.jv.image;

public interface ImageReader extends java.awt.image.ImageProducer {
	public java.awt.Image getImage(String filename) throws java.io.IOException;

	public java.awt.Image getImage(java.io.InputStream is)
			throws java.io.IOException;

	public int[] getTrueColorPixels(java.io.InputStream is)
			throws java.io.IOException;
}

