/*
 * Created on 11.6.2005
 */
package com.jv.pixelgr;

import java.awt.Canvas;
import java.awt.Graphics;

public class UpdatebleCanvas extends Canvas {
	private static final long serialVersionUID = 1L;

	private Updater upd;

	public UpdatebleCanvas(Updater updater) {
		upd = updater;
	}

	public void update(Graphics g) {
		upd.update(g);
	}

	public static interface Updater {
		void update(Graphics g);
	}
}
