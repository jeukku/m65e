/*
 * Created on 11.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.jv.pixelgr;

/**
 * @author juuso
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class SBuffer {
	private int w, h;

	private int bgcolor = 0xff000000;

	boolean smooth = false;

	public int pixels[];

	public int getWidth() {
		return w;
	}

	public int getHeight() {
		return h;
	}

	public int[] getPixels() {
		return pixels;
	}

	public void setPixel(int x, int y, int color) {
		if (x < w && x >= 0 && y < h && y >= 0) {
			int index = y * w + x;
			pixels[index] = color;
		}
	}

	final public int get(int index) {
		return pixels[index];
	}

	final public void set(int index, int c) {
		pixels[index] = c;
	}

	public void setBackground(int color) {
		bgcolor = color;
	}

	public void clear() {
		fill(0, pixels.length, bgcolor);
	}

	public void fadeBuffer(int c) {
		int cr = (c & 0x00ff0000) >> 16;
		int cg = (c & 0x0000ff00) >> 8;
		int cb = (c & 0x000000ff);
		int bgcr = (this.bgcolor & 0x00ff0000) >> 16;
		int bgcg = (this.bgcolor & 0x0000ff00) >> 8;
		int bgcb = (this.bgcolor & 0x000000ff);
		for (int l = 0; l < pixels.length; l++) {
			int pixel = pixels[l];
			int r = (pixel & 0x00ff0000) >> 16;
			int g = (pixel & 0x0000ff00) >> 8;
			int b = (pixel & 0x000000ff);
			if (r < bgcr) {
				r += cr;
				if (r > bgcr)
					r = bgcr;
			} else {
				r -= cr;
				if (r < bgcr)
					r = bgcr;
			}
			if (g < bgcg) {
				g += cg;
				if (g > bgcg)
					g = bgcg;
			} else {
				g -= cg;
				if (g < bgcg)
					g = bgcg;
			}
			if (b < bgcb) {
				b += cb;
				if (b > bgcb)
					b = bgcb;
			} else {
				b -= cb;
				if (b < bgcb)
					b = bgcb;
			}
			pixels[l] = 0xff << 24 | r << 16 | g << 8 | b;
		}
	}

	public void fill(int index, int l, int c) {
		if (pixels.length == 0)
			return;
		int cleared = 1;
		pixels[index++] = c;
		while (cleared < l) {
			System.arraycopy(pixels, 0, pixels, index, cleared);
			l -= cleared;
			index += cleared;
			cleared <<= 1;
		}
		System.arraycopy(pixels, 0, pixels, index, l - 1);
	}

	public boolean setSize(int nw, int nh) {
		if (w != nw || h != nh || pixels == null) {
			System.out.println(getClass().getName() + " setting size " + nw
					+ "x" + nh + " old size=" + w + "x" + h);
			w = nw;
			h = nh;
			pixels = null;
			System.gc();
			try {
				Thread.sleep(20);
			} catch (java.lang.InterruptedException e) {
				e.printStackTrace();
			}
			pixels = new int[w * h];
			return true;
		}
		return false;
	}

	public int getBackground() {
		return bgcolor;
	}
}
