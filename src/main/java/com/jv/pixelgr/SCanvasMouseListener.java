/*
 * Created on 21.10.2004
 * by Juuso
 */
package com.jv.pixelgr;

/**
 * @author juuso
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public interface SCanvasMouseListener {
	void mouseMoved(float x, float y);

	void mousePressed(float x, float y);

	void mouseReleased(float x, float y);
}
