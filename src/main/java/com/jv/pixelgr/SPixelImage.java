/*
 * Created on 11.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.jv.pixelgr;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;

import org.apache.log4j.Logger;

/**
 * @author juuso
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class SPixelImage {
	private SBuffer buffer = new SBuffer();

	private SoftImageProducer prod;

	private Canvas canvas;

	private Image img;

	private Logger log = Logger.getLogger(this.getClass());

	/**
	 * 
	 */
	public SPixelImage() {
		buffer.setBackground(0xff808080);
	}

	public boolean setSize(int w, int h) {
		if (w < 1)
			w = 1;
		if (h < 1)
			h = 1;
		if (buffer.setSize(w, h)) {
			System.out.println("SRenderT w" + w + " H" + h);
			ColorModel cm;
			if (canvas != null) {
				cm = canvas.getColorModel();
				log.debug("component cm " + cm);
			} else {
				// ColorModel cm = Toolkit.getDefaultToolkit().getColorModel();
				cm = new DirectColorModel(24, 0xFF0000, 0xFF00, 0xFF);
			}
			log.debug("" + cm);
			prod = new SoftImageProducer(w, h, cm, buffer.getPixels());
			if (canvas == null) {
				img = Toolkit.getDefaultToolkit().createImage(prod);
			} else {
				img = canvas.createImage(prod);
			}
			return true;
		}
		return false;
	}

	public int[] getPixels() {
		return buffer.getPixels();
	}

	public Image getImage() {
		prod.update();
		return img;
	}

	public int getWidth() {
		return buffer.getWidth();
	}

	public int getHeight() {
		return buffer.getHeight();
	}

	public void clear() {
		buffer.clear();
	}

	public void setSize(Dimension size) {
		setSize(size.width, size.height);
	}

	public void setComponent(Canvas ncomp) {
		canvas = ncomp;
	}
}