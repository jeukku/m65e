/*
 * Created on 21.10.2004
 * by Juuso
 */
package com.jv.pixelgr;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Vector;

/**
 * @author juuso
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class MouseEventAdapter {
	private Component comp;

	private Vector mlis = new Vector();

	public MouseEventAdapter(Component c) {
		comp = c;
	}

	public void addMouseListener(SCanvasMouseListener l) {
		mlis.addElement(l);
	}

	public MouseListener getMouseListener() {
		return new MouseListener() {
			public void mouseClicked(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			public void mousePressed(MouseEvent arg0) {
				for (int i = 0; i < mlis.size(); i++) {
					SCanvasMouseListener l = (SCanvasMouseListener) mlis
							.elementAt(i);
					l.mousePressed(getX(arg0.getX()), getY(arg0.getY()));
				}
			}

			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}
		};
	}

	public MouseMotionListener getMouseMotionListener() {
		return new MouseMotionListener() {
			public void mouseDragged(MouseEvent arg0) {
				// TODO Auto-generated method stub
			}

			public void mouseMoved(MouseEvent arg0) {
				for (int i = 0; i < mlis.size(); i++) {
					SCanvasMouseListener l = (SCanvasMouseListener) mlis
							.elementAt(i);
					l.mouseMoved(getX(arg0.getX()), getY(arg0.getY()));
				}
			}
		};
	}

	private float getX(int x) {
		return (float) x / comp.getWidth() * 2 - 1;
	}

	private float getY(int y) {
		return (float) y / comp.getHeight() * 2 - 1;
	}
}
