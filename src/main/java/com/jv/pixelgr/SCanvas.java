/*
 * Created on 11.6.2005
 */
package com.jv.pixelgr;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class SCanvas {
	private static final long serialVersionUID = 1L;

	//
	private boolean repaint_lock = false, update_lock = false;

	private Object draw_lock = new Object();

	//
	// private Image img;
	private boolean stopped;

	private SPixelImage pimg;

	private UpdatebleCanvas canvas;

	private MouseEventAdapter vmh;

	//
	private Image currentimg;

	private boolean resized;

	public SCanvas(SPixelImage npimg) {
		pimg = npimg;
		canvas = new UpdatebleCanvas(new UpdatebleCanvas.Updater() {
			public void update(Graphics g) {
				this_update(g);
			}
		});
		pimg.setComponent(canvas);
		vmh = new MouseEventAdapter(canvas);
		canvas.addMouseListener(vmh.getMouseListener());
		canvas.addMouseMotionListener(vmh.getMouseMotionListener());
		pimg.setSize(canvas.getSize());
		canvas.addComponentListener(new ComponentAdapter() {
			public void componentResized(ComponentEvent e) {
				this_resize();
			}
		});
	}

	public void startFrame() {
		pimg.setComponent(canvas);
		pimg.setSize(canvas.getSize());
	}

	private void this_resize() {
		resized = true;
	}

	private void this_update(Graphics g) {
		if (!update_lock) {
			update_lock = true;
			synchronized (draw_lock) {
				if (repaint_lock && !resized) {
					g.drawImage(currentimg, 0, 0, canvas.getWidth(), canvas
							.getHeight(), null);
					Toolkit.getDefaultToolkit().sync();
					draw_lock.notifyAll();
				} else {
					System.out.println("SCanvas update repaint_lock "
							+ repaint_lock + " resized:" + resized);
				}
				repaint_lock = false;
			}
			update_lock = false;
		} else {
			throw new RuntimeException("update called when update_lock was on");
		}
	}

	public void paint() {
		if (!repaint_lock && !stopped) {
			synchronized (draw_lock) {
				if (resized) {
					currentimg = null;
					resized = false;
				} else {
					//
					repaint_lock = true;
					Image img = pimg.getImage();
					// if (currentimg == null)
					canvas.repaint();
					currentimg = img;
					try {
						draw_lock.wait(5000);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			System.out
					.println("SCanvas.paint ERNO!!! repaint_lock true!!! waitin 1sec");
			synchronized (draw_lock) {
				try {
					draw_lock.wait(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				repaint_lock = false;
			}
		}
		Thread.yield();
	}

	public void stop() {
		synchronized (draw_lock) {
			draw_lock.notifyAll();
		}
	}

	public Dimension minimumSize() {
		return new Dimension(20, 20);
	}

	public Dimension getMinimumSize() {
		return minimumSize();
	}

	public Dimension preferredSize() {
		return new Dimension(320, 200);
	}

	public Dimension getPreferredSize() {
		return preferredSize();
	}

	public boolean isVisible() {
		return canvas.isVisible();
	}

	public Dimension getSize() {
		return canvas.getSize();
	}

	public void setBackground(Color black) {
		canvas.setBackground(black);
	}

	public void addTo(Container cont) {
		cont.add(canvas);
	}

	public void addMouseListener(SCanvasMouseListener l) {
		vmh.addMouseListener(l);
	}
}