/*
 * Created on 11.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.jv.pixelgr;

import java.awt.image.ColorModel;
import java.awt.image.ImageConsumer;
import java.awt.image.ImageProducer;

/**
 * @author juuso
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class SoftImageProducer implements ImageProducer {
	private int w, h;

	private int[] pixel;

	private int hints;

	private ImageConsumer consumer;

	private ColorModel cm;

	public SoftImageProducer(int w, int h, ColorModel cm, int pixel[]) {
		this.w = w;
		this.h = h;
		this.cm = cm;
		this.pixel = pixel;
		hints = ImageConsumer.TOPDOWNLEFTRIGHT
				| ImageConsumer.COMPLETESCANLINES | ImageConsumer.SINGLEPASS
				| ImageConsumer.SINGLEFRAME;
	}

	public synchronized void addConsumer(ImageConsumer consumer1) {
		this.consumer = consumer1;
	}

	public final void startProduction(ImageConsumer imageconsumer) {
		if (imageconsumer == null)
			return;
		if (consumer != imageconsumer) {
			consumer = imageconsumer;
			consumer.setDimensions(w, h);
			consumer.setProperties(null);
			consumer.setColorModel(cm);
			consumer.setHints(hints);
		}
		consumer.setPixels(0, 0, w, h, cm, pixel, 0, w);
		consumer.imageComplete(ImageConsumer.SINGLEFRAMEDONE);
	}

	public void update() {
		if (consumer != null)
			startProduction(consumer);
	}

	public final boolean isConsumer(ImageConsumer imageconsumer) {
		return consumer == imageconsumer;
	}

	public final void requestTopDownLeftRightResend(ImageConsumer imageconsumer) {
		// ei tarvita
	}

	public final void removeConsumer(ImageConsumer imageconsumer) {
		// ei tarvita
	}
}
