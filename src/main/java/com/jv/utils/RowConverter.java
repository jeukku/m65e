package com.jv.utils;

public interface RowConverter {
	Object getCol(Object row, String colname);

	boolean isRow(Object row);
}
