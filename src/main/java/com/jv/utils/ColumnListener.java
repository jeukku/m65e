package com.jv.utils;

public interface ColumnListener {
	void columnNameChanged(StringColumn stringColumn);
}
