package com.jv.utils;

import java.io.File;

public class MFile extends File {
	private java.util.Vector subfiles;

	public MFile(String s) {
		super(s);
	}

	public int getSubFileCount() {
		if (subfiles == null)
			createSubFiles();
		if (subfiles == null)
			return 0;
		return subfiles.size();
	}

	public MFile getSubFile(int i) {
		if (subfiles == null)
			createSubFiles();
		if (subfiles == null)
			return null;
		return (MFile) subfiles.elementAt(i);
	}

	private void createSubFiles() {
		String list[] = list();
		if (list != null) {
			subfiles = new java.util.Vector();
			for (int i = 0; i < list.length; i++) {
				String npath = getPath() + File.separator + list[i];
				MFile subfile = new MFile(npath);
				subfiles.addElement(subfile);
			}
		}
	}
}
