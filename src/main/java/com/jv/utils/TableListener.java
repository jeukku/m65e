package com.jv.utils;

public interface TableListener {
	void rowRemoved(URow o);
	void rowAdded(URow row);
}
