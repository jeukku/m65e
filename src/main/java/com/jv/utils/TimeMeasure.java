/*
 * Created on Apr 11, 2003
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code Template
 */
package com.jv.utils;

import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * @author juuso
 */
public class TimeMeasure {
	private Hashtable ht = new Hashtable();

	private Block totalblock = new Block();

	private String name;

	private long lastPrint, printDelay = 15000;

	private int totalcount;

	private int hz_dt[] = new int[100];

	private int hz_idt = 0;

	private float hz;

	PrintStream out = System.out;

	private int depth;

	public TimeMeasure(String name) {
		this.name = name;
	}

	public void setTotalStartTime() {
		totalblock.st = System.currentTimeMillis();
		depth++;
	}

	public void setTotalEndTime() {
		totalblock.et = System.currentTimeMillis();
		int totaldt = (int) (totalblock.et - totalblock.st);
		totalblock.total += totaldt;
		totalcount++;
		//
		hz_dt[hz_idt++] = totaldt;
		if (hz_idt == hz_dt.length)
			hz_idt = 0;
		float fdt = 0;
		for (int i = 0; i < hz_dt.length; i++)
			fdt += hz_dt[i];
		hz = 1.0f / (fdt / hz_dt.length / 1000.0f);
		// if(hz>2000) {
		// }
		// System.out.println(name + " hfeds " + (fdt / hz_dt.length) + " depth
		// " + depth + " hz:"+hz);
		//
		long dt = System.currentTimeMillis() - lastPrint;
		if (dt > printDelay) {
			out.println(toString());
			lastPrint = System.currentTimeMillis();
			reset();
		}
		//
		depth--;
	}

	public void setStartTime(Object o) {
		if (ht.get(o) == null)
			ht.put(o, new Block());
		((Block) ht.get(o)).st = System.currentTimeMillis();
	}

	public void setEndTime(Object o) {
		if (ht.get(o) == null)
			return;
		Block b = (Block) ht.get(o);
		b.et = System.currentTimeMillis();
		b.total += b.et - b.st;
	}

	void reset() {
		totalblock.st = 0;
		totalblock.et = 0;
		totalblock.total = 0;
		totalcount = 0;
		Enumeration keys = ht.keys();
		while (keys.hasMoreElements()) {
			Block b = (Block) ht.get(keys.nextElement());
			b.st = 0;
			b.et = 0;
			b.total = 0;
		}
	}

	long getTotal() {
		long total = 0;
		Enumeration keys = ht.keys();
		while (keys.hasMoreElements()) {
			Block b = (Block) ht.get(keys.nextElement());
			total += b.total;
		}
		return total;
	}

	public String toString() {
		long total = totalblock.total;
		if (total == 0)
			return "total 0";
		String s = "TM - " + name + " total:" + total + " hz:" + hz + "\n";
		Enumeration keys = ht.keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			Block b = (Block) ht.get(key);
			s += key + "[" + b.total + "(" + (100 * b.total / total) + "%)]\n";
		}
		return s;
	}

	class Block {
		long st, et, total;
	}

	public float getHz() {
		return hz;
	}
}
