package com.jv.utils;

import org.apache.log4j.Logger;

public class LogErrorHandler implements ErrorHandler {
	private Logger log = Logger.getLogger(this.getClass());
	public void error(Exception e) {
		log.error(e);
	}
	
}
