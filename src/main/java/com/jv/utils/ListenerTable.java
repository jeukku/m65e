package com.jv.utils;

import java.util.ArrayList;
import java.util.List;

public class ListenerTable implements UTable {
	private ArrayList<UColumn> cols = new ArrayList<UColumn>();
	private List<URow> rows = new ArrayList<URow>();
	private List<TableListener> listeners;

	//

	public List<URow> getRows() {
		return rows;
	}

	public List<UColumn> getColumns() {
		return cols;
	}

	public UColumn addColumn(UColumn column) {
		cols.add(column);
		return column;
	}

	public UColumn addColumn(String colname) {
		UColumn col = new StringColumn(colname);
		return addColumn(col);
	}

	public URow addRow(URow row) {
		rows.add(row);
		if (listeners != null) {
			for (TableListener l : listeners) {
				l.rowAdded(row);
			}
		}
		return row;
	}

	public void removeRow(URow o) {
		rows.remove(o);
		fireRowRemoved(o);
	}

	public int getRowCount() {
		return rows.size();
	}

	public boolean contains(Object o) {
		return rows.contains(o);
	}

	public int indexOfRow(Object o) {
		return rows.indexOf(o);
	}

	private void fireRowRemoved(URow o) {
		if (listeners != null) {
			for (TableListener l : listeners) {
				l.rowRemoved(o);
			}
		}
	}

	public int getColIndex(Object o) {
		return cols.indexOf(o);
	}

	public int getColumnCount() {
		return cols.size();
	}

	public void addTableListener(TableListener listener) {
		if (listeners == null) {
			listeners = new ArrayList<TableListener>();
		}
		listeners.add(listener);
	}

	public void clear() {
		cols = new ArrayList<UColumn>();
		rows = new ArrayList<URow>();
	}
}
