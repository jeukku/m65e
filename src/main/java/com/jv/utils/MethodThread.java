package com.jv.utils;

import java.lang.reflect.Method;

public class MethodThread extends Thread {
	private Object o;

	private String mMethod;

	private Object args[];

	public MethodThread(Object o, String mMethod, Object args[]) {
		this.o = o;
		this.mMethod = mMethod;
		this.args = args;
		start();
	}

	public MethodThread(Object o, String mMethod, Object arg) {
		this.o = o;
		this.mMethod = mMethod;
		args = new Object[1];
		args[0] = arg;
		start();
	}

	public void run() {
		try {
			Class c = o.getClass();
			Method ms[] = c.getMethods();
			for (int i = 0; i < ms.length; i++) {
				Method m = ms[i];
				if (m.getName().equals(mMethod)
						&& m.getParameterTypes().length == args.length) {
					m.invoke(o, args);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
