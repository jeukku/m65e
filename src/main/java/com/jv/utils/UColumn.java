package com.jv.utils;

/**
 * Created by IntelliJ IDEA.
 * User: juuso
 * Date: Dec 18, 2007
 * Time: 5:37:20 PM
 * To change this template use File | Settings | File Templates.
 */
public interface UColumn {

	String getName();

	void setName(String t);

	Class<?> getTypeClass();
}

