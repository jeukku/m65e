package com.jv.utils;

import java.io.File;

public class Path {
	String path;

	public Path() {
		path = "";
	}

	public Path(String path) {
		this.path = new String(path);
		check();
	}

	public Path(java.net.URL url) {
		this("" + url);
	}

	public String toString() {
		return path;
	}

	public int length() {
		return path.length();
	}

	public String getValue() {
		return toString();
	}

	public void setValue(String path) {
		this.path = new String(path);
		check();
	}

	public Path getParent() {
		Path p = new Path(path);
		p.parent();
		return p;
	}

	public void parent() {
		int index = path.lastIndexOf(File.separatorChar);
		if (index < path.length() - 1) {
			path = path.substring(0, index);
		} else {
			path = path.substring(0, path.length() - 1);
			parent();
		}
	}

	public Path getAppend(String b) {
		Path p = new Path(path);
		p.append(b);
		return p;
	}

	public void append(String b) {
		Path pb = new Path(b);
		if (!path.endsWith(File.separator)
				&& !pb.path.startsWith(File.separator))
			path += File.separator;
		path += pb.path;
		check();
	}

	public String getFile() {
		String dir = path.substring(0, path.lastIndexOf(File.separatorChar));
		String file = path.substring(dir.length());
		return file;
	}

	public void check() {
		int count = 0;
		if (File.separatorChar == '\\') {
			path = path.replace('/', '\\');
			while (path.indexOf("\\\\") != -1 && count++ < 1000) {
				int index = path.indexOf("\\\\");
				String start = path.substring(0, index);
				String end = path.substring(index + 1);
				path = start + end;
			}
		} else if (File.separatorChar == '/') {
			path = path.replace('/', '/');
			while (path.indexOf("//") != -1 && count++ < 1000) {
				int index = path.indexOf("//");
				String start = path.substring(0, index);
				String end = path.substring(index + 1);
				path = start + end;
			}
		}
		
		if (path.startsWith("file:")) {
			path = path.substring("file:".length());
		}
	}
}
