/******************************************************
 * File: com.jv.utils.ValueListener.java
 * created Oct 21, 2002 2:43:43 PM by vilmunju
 */
package com.jv.utils;

public interface ValueListener {
	void valueChanged(Object src, Object o);
}
