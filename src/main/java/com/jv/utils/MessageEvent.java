//Title:        Your Product Name
//Version:      
//Copyright:    Copyright (c) 1998
//Author:       Your Name
//Company:      Your Company
//Description:  Your description
package com.jv.utils;

import java.util.EventObject;

public class MessageEvent extends EventObject {
	private String message;

	public MessageEvent(Object source, String message) {
		super(source);
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
