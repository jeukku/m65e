package com.jv.utils;

import java.awt.event.KeyEvent;

public class StringGetta {
	// private StringBuffer s = new StringBuffer(10);
	private java.util.Vector s = new java.util.Vector();

	int cursor = 0;

	public void set(StringBuffer s) {
		this.s = new java.util.Vector();
		for (int i = 0; i < s.length(); i++)
			this.s.addElement(new Character(s.charAt(i)));
	}

	public char getChar(int i) {
		return ((Character) s.elementAt(i)).charValue();
	}

	public StringBuffer get() {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < s.size(); i++)
			sb.append(getChar(i));
		return sb;
	}

	public StringBuffer getWithCursor() {
		StringBuffer ns = get();
		ns.insert(cursor, '|');
		return ns;
	}

	public int length() {
		return s.size();
	}

	public void trim() {
		while (getChar(s.size() - 1) == ' ')
			s.removeElementAt(s.size() - 1);
		replace(" ;", ";");
		replace(" )", ")");
		replace("( ", "(");
		replace(" (", "(");
		replace(" ,", ",");
		replace("  ", " ");
		replace(" .", ".");
		replace(". ", ".");
	}

	int replace(String a, String b) {
		StringBuffer ns = new StringBuffer();
		ns.append(s);
		return 0;
	}

	public void append(String s) {
		for (int i = 0; i < s.length(); i++)
			this.s.addElement(new Character(s.charAt(i)));
	}

	public void append(StringBuffer s) {
		for (int i = 0; i < s.length(); i++)
			this.s.addElement(new Character(s.charAt(i)));
	}

	public void append(double d) {
		String s = "" + d;
		append(s);
	}

	public void append(char c) {
		String s = "" + c;
		append(s);
	}

	public void append(int i) {
		String s = "" + i;
		append(s);
	}

	public void clear() {
		s.removeAllElements();
	}

	public boolean key(KeyEvent e) {
		int key = e.getKeyCode();
		char c = e.getKeyChar();
		if (key == KeyEvent.VK_SHIFT || key == KeyEvent.VK_CONTROL
				|| key == KeyEvent.VK_ALT) {
			// ei kiinosta
		} else if (key == KeyEvent.VK_ENTER) {
			return false;
		} else if (key == KeyEvent.VK_BACK_SPACE) {
			if (cursor > 0) {
				int index = cursor;
				if (index >= s.size())
					index = s.size() - 1;
				s.removeElementAt(index);
				cursor--;
			}
		} else if (key == KeyEvent.VK_LEFT) {
			cursor--;
			if (cursor < 0)
				cursor = 0;
		} else if (key == KeyEvent.VK_RIGHT) {
			cursor++;
			if (cursor > s.size())
				cursor = s.size();
		} else {
			if (cursor < s.size())
				s.insertElementAt(new Character(c), cursor);
			else
				append(c);
			cursor++;
		}
		if (cursor < 0)
			cursor = 0;
		else if (cursor > s.size())
			cursor = s.size();
		return true;
	}
}
