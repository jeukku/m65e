package com.jv.utils;

public class Mutex {
	private Thread t;

	private int depth;

	public synchronized void aquire() {
		while (t != null && Thread.currentThread() != t) {
			try {
				System.out.println("waiting " + Thread.currentThread());
				wait(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		t = Thread.currentThread();
		depth++;
	}

	public synchronized void release() {
		depth--;
		if (depth == 0) {
			t = null;
			notifyAll();
		}
	}

}
