//Title:        Your Product Name
//Version:      
//Copyright:    Copyright (c) 1998
//Author:       Your Name
//Company:      Your Company
//Description:  Your description
package com.jv.utils;

import java.util.EventListener;

public interface MessageListener extends EventListener {
	public void progressChanged(int current, int min, int max);
	public void messageChanged(MessageEvent e);
}
