/*
 * Created on Dec 29, 2004 by Juuso
 */
package com.jv.utils;

import java.util.Iterator;
import java.util.Vector;

/* @author juuso */
public class SomethingListenerGroup {
	private Vector listeners = new Vector();

	public void addListener(SomethingListener l) {
		listeners.addElement(l);
	}

	public void fireListeners(Object o) {
		Iterator i = listeners.iterator();
		while (i.hasNext()) {
			SomethingListener l = (SomethingListener) i.next();
			l.somethingHappened(o);
		}
	}
}