/*
 * Created on 8.9.2004
 *
 */
package com.jv.utils;

import java.util.Vector;

/**
 * @author juuso
 * 
 */
public class ProgressEventProvider {
	private Vector listeners = new Vector();

	protected void fireProgressChanged(int current, int min, int max) {
		if (listeners != null) {
			int count = listeners.size();
			for (int i = 0; i < count; i++)
				((MessageListener) listeners.elementAt(i)).progressChanged(
						current, min, max);
		}
	}
}
