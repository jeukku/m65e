package com.jv.utils;

public class EString {
	private char cs[] = new char[20];

	private int m = 0, a;

	private java.util.Vector splits;

	public EString() {
		// empty string
	}

	public EString(String s) {
		cs = s.toCharArray();
		m = s.length();
	}

	public EString(EString es) {
		append(es);
	}

	public EString(char ncs[], int start, int end) {
		cs = new char[end - start];
		a = cs.length;
		m = a;
		System.arraycopy(ncs, start, cs, 0, cs.length);
	}

	public boolean endsWith(String s) {
		if (m < s.length())
			return false;
		boolean equals = true;
		for (int i = 0; i < s.length(); i++)
			if (s.charAt(s.length() - i - 1) != cs[m - i - 1]) {
				equals = false;
				break;
			}
		return equals;
	}

	public boolean startsWith(String s) {
		if (m < s.length())
			return false;
		boolean equals = true;
		for (int i = 0; i < s.length(); i++)
			if (s.charAt(i) != cs[i]) {
				equals = false;
				break;
			}
		return equals;
	}

	public char charAt(int index) {
		return cs[index];
	}

	public EString subString(int start, int end) {
		EString es = new EString(cs, start, end);
		/*
		 * for(int i=start; i<end; i++) es.append(cs[i]);
		 */
		return es;
	}

	public void splitWithQuotas(String s) {
		splits = new java.util.Vector();
		EString es = new EString(this);
		int index = es.indexOfAnyOfCharsWithQuotas(s);
		while (index != -1) {
			EString split = es.subString(0, index);
			if (split.length() > 0)
				splits.addElement(split);
			es = es.subString(index + 1, es.length());
			index = es.indexOfAnyOfCharsWithQuotas(s);
		}
		if (es.length() > 0)
			splits.addElement(es);
	}

	public void split(String s) {
		splits = new java.util.Vector();
		EString es = new EString(this);
		int index = es.indexOfAnyOfChars(s);
		while (index != -1) {
			EString split = es.subString(0, index);
			split.trimEnd(s);
			split.trimStart(s);
			if (split.length() > 0)
				splits.addElement(split);
			es = es.subString(index + 1, es.length());
			index = es.indexOfAnyOfChars(s);
		}
		if (es.length() > 0)
			splits.addElement(es);
	}

	public boolean hasMoreTokens() {
		if (splits == null)
			return false;
		if (splits.size() == 0)
			return false;
		return true;
	}

	public EString nextToken() {
		if (splits == null)
			return null;
		EString token = (EString) splits.elementAt(0);
		splits.removeElementAt(0);
		if (splits.size() == 0)
			splits = null;
		return token;
	}

	public boolean removeBetween(String a, String b, boolean removeborders) {
		int ai = indexOf(a);
		int bi = indexOf(b, ai + a.length());
		if (ai == -1 || bi == -1)
			return false;
		if (removeborders) {
			bi += b.length();
		} else {
			ai += a.length();
		}
		remove(ai, bi);
		return true;
	}

	public void remove(int ai, int bi) {
		int d = bi - ai;
		for (int i = ai; i < m - d; i++)
			cs[i] = cs[i + d];
		m -= d;
	}

	public void append(char ch) {
		// if(!Character.isDefined(ch)) return;
		if (m + 1 >= a)
			grow();
		cs[m++] = ch;
	}

	public void append(EString es) {
		for (int i = 0; i < es.length(); i++)
			append(es.cs[i]);
	}

	public void append(char ncs[]) {
		int start = 0;
		while (!Character.isDefined(ncs[start]))
			start++;
		while (m + ncs.length >= a)
			grow();
		for (int i = 0; i < ncs.length; i++)
			cs[m++] = ncs[i];
	}

	public int length() {
		return m;
	}

	public void trimEnd(String s) {
		while (m > 0) {
			int i = s.indexOf(cs[m - 1]);
			if (i == -1)
				break;
			m--;
		}
	}

	public int indexOfAnyOfChars(String s) {
		return indexOfAnyOfChars(s, 0);
	}

	public int indexOfAnyOfChars(String s, int start) {
		for (int i = start; i < m; i++)
			if (s.indexOf(cs[i]) != -1)
				return i;
		return -1;
	}

	public int indexOfAnyOfCharsWithQuotas(String s) {
		return indexOfAnyOfCharsWithQuotas(s, 0);
	}

	public int indexOfAnyOfCharsWithQuotas(String s, int start) {
		boolean inq = false;
		for (int i = start; i < m; i++) {
			if (cs[i] == '\"' || cs[i] == '\'')
				inq = !inq;
			if (!inq && s.indexOf(cs[i]) != -1)
				return i;
		}
		return -1;
	}

	public int indexOf(String s, int start) {
		int sl = s.length();
		int max = m - sl + 1;
		for (int i = start; i < max; i++) {
			int si = 0;
			for (; si < sl; si++) {
				if (cs[i + si] != s.charAt(si))
					break;
			}
			if (si == sl)
				return i;
		}
		return -1;
	}

	public int indexOf(String s) {
		return indexOf(s, 0);
	}

	public void removeString(String s) {
		int index = indexOf(s);
		while (index != -1) {
			for (int i = 0; i < s.length(); i++)
				remove(index);
			index = indexOf(s);
		}
	}

	public void remove(String s) {
		for (int i = 0; i < m; i++) {
			int ich = s.indexOf(cs[i]);
			if (ich != -1)
				remove(i--);
		}
	}

	public void remove(int remove) {
		for (int i = remove; i < m - 1; i++)
			cs[i] = cs[i + 1];
		m--;
	}

	public void trimStart(String s) {
		int start = 0;
		while (start < m) {
			int i = s.indexOf(cs[start]);
			if (i == -1)
				break;
			start++;
		}
		m -= start;
		for (int i = 0; i < m; i++) {
			cs[i] = cs[i + start];
		}
	}

	public String toString() {
		return new String(cs, 0, m);
	}

	public void grow() {
		int nl = (int) (cs.length * 1.2);
		if (nl == cs.length)
			nl = cs.length + 1;
		// log.errorln("com.jv.utils.EString growin' oldlength:" + cs.length + "
		// newlength:"+nl);
		char old[] = cs;
		cs = new char[nl];
		System.arraycopy(old, 0, cs, 0, old.length);
		a = cs.length;
	}
}
