package com.jv.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA. User: juuso Date: Dec 18, 2007 Time: 5:42:32 PM To
 * change this template use File | Settings | File Templates.
 */
public class StringColumn implements UColumn {
	private String name;
	private List<ColumnListener> listeners = new ArrayList<ColumnListener>(0);

	public StringColumn(String colname) {
		this.name = colname;
	}

	
	public Class<?> getTypeClass() {
		return String.class;
	}

	public String getName() {
		return name;
	}

	
	public void setName(String t) {
		this.name = t;
		fireNameChanged();
	}

	private void fireNameChanged() {
		for (ColumnListener l : listeners) {
			l.columnNameChanged(this);
		}
	}

}
