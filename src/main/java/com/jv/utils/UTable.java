package com.jv.utils;

import java.util.List;

public interface UTable {
	UColumn addColumn(UColumn column);

	UColumn addColumn(String name);

	URow addRow(URow row);

	void removeRow(URow o);

	int getRowCount();

	boolean contains(Object o);

	int indexOfRow(Object o);

	int getColIndex(Object o);

	int getColumnCount();

	void clear();

	List<URow> getRows();

	List<UColumn> getColumns();
}
