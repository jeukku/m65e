package com.jv.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

import org.apache.log4j.Logger;

public class CopyRepeat {
	Vector sources = new Vector();

	Vector dests = new Vector();

	private Logger log = Logger.getLogger(this.getClass().getName());

	void copy(String source, String dest) throws IOException {
		log.debug("source " + source);
		log.debug("dest " + dest);
		File fsource = new File(source);
		if (fsource.isDirectory()) {
			String files[] = fsource.list();
			for (int i = 0; i < files.length; i++) {
				String nsource = source + File.separatorChar + files[i];
				String ndest = dest + File.separatorChar + files[i];
				copy(nsource, ndest);
			}
		}
		try {
			InputStream is = new BufferedInputStream(
					new FileInputStream(source));
			OutputStream os = new BufferedOutputStream(new FileOutputStream(
					dest));
			while (true) {
				int b = is.read();
				if (b == -1)
					break;
				os.write(b);
			}
			os.close();
			is.close();
		} catch (IOException ioe) {
			log.debug("copying source " + source + " dest " + dest);
			sources.addElement(source);
			dests.addElement(dest);
		}
	}

	public static void main(String args[]) {
		if (args.length < 2) {
			System.out.println("more argumetns please\n\n\n");
			return;
		}
		CopyRepeat cr = new CopyRepeat();
		try {
			cr.copy(args[0], args[1]);
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
