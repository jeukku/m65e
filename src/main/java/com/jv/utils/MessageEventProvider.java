/*
 * Created on 8.9.2004
 *
 */
package com.jv.utils;

import java.util.Vector;

/**
 * @author juuso
 * 
 */
public class MessageEventProvider {
	private transient Vector messageListeners;

	public synchronized void removeMessageListener(MessageListener l) {
		if (messageListeners != null && messageListeners.contains(l)) {
			Vector v = (Vector) messageListeners.clone();
			v.removeElement(l);
			messageListeners = v;
		}
	}

	public synchronized void addMessageListener(MessageListener l) {
		Vector v = messageListeners == null ? new Vector(2)
				: (Vector) messageListeners.clone();
		if (!v.contains(l)) {
			v.addElement(l);
			messageListeners = v;
		}
	}

	public void fireMessageChanged(MessageEvent e) {
		if (messageListeners != null) {
			Vector listeners = messageListeners;
			int count = listeners.size();
			for (int i = 0; i < count; i++)
				((MessageListener) listeners.elementAt(i)).messageChanged(e);
		}
	}
}
