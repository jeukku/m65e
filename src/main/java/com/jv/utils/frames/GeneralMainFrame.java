//Title:        Your Product Name
//Version:      
//Copyright:    Copyright (c) 1998
//Author:       Juuso Vilmunen
//Company:      -
//Description:  Your description
package com.jv.utils.frames;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.event.WindowEvent;

public class GeneralMainFrame extends Frame {
	FlowLayout flowLayout1 = new FlowLayout();

	public GeneralMainFrame() {
		try {
			jbInit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void jbInit() throws Exception {
		this.setLayout(flowLayout1);
		this.setSize(new Dimension(402, 303));
		this.setTitle("generalframe");
		this.addWindowListener(new GeneralMainFrame_this_windowAdapter(this));
	}

	void this_windowClosing(WindowEvent e) {
		dispose();
	}
}

class GeneralMainFrame_this_windowAdapter extends java.awt.event.WindowAdapter {
	GeneralMainFrame adaptee;

	GeneralMainFrame_this_windowAdapter(GeneralMainFrame adaptee) {
		this.adaptee = adaptee;
	}

	public void windowClosing(WindowEvent e) {
		adaptee.this_windowClosing(e);
	}
}
