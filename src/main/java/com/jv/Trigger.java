package com.jv;

public class Trigger {
	private boolean set;

	private Object lock = new Object();

	private String name;

	public Trigger(String name) {
		this.name = name;
	}

	public void set() {
		this.set = true;
		synchronized (lock) {
			lock.notify();
		}
	}

	public boolean isSet() {
		return set;
	}

	public void waitTrigger(int i) {
		if (set)
			return;
		long st = System.currentTimeMillis();
		synchronized (lock) {
			try {
				lock.wait(i);
				long et = System.currentTimeMillis();
				if (et - st >= i) {
					throw new RuntimeException(Messages
							.getString("WARNING_TRIGGERED0") + name); //$NON-NLS-1$
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (RuntimeException e) {
				e.printStackTrace();
			}
		}
	}
}
