package com.jv.resources;

import java.io.IOException;
import java.io.InputStream;

public class VerboseInputStream extends InputStream {
	private InputStream stream;

	public VerboseInputStream(InputStream stream) {
		this.stream = stream;
	}

	
	public int read() throws IOException {
		int b = stream.read();
		System.out.println("b: " + b + " ch:" + (char) b);
		return b;
	}
}
