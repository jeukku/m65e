package com.jv.resources;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class URLResourceInputStreamFactory implements ResourcesInputStreamFactory {

	private URL baseurl;

	public URLResourceInputStreamFactory(URL baseurl) {
		this.baseurl = baseurl;
	}

	public InputStream getInputStream(String sfile) {
		try {
			URL url = getURL(sfile);
			return url.openStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private URL getURL(String sfile) throws MalformedURLException {
		URL url = new URL(baseurl.toString() + sfile);
		return url;
	}

}
