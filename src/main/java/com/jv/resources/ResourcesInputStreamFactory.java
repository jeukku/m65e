package com.jv.resources;

import java.io.InputStream;

public interface ResourcesInputStreamFactory {
	InputStream getInputStream(String sfile);
}