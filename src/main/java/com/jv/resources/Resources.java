package com.jv.resources;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import com.jv.image.ImageLoader;


public class Resources {
	private ResourcesInputStreamFactory ris = new DirectoryResourceInputStreamFactory();
	private String defaultini;

	public void setInputStreamFactory(ResourcesInputStreamFactory ris) {
		this.ris = ris;
	}

	public void setDefaultIni(String name) {
		defaultini = name;
	}

	public InputStream getBufferStream(String sfile) {
		System.out.println(getClass().getName() + " ris " + ris);
		// return new VerboseInputStream( new
		// BufferedInputStream(ris.getInputStream(sfile)));
		InputStream inputStream;
		inputStream = getURL(sfile);
		if (inputStream == null) {
			inputStream = ris.getInputStream(sfile);
		}
		return new BufferedInputStream(inputStream);
	}

	private InputStream getURL(String sfile) {
		URL ret;
		try {
			ret = new URL(sfile);
			return ret.openStream();
		} catch (MalformedURLException e) {
			System.out.println("" + sfile + " is not a valid URL");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Image getImage(String string) {
		ImageLoader il = new ImageLoader();
		try {
			return il.getImage(getBufferStream(string));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Properties getIni(String string) {
		Properties p = new Properties();
		try {
			p.load(getBufferStream(string + ".ini"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return p;
	}

	public String getProperty(String name) {
		return getIni(defaultini).getProperty(name);
	}
}