package com.jv.resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class DirectoryResourceInputStreamFactory implements
		ResourcesInputStreamFactory {
	private String base = ".";

	public DirectoryResourceInputStreamFactory() {
	}

	public void setDirectory(String resbase) {
		this.base = resbase;
	}

	public InputStream getInputStream(String sfile) {
		String sfilepath = base + File.separator + sfile;
		File f = new File(sfilepath);
		System.out.println("file : " + f.getAbsolutePath());
		try {
			return new FileInputStream(sfilepath);
		} catch (FileNotFoundException e) {
			System.out.println("file not found at " + f.getAbsolutePath());
			e.printStackTrace();
		}
		return null;
	}
}
