/*
 * Created on 7.9.2004
 *
 */
package m65eII.importers;


/**
 * @author juuso
 *  
 */
public class YafRay {
//	implements GeometryLoader, DocHandler {
//
//    private TTransformationNode base_tr;
//
//    private TTransformationNode current_tr;;
//
//    private int mode;
//
//    private Hashtable materials = new Hashtable();
//
//    private Hashtable textures = new Hashtable();
//
//    private ElementHandler c_handler;
//
//    class ElementHandler {
//        private ElementHandler parent;
//
//        protected ElementHandler getParent() {
//            return parent;
//        }
//
//        /**
//         *  
//         */
//        public ElementHandler(Hashtable h, ElementHandler nparent) {
//            parent = nparent;
//        }
//
//        void startElement(String tag, Hashtable attr) {
//            GOutput.push("Default ElementHandler " + tag);
//            GOutput.print(attr);
//            if (tag.equals("object")) {
//                GOutput.print(attr);
//                c_handler = new ObjectHandler(attr, c_handler);
//            } else if (tag.equals("shader")) {
//                c_handler = new ShaderHandler(attr, c_handler);
//            } else if (tag.equals("texture")) {
//                c_handler = new TextureHandler(attr, c_handler);
//            } else {
//                GOutput.println("tag unkown " + tag);
//                c_handler = new ElementHandler(attr, c_handler);
//            }
//        }
//
//        void endElement(String tag) {
//            if (parent != null) {
//                c_handler = parent;
//                GOutput.println("popping " + this);
//            } else {
//                GOutput.println("not popping empty " + this);
//            }
//
//            GOutput.pop();
//        }
//    }
//
//    public YafRay() {
//        c_handler = new ElementHandler(null, null);
//    }
//
//    public TransformationNode attach(TransformationNode node) {
//        TransformationNode nnode = node.addNode();
//        base_tr.copyTo(nnode);
//        return nnode;
//    }
//
//    public void read(InputStream is) throws IOException {
//        try {
//            QDParser.parse(this, new InputStreamReader(is));
//        } catch (Exception e) {
//            GOutput.print(e);
//        }
//
//    }
//
//    public void endDocument() throws Exception {
//        GOutput.pop();
//    }
//
//    /*
//     * (non-Javadoc)
//     * 
//     * @see qdxml.DocHandler#endElement(java.lang.String)
//     */
//    public void endElement(String tag) throws Exception {
//        c_handler.endElement(tag);
//    }
//
//    /*
//     * (non-Javadoc)
//     * 
//     * @see qdxml.DocHandler#startDocument()
//     */
//    public void startDocument() throws Exception {
//        GOutput.push("Yaf startDocument");
//    }
//
//    /*
//     * (non-Javadoc)
//     * 
//     * @see qdxml.DocHandler#startElement(java.lang.String, java.util.Hashtable)
//     */
//    public void startElement(String tag, Hashtable h) throws Exception {
//        c_handler.startElement(tag, h);
//    }
//
//    private class FacesHandler extends ElementHandler {
//        private Object3D obj;
//
//        private TriangleList tl;
//
//        private VectorList vl;
//        private PointList txl;
//
//        private P tmp = new P(0,0);
//
//        /**
//         *  
//         */
//        public FacesHandler(Hashtable h, ElementHandler parent, Object3D nobj,
//                TMaterial mat) {
//            super(h, parent);
//            obj = nobj;
//            tl = obj.addTriangleList();
//            vl = obj.getVectorList();
//            mat.copyTo(tl.getMaterial());
//        }
//
//        void endElement(String tag) {
//            if (tag.equals("f")) {
//            } else
//                super.endElement(tag);
//        }
//
//        /*
//         * (non-Javadoc)
//         * 
//         * @see m65eII.importers.YafRay.ElementHandler#startElement(java.lang.String,
//         *      java.util.Hashtable)
//         */
//        public void startElement(String tag, Hashtable attr) {
//            if (tag.equals("f")) {
//                int a = Integer.parseInt((String) attr.get("a"));
//                int b = Integer.parseInt((String) attr.get("b"));
//                int c = Integer.parseInt((String) attr.get("c"));
//                //GOutput.println("f " + a + ", " + b + ", " + c);
//                tl.addTriangle(a, b, c);
//
//                try {
//                    tmp.u = Float.parseFloat((String) attr.get("u_a"));
//                    tmp.v = Float.parseFloat((String) attr.get("v_a"));                    
//                    txl.set(a, tmp);
//
//                    tmp.u = Float.parseFloat((String) attr.get("u_b"));
//                    tmp.v = Float.parseFloat((String) attr.get("v_b"));
//                    txl.set(b, tmp);
//
//                    tmp.u = Float.parseFloat((String) attr.get("u_c"));
//                    tmp.v = Float.parseFloat((String) attr.get("v_c"));
//                    txl.set(c, tmp);
//                } catch (NullPointerException e) {
//                    GOutput
//                            .println("nullpointerexcetion... no uv coordinates for face");
//                }
//            }
//        }
//    }
//
//    private class PointsHandler extends ElementHandler {
//        VectorList vl;
//
//        /**
//         *  
//         */
//        public PointsHandler(Hashtable h, ElementHandler parent, Object3D obj) {
//            super(h, parent);
//
//            vl = obj.getVectorList();
//        }
//
//        public void endElement(String tag) {
//            if (tag.equals("p")) {
//            } else
//                super.endElement(tag);
//        }
//
//        /*
//         * (non-Javadoc)
//         * 
//         * @see m65eII.importers.YafRay.ElementHandler#startElement(java.lang.String,
//         *      java.util.Hashtable)
//         */
//        public void startElement(String tag, Hashtable attr) {
//            if (tag.equals("p")) {
//                float x = Float.parseFloat((String) attr.get("x"));
//                float y = Float.parseFloat((String) attr.get("y"));
//                float z = Float.parseFloat((String) attr.get("z"));
//                GOutput.println("p " + x + ", " + y + ", " + z);
//                vl.addVector(new V(x, y, z));
//            }
//        }
//    }
//
//    private class ObjectHandler extends ElementHandler {
//        Object3D obj;
//
//        TMaterial mat;
//
//        public ObjectHandler(Hashtable h, ElementHandler parent) {
//            super(h, parent);
//
//            if (current_tr == null || base_tr == null) {
//                base_tr = new TTransformationNode();
//                current_tr = base_tr;
//            }
//
//            String shader_name = (String) h.get("shader_name");
//            mat = (TMaterial) materials.get(shader_name);
//
//            GOutput.print("Material " + mat);
//
//            obj = current_tr.addObject3D();
//            obj.setName((String) h.get("name"));
//
//            GOutput.print("created " + obj);
//        }
//
//        /*
//         * (non-Javadoc)
//         * 
//         * @see m65eII.importers.YafRay.ElementHandler#startElement(java.lang.String,
//         *      java.util.Hashtable)
//         */
//        public void startElement(String tag, Hashtable attr) {
//            if (tag.equals("mesh")) {
//                GOutput.push("Object startElement " + tag);
//                GOutput.print(attr);
//
//                c_handler = new MeshHandler(attr, this, obj, mat);
//            } else {
//                GOutput.push("Object unkown element " + tag);
//                c_handler = new ElementHandler(attr, this);
//            }
//        }
//    }
//
//    private class MeshHandler extends ElementHandler {
//        private Object3D obj;
//
//        private TMaterial mat;
//
//        public MeshHandler(Hashtable h, ElementHandler parent, Object3D nobj,
//                TMaterial nmat) {
//            super(h, parent);
//
//            obj = nobj;
//            mat = nmat;
//        }
//
//        public void startElement(String tag, Hashtable attr) {
//            if (tag.equals("points")) {
//                GOutput.push("Yaf startElement " + tag);
//                c_handler = new PointsHandler(attr, this, obj);
//            } else if (tag.equals("faces")) {
//                GOutput.push("Yaf startElement " + tag);
//                c_handler = new FacesHandler(attr, this, obj, mat);
//            }
//        }
//    }
//
//    private class ShaderHandler extends ElementHandler {
//        private TMaterial mat = new TMaterial();
//
//        private String name;
//
//        /**
//         *  
//         */
//        public ShaderHandler(Hashtable h, ElementHandler parent) {
//            super(h, parent);
//            name = (String) h.get("name");
//            materials.put(name, mat);
//
//        }
//
//        /*
//         * (non-Javadoc)
//         * 
//         * @see m65eII.importers.YafRay.ElementHandler#startElement(java.lang.String,
//         *      java.util.Hashtable)
//         */
//        public void startElement(String tag, Hashtable attr) {
//            GOutput.push(this, "startElement " + tag);
//            if (tag.equals("attributes")) {
//                c_handler = new AttributesHandler(attr, this, tag);
//            } else if (tag.equals("modulator")) {
//                c_handler = new AttributesHandler(attr, this, tag);
//
//                GOutput.print(attr);
//
//                String texname = (String) attr.get("texname");
//                String mode = (String) attr.get("mode");
//                String texco = (String) attr.get("texco");
//                String clipping = (String) attr.get("clipping");
//                GOutput.println("modulator texname " + texname);
//
//                String texfilename = (String) textures.get(texname);
//                // TODO mites noi muut
//                mat.setTexture(texfilename);
//            } else {
//                GOutput.println(this, "unkown tag " + tag);
//                GOutput.pop();
//            }
//
//        }
//
//        private class AttributesHandler extends ElementHandler {
//            private String atag;
//
//            /**
//             *  
//             */
//            public AttributesHandler(Hashtable h, ElementHandler parent,
//                    String ntag) {
//                super(h, parent);
//                atag = ntag;
//            }
//
//            /*
//             * (non-Javadoc)
//             * 
//             * @see m65eII.importers.YafRay.ElementHandler#startElement(java.lang.String,
//             *      java.util.Hashtable)
//             */
//            public void startElement(String tag, Hashtable attr) {
//                GOutput.println(this, "startElement " + tag);
//                GOutput.print(attr);
//            }
//
//            /*
//             * (non-Javadoc)
//             * 
//             * @see m65eII.importers.YafRay.ElementHandler#endElement(java.lang.String)
//             */
//            public void endElement(String tag) {
//                GOutput.println("endElement " + tag);
//                if (tag.equals(atag)) {
//                    super.endElement(tag);
//                } else {
//                }
//            }
//        }
//
//        /*
//         * (non-Javadoc)
//         * 
//         * @see m65eII.importers.YafRay.ElementHandler#endElement(java.lang.String)
//         */
//        public void endElement(String tag) {
//            GOutput.println("endElement " + tag);
//            if (tag.equals("attributes")) {
//                mode = 0;
//                GOutput.pop();
//                c_handler = getParent();
//            } else if (tag.equals("modulator")) {
//                mode = 0;
//                GOutput.pop();
//                c_handler = getParent();
//            } else {
//                super.endElement(tag);
//            }
//        }
//    }
//
//    private class TextureHandler extends ElementHandler {
//        private String name;
//
//        /**
//         *  
//         */
//        public TextureHandler(Hashtable h, ElementHandler parent) {
//            super(h, parent);
//            name = (String) h.get("name");
//        }
//
//        public void startElement(String tag, Hashtable attr) {
//            GOutput.println(this, "startElement " + tag);
//            GOutput.print(attr);
//
//            if (tag.equals("filename")) {
//                String filename = (String) attr.get("value");
//                GOutput.println("FILENAME " + filename);
//                textures.put(name, filename);
//            }
//        } /*
//           * (non-Javadoc)
//           * 
//           * @see m65eII.importers.YafRay.ElementHandler#endElement(java.lang.String)
//           */
//
//        public void endElement(String tag) {
//            GOutput.println("endElement " + tag);
//            if (tag.equals("texture")) {
//                super.endElement(tag);
//            } else if (tag.equals("filename")) {
//            } else {
//            }
//        }
//    }
//
//    /*
//     * (non-Javadoc)
//     * 
//     * @see qdxml.DocHandler#text(java.lang.String)
//     */
//    public void text(String str) throws Exception {
//        // TODO Auto-generated method stub
//
//    }
}