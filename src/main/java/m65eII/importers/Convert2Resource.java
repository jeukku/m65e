/*
 * Created on Jun 22, 2005
 */
package m65eII.importers;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import m65eII.resources.DefaultResources3D;
import m65eII.resources.GeometryLoader;
import m65eII.tr.TTransformationNode;

public class Convert2Resource {
    private String file;

    private void run() {
        try {
            DefaultResources3D res = new DefaultResources3D((URL)null);
            GeometryLoader gl = res.getObject(getInputStream(), file);
            TTransformationNode tnode = new TTransformationNode();
            gl.attach(tnode);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }

    private InputStream getInputStream() throws FileNotFoundException {
        return new BufferedInputStream(new FileInputStream(file));
    }

    public static void main(String[] args) {
        Convert2Resource c2r = new Convert2Resource();
        for (int i = 0; i < args.length; i++) {
            c2r.file = args[i];
        }
        //
        c2r.run();
    }
}
