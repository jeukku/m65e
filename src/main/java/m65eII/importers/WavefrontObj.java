/*
 * Created on Jun 22, 2005
 */
package m65eII.importers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import m65eII.Logger;
import m65eII.LoggerFactory;
import m65eII.engine.Object3D;
import m65eII.engine.P;
import m65eII.engine.PointList;
import m65eII.engine.TransformationNode;
import m65eII.engine.TriangleList;
import m65eII.engine.V;
import m65eII.engine.VectorList;
import m65eII.resources.GeometryLoader;
import m65eII.tr.TTransformationNode;

public class WavefrontObj implements GeometryLoader {
    private TTransformationNode basenode;
    private BufferedReader br;
    private VectorList vl;
    private PointList txl;
    private TriangleList tl;
    private Object3D obj;
	private boolean ok;
	//
	private Logger log = LoggerFactory.createLogger(this
			);
    //
    private static final int FACECOMPONENTINDEX_VECTOR = 0;
    private static final int FACECOMPONENTINDEX_NORMAL = 1;
    private static final int FACECOMPONENTINDEX_TEXTURE = 2;

    public TransformationNode attach(TransformationNode node) {
        basenode.copyTo(node);
        return node;
    }
    
    public boolean isOk() {
    	// TODO Auto-generated method stub
    	return ok;
    }

    public void read(InputStream is) throws IOException {
        br = new BufferedReader(new InputStreamReader(is));
        try {
        	loop();
        	ok = true;
        } catch(NumberFormatException e) {
        	e.printStackTrace();
        	ok = false;
        }
    }

    private void loop() throws IOException {
        basenode = new TTransformationNode();
        //
        while (true) {
            String line = br.readLine();
            if (line == null) break;
            if (line.startsWith("#")) {
                System.out.println("comment: " + line);
            } else if (line.startsWith("o ")) {
                loadObject(line);
            } else if (line.startsWith("v ")) {
                loadVertex(line);
            } else if (line.startsWith("vt ")) {
                loadVertexTexture(line);
            } else if (line.startsWith("f ")) {
                loadFace(line);
            } else {
                System.out.println("unkown: " + line);
            }
        }
        log.debugln("vl size " + vl.size());
    }

    private void loadVertexTexture(String line) {
        V v = loadVector(line);
        txl.add(new P(v.x, v.y));
    }

    private V loadVector(String line) {
        StringTokenizer st = new StringTokenizer(line);
        st.nextToken();
        String sx = st.nextToken();
        String sy = st.nextToken();
        String sz = st.nextToken();
        float x = Float.valueOf(sx).floatValue();
        float y = Float.valueOf(sy).floatValue();
        float z = Float.valueOf(sz).floatValue();
        return new V(x,y,z);
    }
    
    private void loadVertex(String line) {
        V v = loadVector(line);
        vl.addVector(v);
    }

    private void loadFace(String line) {
        try {
            StringTokenizer st = new StringTokenizer(line);
            st.nextToken();
            int ia[] = new int[3];
            int ib[] = new int[3];
            int ic[] = new int[3];
            parseFaceComponent(ia, st.nextToken());
            parseFaceComponent(ib, st.nextToken());
            do {
                parseFaceComponent(ic, st.nextToken());
                int nindex = tl.addTriangle(ia[FACECOMPONENTINDEX_VECTOR],
                        ib[FACECOMPONENTINDEX_VECTOR],
                        ic[FACECOMPONENTINDEX_VECTOR]);
                log.debugln("tri " + ia[FACECOMPONENTINDEX_VECTOR]+
                		", "+ ib[FACECOMPONENTINDEX_VECTOR]+
                		", "+ic[FACECOMPONENTINDEX_VECTOR]);
                //
                tl.setNormals(nindex, 
                        ia[FACECOMPONENTINDEX_NORMAL],
                        ib[FACECOMPONENTINDEX_NORMAL],
                        ic[FACECOMPONENTINDEX_NORMAL]);
//                tl.setTextures(nindex, 
//                        ia[FACECOMPONENTINDEX_TEXTURE],
//                        ib[FACECOMPONENTINDEX_TEXTURE],
//                        ic[FACECOMPONENTINDEX_TEXTURE]);

                //System.arraycopy(ib, 0, ia, 0, 3);
                System.arraycopy(ic, 0, ib, 0, 3);
            } while (st.hasMoreTokens());
        } catch (NoSuchElementException e) {
            System.out.println("line " + line);
            throw e;
        }
    }

    private void parseFaceComponent(int[] ic, String stri) {
        String sa = stri.substring(0, stri.indexOf('/'));
        String sb = stri.substring(sa.length() + 1, stri.indexOf('/', sa
                .length() + 1));
        String sc = stri.substring(sa.length() + sb.length() + 2);
        int v = getInt(sa);
        int t = getInt(sb);
        int n = getInt(sc);
        ic[FACECOMPONENTINDEX_VECTOR] = v-1;
        ic[FACECOMPONENTINDEX_TEXTURE] = t-1;
        ic[FACECOMPONENTINDEX_NORMAL] = n-1;
    }

    private int getInt(String sa) {
    	if(sa==null) return -1;
    	if(sa.length()==0) return -1;
    	return Integer.valueOf(sa).intValue();
	}

	private void loadObject(String line) {
        obj = basenode.addObject3D();
        vl = obj.getVectorList();
        txl = obj.getTexturePointList();
        tl = obj.addTriangleList();
    }

}
