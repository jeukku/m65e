/*
 * Created on Jun 23, 2005
 */
package m65eII.tr;

import m65eII.Logger;
import m65eII.LoggerFactory;
import m65eII.engine.V;
import m65eII.engine.VectorList;

public class TVectorList implements VectorList {
	private float fs[] = new float[0];
	private int size = 0;
	private final int vertexsize = 3;
	private Logger log = LoggerFactory.createLogger(this);

	public int size() {
		return size;
	}

	public void copyTo(VectorList nvl) {
		log.debugpush("copyTo");
		nvl.reset();
		System.gc();
		for (int i = 0; i < size; i++) {
			int iv = 3 * i;
			nvl.addVector(new V(fs[iv + 0], fs[iv + 1], fs[iv + 2]));
			if (i % 200 == 0)
				Thread.yield();
		}
		log.pop();
	}

	public void reset() {
		size = 0;
	}

	private void grow() {
		int nsize = (int) (size * 1.5f);
		if (nsize == size)
			nsize++;
		resize(nsize);
	}

	private void resize(int nsize) {
		float nfs[] = new float[nsize * vertexsize];
		System.arraycopy(fs, 0, nfs, 0, fs.length);
		Runtime.getRuntime().gc();
		log.debugln("VertexList resized array from " + size + "["
				+ (size * vertexsize) + "] to " + nsize + "["
				+ (nsize * vertexsize) + "]");
		log.debugln("mem " + Runtime.getRuntime().freeMemory() / 1024 + "/"
				+ Runtime.getRuntime().totalMemory() / 1024);
		fs = nfs;
	}

	public void get(int i, V v) {
		final int vi = vertexsize * i;
		v.x = fs[vi];
		v.y = fs[vi + 1];
		v.z = fs[vi + 2];
	}

	public V addVector(V v) {
		int id = size;
		if ((id + 1) * vertexsize >= fs.length) {
			log.debugln("VertexList calling grow id:" + id + " vertexsize:"
					+ vertexsize + " fs.length:" + fs.length);
			grow();
		}
		set(id, v);
		size = id + 1;
		return v;
	}

	public V set(int id, V v) {
		int i = id * vertexsize;
		fs[i + 0] = v.x;
		fs[i + 1] = v.y;
		fs[i + 2] = v.z;
		return v;
	}
}
