package m65eII.tr;

import java.util.Hashtable;
import java.util.Vector;

import m65eII.engine.Camera;
import m65eII.engine.RenderTarget;
import m65eII.engine.Scene;
import m65eII.engine.TransformationNode;
import m65eII.engine.bsp.BSP;

public class TScene implements Scene {
	private Hashtable cameras;
	private TCamera current_camera;
	private TTransformationNode node_base;
	private Vector bsps;

	public BSP addBSP() {
		if(bsps==null) bsps = new Vector();
		BSP bsp = new TBSP();
		bsps.add(bsp);
		return bsp;
	}
	
	public void clear() {
		 cameras = new Hashtable();
		 node_base  = new TTransformationNode();
	}
	
	public void draw(TFrameDrawer fd) {
		TTransformation tr = new TTransformation();
		if (current_camera == null) {
			addCamera("default_cam");
		}
		tr.setCamera(current_camera);
		node_base.draw(fd, tr);
	}

	public TScene(String id) {
		clear();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Scene#addCamera(java.lang.String)
	 */
	public Camera addCamera(String id) {
		Camera cam = new TCamera();
		cameras.put(id, cam);
		if (current_camera == null) {
			selectCamera(id);
		}
		return cam;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Scene#getBaseNode()
	 */
	public TransformationNode getBaseNode() {
		return node_base;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Scene#selectCamera(java.lang.String)
	 */
	public Camera selectCamera(String id) {
		current_camera = (TCamera) cameras.get(id);
		return current_camera;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Scene#setRenderTarget(m65eII.engine.RenderTarget)
	 */
	public void setRenderTarget(RenderTarget rt) {
		// TODO Auto-generated method stub
	}
}