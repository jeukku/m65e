package m65eII.tr;

import java.util.Iterator;
import java.util.Vector;

import m65eII.engine.ColorList;
import m65eII.engine.Object3D;
import m65eII.engine.PointList;
import m65eII.engine.TriangleList;
import m65eII.engine.VectorList;

public class TObject3D implements Object3D {
	private Vector tls = new Vector();
	private TVectorList vl = new TVectorList();
	private TVectorList nl = new TVectorList();
	private TPointList txl = new TPointList();
	private TColorList cl = new TColorList();
	private String name;

	void draw(TFrameDrawer fd) {
		fd.setVectorList(vl);
		fd.setTexturePointList(txl);
		fd.setNormalList(nl);
		fd.setColorList(cl);
		for (int i = 0; i < tls.size(); i++) {
			TTriangleList tl = (TTriangleList) tls.elementAt(i);
			tl.draw(fd);
		}
	}

	public Iterator triangleLists() {
		return tls.iterator();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Object3D#addTriangleList()
	 */
	public TriangleList addTriangleList() {
		TriangleList t = new TTriangleList();
		tls.addElement(t);
		return t;
	}

	public VectorList getNormalList() {
		return nl;
	}

	public PointList getTexturePointList() {
		return txl;
	}

	public VectorList getVectorList() {
		return vl;
	}

	public ColorList getColorList() {
		return cl;
	}

	public void copyTo(Object3D o) {
		o.setName(getName());
		VectorList nvl = o.getVectorList();
		vl.copyTo(nvl);
		for (int i = 0; i < tls.size(); i++) {
			TriangleList ntl = o.addTriangleList();
			TriangleList tl = (TTriangleList) tls.elementAt(i);
			tl.copyTo(ntl);
			ntl.createNormals(nvl);
		}
	}

	/**
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            The name to set.
	 */
	public void setName(String nname) {
		this.name = nname;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return "TObject3D " + name;
	}
}