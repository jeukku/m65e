package m65eII.tr;

import m65eII.Logger;
import m65eII.LoggerFactory;
import m65eII.engine.ColorList;

public class TColorList implements ColorList {
	private float fs[] = new float[0];

	private int size = 0;

	private final int vertexsize = 3;
	private Logger log = LoggerFactory.createLogger(this);
	
	public int size() {
		return size;
	}
	
	public void copyTo(ColorList vl) {
		log.debugln("copyTo");
		vl.reset();
		System.gc();
		for (int i = 0; i < size; i++) {
			int iv = 3 * i;
			vl.addColor(fs[iv + 0], fs[iv + 1], fs[iv + 2]);
			if (i % 200 == 0)
				Thread.yield();
		}

	}

	public void reset() {
		size = 0;
	}

	private void grow() {
		int nsize = (int) (size * 1.5f);
		if (nsize == size)
			nsize++;
		resize(nsize);
	}

	private void resize(int nsize) {
		float nfs[] = new float[nsize * vertexsize];
		System.arraycopy(fs, 0, nfs, 0, fs.length);
		Runtime.getRuntime().gc();
		log.debugln("VertexList resized array from " + size + "["
				+ (size * vertexsize) + "] to " + nsize + "["
				+ (nsize * vertexsize) + "]");
		log.debugln("mem " + Runtime.getRuntime().freeMemory() / 1024 + "/"
				+ Runtime.getRuntime().totalMemory() / 1024);
		fs = nfs;
	}

	public void get(int i, C p) {
		final int vi = vertexsize * i;
		p.r = fs[vi];
		p.g = fs[vi + 1];
		p.b = fs[vi + 2];
	}

	public void addColor(float r, float g, float b) {
		int id = size;
		if ((id + 1) * vertexsize >= fs.length) {
			log.debugln("VertexList calling grow id:" + id + " vertexsize:"
					+ vertexsize + " fs.length:" + fs.length);
			grow();
		}
		set(id, r,g,b);
		size = id + 1;
	}
	
	public void add(C v) {
		addColor(v.r, v.g, v.b);
	}

	public void set(int id, C v) {
		set(id, v.r, v.g, v.b);
	}

	public void set(int id, float r, float g, float b) {
		int i = id * vertexsize;
		fs[i + 0] = r;
		fs[i + 1] = g;
		fs[i + 2] = b;
	}
}
