package m65eII.tr;

import m65eII.engine.Material;

public class TMaterial implements Material {
	private String texture;

	private boolean colorsenabled = true;

	public String getTexture() {
		return texture;
	}

	public boolean isTriangleColorsEnabled() {
		return colorsenabled;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Material#setTextureID(java.lang.String)
	 */
	public void setTexture(String id) {
		texture = id;
	}

	public void copyTo(Material mat) {
		mat.setTexture(texture);
	}
}