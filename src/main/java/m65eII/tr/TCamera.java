package m65eII.tr;

import m65eII.engine.Camera;
import m65eII.engine.Matrix;
import m65eII.engine.V;

public class TCamera implements Camera {
	private V location = new V();
	private V forward = new V();
	private V up = new V();
	private float near = 0.2f, far = 1000f;
	private Matrix m = new Matrix();
	private V p = new V();
	private V u = new V();
	private V v = new V();
	private V n = new V();

	public void setupViewPort(TViewPort viewport) {
		viewport.setNearAndFar(near, far);
		viewport.setRight(1);
		viewport.setTop(1);
	}

	/*
	 * private void setup_fustrum3D(TViewPort vp) { float vfov =
	 * vp.getSizeRatio() * fov;
	 * 
	 * vp.setFustrum(TViewPort.FUSTRUM_BOTTOM, new V(0, -(float) Math .cos(vfov *
	 * 1.0), (float) Math.sin(vfov), 0));
	 * 
	 * vp.setFustrum(TViewPort.FUSTRUM_TOP, new V(0, (float) Math .cos(vfov *
	 * 0.99), (float) Math.sin(vfov), 0)); vp.setFustrum(TViewPort.FUSTRUM_LEFT,
	 * new V((float) Math.cos(fov), 0, (float) Math.sin(fov), 0));
	 * vp.setFustrum(TViewPort.FUSTRUM_RIGHT, new V(-(float) Math.cos(fov), 0,
	 * (float) Math.sin(fov), 0));
	 * 
	 * vp.setFustrum(TViewPort.FUSTRUM_NEAR, new V(0, 0, 1, near));
	 * vp.setFustrum(TViewPort.FUSTRUM_FAR, new V(0, 0, -1, -far)); }
	 */
	//
	public Matrix getMatrix() {
		Matrix nm = new Matrix();
		nm.copy(m);
		return nm;
	}

	private void updateMatrix() {
		n.copy(forward);
		n.mult(-1);
		n.normalize();
		v = new V(0, 1, 0);
		float shadow = v.dot(n);
		V tmp = new V(n);
		tmp.mult(shadow);
		v.subtract(tmp);
		if (1e-3f > v.length())
			v = new V(0, 0, -n.y, 1);
		v.normalize();
		u = v.cross(n);
		p = new V(location);
		setPUVN(p, u, v, n);
	}

	private void setPUVN(V p, V u, V v, V n) {
		V pneg = new V(p);
		pneg.mult(-1);
		m.setRow(0, u.x, u.y, u.z, pneg.dot(u));
		m.setRow(1, v.x, v.y, v.z, pneg.dot(v));
		m.setRow(2, n.x, n.y, n.z, pneg.dot(n));
		m.setRow(3, 0, 0, 0, 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Camera#setFOV(float)
	 */
	public void setFOV(float fov) {
		updateMatrix();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Camera#setFrom(m65eII.engine.V)
	 */
	public void setLocation(V v) {
		location.copy(v);
		updateMatrix();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Camera#setForward(m65eII.engine.V)
	 */
	public void setForward(V v) {
		forward.copy(v);
		updateMatrix();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Camera#setTo(m65eII.engine.V)
	 */
	public void setTo(V v) {
		forward.copy(v);
		forward.subtract(location);
		forward.normalize();
		updateMatrix();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Camera#setUP(m65eII.engine.V)
	 */
	public void setUp(V v) {
		up.copy(v);
		updateMatrix();
	}

	/**
	 * @param f
	 */
	public void setFar(float f) {
		far = f;
	}

	/**
	 * @param f
	 */
	public void setNear(float f) {
		near = f;
	}
}