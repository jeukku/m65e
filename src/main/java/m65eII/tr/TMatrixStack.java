/*
 * Created on 7.2.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package m65eII.tr;

import m65eII.engine.Matrix;

/**
 * @author juuso
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TMatrixStack {
	private Matrix stack[] = new Matrix[256];

	private int istack;

	public TMatrixStack() {
		stack[0] = new Matrix();
		stack[0].identity();
	}

	void push(Matrix m) {
		// com.jv.GOutput.println("TMatrixStack pushin current before : " +
		// getCurrent().asString());

		if (stack[istack + 1] == null)
			stack[istack + 1] = new Matrix();
		stack[istack + 1].copy(stack[istack]);
		stack[istack + 1].mult(m);
		istack++;

		// com.jv.GOutput.println("TMatrixStack pushin current now : " +
		// getCurrent().asString());
	}

	Matrix pop() {
		istack--;
		return stack[istack];
	}

	Matrix getCurrent() {
		return stack[istack];
	}
}
