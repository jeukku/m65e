package m65eII.tr;

import java.util.Vector;

import m65eII.Logger;
import m65eII.LoggerFactory;
import m65eII.engine.Light;
import m65eII.engine.Matrix;
import m65eII.engine.Object3D;
import m65eII.engine.TransformationNode;

public class TTransformationNode implements TransformationNode {
	private Vector objs;
	private Vector nodes;
	private TTransformationNode parent;
	private Matrix matrix = new Matrix();
	private Logger log = LoggerFactory.createLogger(this);

	public Object3D findObject(String name) {
		log.debugpush("TTr findObject " + name);
		log.debugln("objs " + (objs != null ? objs.size() : -1));
		log.debugln("nodes " + (nodes != null ? nodes.size() : -1));
		try {
			if (objs != null)
				for (int i = 0; i < objs.size(); i++) {
					Object3D o = (Object3D) objs.elementAt(i);
					log.debugln("o " + i + " " + o);
					if (name.equals(o.getName())) {
						log.debugln("returing " + o);
						return o;
					}
				}
			if (nodes != null)
				for (int i = 0; i < nodes.size(); i++) {
					TTransformationNode node = (TTransformationNode) nodes
							.elementAt(i);
					Object3D ret = node.findObject(name);
					if (ret != null)
						return ret;
				}
			return null;
		} finally {
			log.pop();
		}
	}

	public TransformationNode getParent() {
		return parent;
	}

	public void copyTo(TransformationNode node) {
		log.debugpush(getClass().getName() + " copyTo " + node);
		for (int i = 0; i < objs.size(); i++) {
			Object3D no = node.addObject3D();
			Object3D obj = (Object3D) objs.elementAt(i);
			obj.copyTo(no);
		}
		for (int i = 0; nodes != null && i < nodes.size(); i++) {
			TransformationNode ntr = node.addNode();
			TransformationNode tr = (TransformationNode) nodes.elementAt(i);
			tr.copyTo(ntr);
		}
		log.pop();
	}

	void draw(TFrameDrawer fs, TTransformation tr) {
		tr.pushWorld(matrix);
		if (objs != null)
			for (int i = 0; i < objs.size(); i++) {
				TObject3D obj = (TObject3D) objs.elementAt(i);
				fs.setTransformation(tr);
				obj.draw(fs);
			}
		if (nodes != null)
			for (int i = 0; i < nodes.size(); i++) {
				TTransformationNode node = (TTransformationNode) nodes
						.elementAt(i);
				node.draw(fs, tr);
			}
		tr.popWorld();
	}

	public Light addLight() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.TransformationNode#addNode()
	 */
	public TransformationNode addNode() {
		TTransformationNode t = new TTransformationNode();
		t.parent = this;
		if (nodes == null)
			nodes = new Vector();
		nodes.addElement(t);
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.TransformationNode#addObject3D()
	 */
	public Object3D addObject3D() {
		if (objs == null)
			objs = new Vector();
		Object3D o = new TObject3D();
		objs.addElement(o);
		return o;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.TransformationNode#setTransformation(m65eII.engine.Matrix)
	 */
	public void setTransformation(Matrix m) {
		matrix = m;
	}
}