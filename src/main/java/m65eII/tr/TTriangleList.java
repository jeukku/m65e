package m65eII.tr;

import m65eII.Logger;
import m65eII.LoggerFactory;
import m65eII.engine.Material;
import m65eII.engine.TriangleList;
import m65eII.engine.V;
import m65eII.engine.VectorList;

public class TTriangleList implements TriangleList {
	private int tris[] = new int[3];
	private int ns[] = new int[3];
	private int ts[] = new int[3];
	private int cs[] = new int[3];
	// per triangle
	private V trins[] = new V[1];
	private int size = 0;
	private Material material = new TMaterial();
	private Logger log = LoggerFactory.createLogger(this);
	
	public int getColor(int i) {
		return cs[i];
	}

	public int[] getTris() {
		return tris;
	}

	public V[] getNs() {
		return trins;
	}

	public int[] getCs() {
		return cs;
	}

	public int size() {
		return size;
	}

	public void copyTo(TriangleList tl) {
		log.debugpush("copyTo");
		getMaterial().copyTo(tl.getMaterial());
		for (int i = 0; i < size; i += 3) {
			int a = tris[i + 0];
			int b = tris[i + 1];
			int c = tris[i + 2];
			tl.addTriangle(a, b, c);
			if (i % 300 == 0)
				Thread.yield();
		}
		log.debugln("copied");
		log.pop();
	}

	void draw(TFrameDrawer fd) {
		fd.setMaterial(material);
		fd.drawTriangleList(this);
	}

	public int addTriangle(int a, int b, int c) {
		if (tris.length <= size) {
			tris = grow(tris, 3);
		}
		int index = size;
		tris[size + 0] = a;
		tris[size + 1] = b;
		tris[size + 2] = c;
		size += 3;
		return index / 3;
	}

	public void setColors(int index, int a, int b, int c) {
		int ii = index * 3;
		if (cs.length <= ii)
			cs = grow(cs, 3);
		cs[ii + 0] = a;
		cs[ii + 1] = b;
		cs[ii + 2] = c;
	}

	public void setNormals(int index, int a, int b, int c) {
		int ii = index * 3;
		if (ns.length <= ii)
			ns = grow(ns, 3);
		ns[ii + 0] = a;
		ns[ii + 1] = b;
		ns[ii + 2] = c;
	}

	public void setTextures(int index, int a, int b, int c) {
		int ii = index * 3;
		if (ts.length <= ii)
			ts = grow(ts, 3);
		ts[ii + 0] = a;
		ts[ii + 1] = b;
		ts[ii + 2] = c;
	}

	private void setTriangleNormal(int i, V n) {
		if (trins.length <= i)
			trins = grow(trins, 1);
		if (trins[i] == null) {
			trins[i] = new V();
		}
		trins[i].copy(n);
	}

	//
	// private void grow() {
	// int nsize = (int) (1.1 * tris.length / 3) * 3;
	// if (nsize == tris.length) nsize += 3;
	// log.println("grow osoze:" + size + " nsize:" + nsize);
	// log.println("mem " + Runtime.getRuntime().freeMemory() / 1024 + "/"
	// + Runtime.getRuntime().totalMemory() / 1024);
	// tris = grow(tris, 3);
	// V nns[] = new V[nsize / 3];
	// System.arraycopy(trins, 0, nns, 0, size / 3);
	// trins = nns;
	// //
	// cs = grow(cs, 3);
	// ts = grow(ts, 3);
	// ns = grow(ns, 3);
	// }
	//
	private int[] grow(int orgis[], int mult) {
		int orgsize = orgis.length / mult;
		int nsize = (int) (1.1 * orgsize);
		if (orgsize == nsize)
			nsize *= 2;
		int nis[] = new int[nsize * mult];
		System.arraycopy(orgis, 0, nis, 0, orgsize * mult);
		return nis;
	}

	private V[] grow(V[] orgis, int mult) {
		int orgsize = orgis.length / mult;
		int nsize = (int) (1.1 * orgsize);
		if (orgsize == nsize)
			nsize *= 2;
		V nis[] = new V[nsize * mult];
		System.arraycopy(orgis, 0, nis, 0, orgsize * mult);
		return nis;
	}

	public Material getMaterial() {
		return material;
	}

	public void createNormals(VectorList vl) {
		V vbc = new V();
		V vb = new V();
		V vba = new V();
		for (int i = 0; i < size; i += 3) {
			vl.get(tris[i + 0], vba);
			vl.get(tris[i + 1], vb);
			vl.get(tris[i + 2], vbc);
			vba.subtract(vb);
			vbc.subtract(vb);
			V n = vba.cross(vbc);
			n.normalize();
			setTriangleNormal(i / 3, n);
		}
	}

}