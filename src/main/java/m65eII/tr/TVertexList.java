package m65eII.tr;

import m65eII.Logger;
import m65eII.LoggerFactory;
import m65eII.engine.P;
import m65eII.engine.V;
import m65eII.engine.VertexList;

public class TVertexList implements VertexList {
	private float fs[] = new float[0];
	private int size = 0;
	private boolean textures = false;
	private int textures_offset;
	private boolean normals = false;
	private int normals_offset;
	private int vertexsize = 5;
	private Logger log = LoggerFactory.createLogger(this);
	
	public int size() {
		return size;
	}

	public void copyTo(VertexList vl) {
		log.debugpush("copyTo");
		vl.reset();
		System.gc();
		for (int i = 0; i < size; i++) {
			int iv = vertexsize * i;
			vl.addVector(new V(fs[iv + 0], fs[iv + 1], fs[iv + 2]));
			if (i % 200 == 0)
				Thread.yield();
		}
		log.pop();
	}

	public void reset() {
		size = 0;
	}

	private void grow() {
		int nsize = (int) (size * 1.5f);
		if (nsize == size)
			nsize++;
		resize(nsize);
	}

	private void resize(int nsize) {
		float nfs[] = new float[nsize * vertexsize];
		System.arraycopy(fs, 0, nfs, 0, fs.length);
		Runtime.getRuntime().gc();
		log.debugln("VertexList resized array from " + size + "["
				+ (size * vertexsize) + "] to " + nsize + "["
				+ (nsize * vertexsize) + "]");
		log.debugln("mem " + Runtime.getRuntime().freeMemory() / 1024 + "/"
				+ Runtime.getRuntime().totalMemory() / 1024);
		fs = nfs;
	}

	private void check() {
		vertexsize = 3;
		if (normals) {
			normals_offset = vertexsize;
			vertexsize += 3;
		}
		if (textures) {
			textures_offset = vertexsize;
			vertexsize += 2;
		}
	}

	private void setOnNormals(boolean b) {
		normals = b;
		reSetVertexs();
	}

	private void setOnTextures(boolean b) {
		textures = b;
		reSetVertexs();
	}

	private void reSetVertexs() {
		int oldvertexsize = vertexsize;
		int t_offset = textures_offset;
		int n_offset = normals_offset;
		float old_fs[] = fs;
		check();
		fs = new float[size * vertexsize];
		for (int i = 0; i < size; i++) {
			int iv = vertexsize * i;
			int oldiv = oldvertexsize * i;
			int offset = 0;
			fs[iv + 0] = old_fs[i * oldvertexsize + 0];
			fs[iv + 1] = old_fs[i * oldvertexsize + 1];
			fs[iv + 2] = old_fs[i * oldvertexsize + 2];
			offset += 3;
			if (normals) {
				fs[iv + offset++] = old_fs[i * oldvertexsize + n_offset + 0];
				fs[iv + offset++] = old_fs[i * oldvertexsize + n_offset + 1];
				fs[iv + offset++] = old_fs[i * oldvertexsize + n_offset + 2];
			}
			if (textures) {
				fs[iv + offset++] = old_fs[i * oldvertexsize + t_offset + 0];
				fs[iv + offset++] = old_fs[i * oldvertexsize + t_offset + 1];
			}
		}
	}

	public void getVector(int i, V v) {
		final int vi = vertexsize * i;
		v.x = fs[vi];
		v.y = fs[vi + 1];
		v.z = fs[vi + 2];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.VertexList#addVector(m65eII.engine.V)
	 */
	public V addVector(V v) {
		int id = size;
		if ((id + 1) * vertexsize >= fs.length) {
			log.debugln("VertexList calling grow id:" + id + " vertexsize:"
					+ vertexsize + " fs.length:" + fs.length);
			grow();
		}
		setVector(id, v);
		size = id + 1;
		return v;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.VertexList#setNormal(int, m65eII.engine.V)
	 */
	public V setNormal(int id, V v) {
		if (!normals)
			setOnNormals(true);
		if ((id + 1) * vertexsize >= fs.length)
			grow();
		int i = id * vertexsize;
		fs[i + normals_offset + 0] = v.x;
		fs[i + normals_offset + 1] = v.y;
		fs[i + normals_offset + 2] = v.z;
		return v;
	}

	public void getTexture(int i, P p) {
		final int vi = vertexsize * i + textures_offset;
		p.u = fs[vi];
		p.v = fs[vi + 1];
	}

	public P setTexture(int id, P p) {
		if (!textures)
			setOnTextures(true);
		if ((id + 1) * vertexsize >= fs.length)
			grow();
		int i = id * vertexsize;
		fs[i + textures_offset + 0] = p.u;
		fs[i + textures_offset + 1] = p.v;
		return p;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.VertexList#setVector(int, m65eII.engine.V)
	 */
	public V setVector(int id, V v) {
		int i = id * vertexsize;
		fs[i + 0] = v.x;
		fs[i + 1] = v.y;
		fs[i + 2] = v.z;
		return v;
	}
}