/*
 * Created on 11.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package m65eII.tr;

import m65eII.engine.TimeStep;

/**
 * @author juuso
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class TimeFrame {
	private long time_lastframe;

	private long time_start;

	public TimeStep getStep() {
		if (time_lastframe == 0) {
			time_lastframe = System.currentTimeMillis();
			time_start = time_lastframe;
		}

		long time_now = System.currentTimeMillis();
		long dt = time_now - time_lastframe;
		time_lastframe = time_now;

		float sec = (time_now - time_start) / 1000.0f;
		float dsec = dt / 1000.0f;

		TimeStep step = new TimeStep(sec, dsec);
		return step;
	}
}
