/*
 * Created on 20.7.2004
 *
 */
package m65eII.tr;

import m65eII.engine.V;

/**
 * @author juuso
 * 
 */
public class TViewPort {
	private float near, far;
	private int w, h;
	private float right;
	private float hfus;
	private final V fustrums[] = new V[6];
	public final static int FUSTRUM_NEAR = 0;
	public final static int FUSTRUM_FAR = 1;
	public final static int FUSTRUM_LEFT = 2;
	public final static int FUSTRUM_RIGHT = 3;
	public final static int FUSTRUM_TOP = 4;
	public final static int FUSTRUM_BOTTOM = 5;

	public TViewPort() {
		for (int i = 0; i < 6; i++)
			fustrums[i] = new V();
		setNearAndFar(1f, 1000.0f);
	}

	public void setTop(float top) {
		updateHFus();
	}

	public void setRight(float right) {
		this.right = right;
		updateHFus();
	}

	private void updateHFus() {
		hfus = w / right;
	}

	public void setNearAndFar(float nnear, float nfar) {
		near = nnear;
		far = nfar;
		updateHFus();
	}

	public void setSize(int width, int height) {
		if (width == 0 || height == 0) {
			throw new IllegalArgumentException("TViewPort setSize w:" + w
					+ " h:" + h);
		}
		w = width;
		h = height;
		updateHFus();
	}

	float getSizeRatio() {
		try {
			return h / w;
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

	void setFustrum(int fustrum, V v) {
		fustrums[fustrum].copy(v);
	}

	public void screenTransform(V v) {
		float z = v.z;
		float y = v.y;
		float x = v.x;
		y /= z;
		y *= hfus;
		y += h / 2;
		x /= z;
		x *= hfus;
		x += w / 2;
		z += near;
		z /= (far - near);
		v.x = x;
		v.y = y;
		v.z = z;
	}

	public int check(V va, V vb, V vc) {
		int ret = 0;
		ret|=inside(va)<<0;
		ret|=inside(vb)<<1;
		ret|=inside(vc)<<2;
		return ret;
	}

	private final int inside(V va) {
		if(va.z<-near) return 1;
		return 0;
	}
}
