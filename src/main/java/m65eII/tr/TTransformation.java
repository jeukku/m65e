package m65eII.tr;

import m65eII.engine.Matrix;

/**
 * 
 * @author juuso Eri transformaatio pinot
 */
public class TTransformation {
	private TMatrixStack world = new TMatrixStack();

	private TCamera camera;

	public void setupViewport(TViewPort viewPort) {
		camera.setupViewPort(viewPort);
	}

	/**
	 * Palauttaa senhetkisen View-transformaation eli Kamera matriisista ja
	 * World-matriisista Matriisin.
	 * 
	 * @return
	 */
	public Matrix getViewTransformation() {
		Matrix camm = camera.getMatrix();
		Matrix worldm = world.getCurrent();

		Matrix m = new Matrix();
		m.copy(camm);
		m.mult(worldm);
		return m;
	}

	public Matrix getCurrentWorld() {
		return world.getCurrent();
	}

	public Matrix getInverseWorld() {
		Matrix m = getCurrentWorld();
		m.inverse();

		return m;
	}

	public void pushWorld(Matrix m) {
		world.push(m);
	}

	public void popWorld() {
		world.pop();
	}

	public void setCamera(TCamera cam) {
		camera = cam;
	}
}