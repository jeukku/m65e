/*
 * Created on Jun 23, 2005
 */
package m65eII.tr;

import m65eII.Logger;
import m65eII.LoggerFactory;
import m65eII.engine.P;
import m65eII.engine.PointList;
import m65eII.engine.V;
import m65eII.engine.VertexList;

public class TPointList implements PointList {
	private float fs[] = new float[0];

	private int size = 0;

	private final int vertexsize = 2;
	private Logger log = LoggerFactory.createLogger(this);
	
	public int size() {
		return size;
	}

	public void copyTo(VertexList vl) {
		log.debugpush("copyTo");
		vl.reset();
		System.gc();
		for (int i = 0; i < size; i++) {
			int iv = 3 * i;
			vl.addVector(new V(fs[iv + 0], fs[iv + 1], fs[iv + 2]));
			if (i % 200 == 0)
				Thread.yield();
		}
		log.pop();
	}

	public void reset() {
		size = 0;
	}

	private void grow() {
		int nsize = (int) (size * 1.5f);
		if (nsize == size)
			nsize++;
		resize(nsize);
	}

	private void resize(int nsize) {
		float nfs[] = new float[nsize * vertexsize];
		System.arraycopy(fs, 0, nfs, 0, fs.length);
		Runtime.getRuntime().gc();
		log.debugln("VertexList resized array from " + size + "["
				+ (size * vertexsize) + "] to " + nsize + "["
				+ (nsize * vertexsize) + "]");
		log.debugln("mem " + Runtime.getRuntime().freeMemory() / 1024 + "/"
				+ Runtime.getRuntime().totalMemory() / 1024);
		fs = nfs;
	}

	public void get(int i, P p) {
		final int vi = vertexsize * i;
		p.u = fs[vi];
		p.v = fs[vi + 1];
	}

	public void add(P v) {
		int id = size;
		if ((id + 1) * vertexsize >= fs.length) {
			log.debugln("VertexList calling grow id:" + id + " vertexsize:"
					+ vertexsize + " fs.length:" + fs.length);
			grow();
		}
		set(id, v);
		size = id + 1;
	}

	public void set(int id, P v) {
		int i = id * vertexsize;
		fs[i + 0] = v.u;
		fs[i + 1] = v.v;
	}
}
