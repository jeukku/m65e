/*
 * Created on 21.6.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package m65eII.tr;

import m65eII.engine.ColorList;
import m65eII.engine.Material;
import m65eII.engine.PointList;
import m65eII.engine.RenderTarget;
import m65eII.engine.VectorList;

/**
 * @author juuso
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface TFrameDrawer {
	void setRenderTarget(RenderTarget rt);

	void setMaterial(Material mat);

	void setVectorList(VectorList vl);

	void drawTriangleList(TTriangleList tl);

	void setTransformation(TTransformation tr);

	void setTexturePointList(PointList txl);

	void setNormalList(VectorList nl);

	void setColorList(ColorList cl);
}
