/*
 * Created on Jun 20, 2005
 */
package m65eII.rigidbody;

import m65eII.engine.V;

public interface Force {

    void getForces(State state, float t, V nforce, V ntorque);
}
