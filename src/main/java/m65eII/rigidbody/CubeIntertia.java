/*
 * Created on Jun 21, 2005
 */
package m65eII.rigidbody;

public class CubeIntertia implements InertiaProvider {
    private float inertia;
    private float size = 1;
    
    public float getInertia(float mass) {
        inertia = 1.0f / 6 * size * mass;
        return inertia;
    }

    public float getInvIntertia(float mass) {
        return 1.0f/getInertia(mass);
    }
}
