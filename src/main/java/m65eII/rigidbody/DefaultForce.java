/*
 * Created on Jun 20, 2005
 */
package m65eII.rigidbody;

import m65eII.engine.V;

public class DefaultForce implements Force {
    private V f;
    private float st, et;
    
    public void getForces(State state, float t, V nforce, V ntorque) {
        if(t>st && t<et) {
            nforce.add(f);
        }
    }
    
    public DefaultForce(V v, float st, float et) {
        f = new V(v);
        this.st = st;
        this.et = et;
    }
}
