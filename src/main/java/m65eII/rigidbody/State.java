package m65eII.rigidbody;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import m65eII.engine.Matrix;
import m65eII.engine.Q;
import m65eII.engine.V;

public class State {
    // primary
    private V x = new V(); // position
    private V m = new V(); // momentum ... siis liike-energia
    private Q o = Q.getRotation(0, 0, 0, 1); // orientation... siis asento
    private V angular_m = new V();
    // secondary
    private V v = new V();
    private Q spin = Q.getRotation(0, 0, 0, 1);
    private V angular_v = new V();
    // static
    float mass, invmass;
    float intertia, invinertia;
    private InertiaProvider inertiaprov = new CubeIntertia();
    private List forces = new LinkedList();

    private void recalculate() {
        v.set(m);
        v.mult(invmass);
        //
        angular_v.set(angular_m);
        angular_v.mult(invinertia);
        //
        spin = new Q(0, angular_v.x, angular_v.y, angular_v.z);
        spin.mult(0.5f);
        spin.mult(o);
        
    }

    public void integrate(float t, float dt) {
        Derivative a = evaluate(t); // note: overloaded 'evaluate' just returns
        // derivative at current time 't'
        Derivative b = evaluate(t, dt * 0.5f, a);
        Derivative c = evaluate(t, dt * 0.5f, b);
        Derivative d = evaluate(t, dt, c);
        //
        V dxdt = integrate(a.velocity, b.velocity, c.velocity, d.velocity);
        dxdt.mult(dt);
        x.add(dxdt);
        //
        V dfdt = integrate(a.force, b.force, c.force, d.force);
        dfdt.mult(dt);
        m.add(dfdt);
        //
        Q dodt = integrate(a.spin, b.spin, c.spin, d.spin);
        dodt.mult(dt);
        o.add(dodt);
        o.normalize();
         //System.out.println("o: " + o);
        //
        V dmdt = integrate(a.torque, b.torque, c.torque, d.torque);
        dmdt.mult(dt);
        angular_m.add(dmdt);
        //
    }

    private Q integrate(Q a, Q b, Q c, Q d) {
        Q qdt = new Q();
        qdt.add(b);
        qdt.add(c);
        qdt.mult(2);
        qdt.add(a);
        qdt.add(d);
        qdt.mult(1.0f / 6.0f);
        return qdt;
    }

    private V integrate(V a, V b, V c, V d) {
        V dxdt = new V();
        dxdt.add(b);
        dxdt.add(c);
        dxdt.mult(2);
        dxdt.add(a);
        dxdt.add(d);
        dxdt.mult(1.0f / 6.0f);
        return dxdt;
    }

    private Derivative evaluate(float t) {
        State state = new State();
        state.setMass(mass);
        state.x = new V(x);
        state.m = new V(m);
        state.o = new Q(o);
        state.angular_m = new V(angular_m);
        Derivative output = new Derivative();
        output.velocity.set(v);
        output.spin.set(spin);
        forces(state, t, output.force, output.torque);
        return output;
    }

    private Derivative evaluate(float t, float dt, Derivative d) {
        State state = new State();
        state.setMass(mass);
        state.x.set(d.velocity);
        state.x.mult(dt);
        state.x.add(x);
        //
        state.m = new V(d.force);
        state.m.mult(dt);
        state.m.add(m);
        //
        state.angular_m = new V(d.torque);
        state.angular_m.mult(dt);
        state.angular_m.add(angular_m);
        //
        state.o = new Q(d.spin);
        state.o.mult(dt);
        state.o.add(o);
        //
        state.recalculate();
        //
        Derivative output = new Derivative();
        output.velocity.set(state.v);
        output.spin.set(state.spin);
        forces(state, t + dt, output.force, output.torque);
        return output;
    }

    private void forces(State state, float t, V force, V torque) {
        Iterator i = forces.iterator();
        V nforce = new V();
        V ntorque = new V();
        while (i.hasNext()) {
            Force f = (Force) i.next();
            nforce.zero();
            ntorque.zero();
            f.getForces(state, t, nforce, ntorque);
            force.add(nforce);
            torque.add(ntorque);
        }
    }

    public String toString() {
        String s = "";
        s += "x:" + x;
        s += "\nq:" + o;
        return s;
    }

    public void setMass(float mass2) {
        mass = mass2;
        invmass = 1.0f / mass;
        intertia = inertiaprov.getInertia(mass);
        invinertia = inertiaprov.getInvIntertia(mass);
        //
        recalculate();
    }

    public V getLocation() {
        return new V(x);
    }

    public V getVelocity() {
        return new V(v);
    }

    public void setVelocity(V nv) {
        m = new V(nv);
        m.mult(mass);
        recalculate();
    }

    public void setLocation(V nv) {
        x = new V(nv);
    }

    public void doCollide(V dist, State state) {
        // TODO Auto-generated method stub
    }

    public Matrix getTransformationMatrix() {
        Matrix m = new Matrix();
        m.translate(x.x, x.y, x.z);
        m.mult(o.toMatrix());
        return m;
    }

    public void addForce(Force f) {
        forces.add(f);
    }
}
