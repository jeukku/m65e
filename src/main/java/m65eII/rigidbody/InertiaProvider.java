/*
 * Created on Jun 21, 2005
 */
package m65eII.rigidbody;

public interface InertiaProvider {

    float getInertia(float mass);

    float getInvIntertia(float mass);
}
