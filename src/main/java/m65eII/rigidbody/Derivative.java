/*
 * Created on Jun 20, 2005
 */
package m65eII.rigidbody;

import m65eII.engine.Q;
import m65eII.engine.V;

public class Derivative {
    final V velocity = new V(); // derivate of state.x
    final V force = new V(); // derivate of state.m
    final Q spin = new Q(); // derivate of state.o
    final V torque = new V(); // derivate of state.angular_m
}
