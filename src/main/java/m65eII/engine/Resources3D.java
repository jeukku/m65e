package m65eII.engine;

import java.awt.Image;

import m65eII.resources.GeometryLoader;

public interface Resources3D {
	Image getImage(String name);
	GeometryLoader getObject(String name);
	//Resources3D loadResources3D(String string);
}
