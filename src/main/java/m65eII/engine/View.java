package m65eII.engine;

import java.awt.Container;

import com.jv.pixelgr.SCanvasMouseListener;

public interface View {
	void attachTo(Container cont);
	
	void startRender();
	void endRender();
	
	boolean isVisible();
	void setClear(boolean b);
    
    void stop();
    
	void addMouseListener(SCanvasMouseListener l);

	void setUpdater(Runnable runnable);

	void repaint();

	int getWidth();

	int getHeight();	
}