package m65eII.engine;

import m65eII.tr.C;

public interface ColorList {

	void reset();

	void addColor(float f, float g, float h);

	void get(int i, C ca);
}
