
/*copy
 * Created on 21.6.2004
 *
 */
package m65eII.engine;

/**
 * @author juuso
 *
 */
public class Matrix {
	private final float array[] = new float[16];
	private final float apuarray[] = new float[16];

	public float[] getArray() {
	    return array;
	}
	
	public void inverse() {
		for (int m = 0; m < 4; m++) {
			for (int n = 0; n < 4; n++) {
				apuarray[n * 4 + m] = array[m * 4 + n];
			}
		}

		System.arraycopy(apuarray, 0, array, 0, 16);
	}
	
	public final static Matrix getPerspectiveProjection(
		float left,
		float right,
		float top,
		float bottoMatrix,
		float near,
		float far) {
		Matrix matrix = new Matrix();
		matrix.perspectiveProjection(left, right, top, bottoMatrix, near, far);
		return matrix;
	}

	public void setRow(int irow, float a, float b, float c, float d) {
		array[irow * 4 + 0] = a;
		array[irow * 4 + 1] = b;
		array[irow * 4 + 2] = c;
		array[irow * 4 + 3] = d;
	}

	public Matrix(V x, V y, V z) {
		array[0 + 0] = x.x;
		array[1 + 0] = x.y;
		array[2 + 0] = x.z;
		array[0 + 4] = y.x;
		array[1 + 4] = y.y;
		array[2 + 4] = y.z;
		array[0 + 8] = z.x;
		array[1 + 8] = z.y;
		array[2 + 8] = z.z;
		array[15] = 1;
	}

	/**
	
	* put your docuMatrixentation coMatrixMatrixent here
	
	* @return
	
	*/

	public String toString() {
		String s = new String("");

		for (int l = 0; l < 16; l += 4) {
			s += "|" + round(array[l], 2) + ' ';
			s += "|" + round(array[l + 1], 2) + ' ';
			s += "|" + round(array[l + 2], 2) + ' ';
			s += "|" + round(array[l + 3], 2) + ' ';
		}
		s += '\n';

		/*
		s += "|" + round(array[0], 2) + "\t" + round(array[1], 2) + "\t" + round(array[2], 2) + "\t" + round(array[3], 2) + "|\n";
		s += "|" + round(array[4], 2) + "\t" + round(array[5], 2) + "\t" + round(array[6], 2) + "\t" + round(array[7], 2) + "|\n";
		s += "|" + round(array[8], 2) + "\t" + round(array[9], 2) + "\t" + round(array[10], 2) + "\t" + round(array[11], 2) + "|\n";
		s += "|" + round(array[12], 2) + "\t" + round(array[13], 2) + "\t" + round(array[14], 2) + "\t" + round(array[15], 2) + "|\n";
		*/
		return s;
	}

	/**
	
	* put your docuMatrixentation coMatrixMatrixent here
	
	* @return
	
	*/

	public String asStringLines(int tabs) {
		String s = new String();

		for (int l = 0; l < 16; l += 4) {
			for (int tl = 0; tl < tabs; tl++)
				s += '\t';
			s += "|" + round(array[l], 2) + ' ';
			s += "|" + round(array[l + 1], 2) + ' ';
			s += "|" + round(array[l + 2], 2) + ' ';
			s += "|" + round(array[l + 3], 2) + '\n';
		}

		/*
		s += "|" + round(array[0], 2) + "\t" + round(array[1], 2) + "\t" + round(array[2], 2) + "\t" + round(array[3], 2) + "|\n";
		s += "|" + round(array[4], 2) + "\t" + round(array[5], 2) + "\t" + round(array[6], 2) + "\t" + round(array[7], 2) + "|\n";
		s += "|" + round(array[8], 2) + "\t" + round(array[9], 2) + "\t" + round(array[10], 2) + "\t" + round(array[11], 2) + "|\n";
		s += "|" + round(array[12], 2) + "\t" + round(array[13], 2) + "\t" + round(array[14], 2) + "\t" + round(array[15], 2) + "|\n";
		*/
		return s;
	}

	public void copy(Matrix m) {
		for (int las = 0; las < 16; las++)
			array[las] = m.array[las];
	}

	public boolean equals(Matrix m) {
		boolean equals = true;

		for (int l = 0; l < array.length; l++)
			if (m.array[l] != array[l])
				equals = false;
		return equals;
	}

	public void fromatrixVectors(V x, V y, V z) {
		array[0] = x.x;
		array[1] = x.y;
		array[2] = x.z;
		array[3] = 0;

		array[4] = y.x;
		array[5] = y.y;
		array[6] = y.z;
		array[7] = 0;

		array[8] = z.x;
		array[9] = z.y;
		array[10] = z.z;
		array[11] = 0;

		array[12] = 0;
		array[13] = 0;
		array[14] = 0;
		array[15] = 1;
	}

	public void identity() {
		zero();
		array[0] = 1;
		array[5] = 1;
		array[10] = 1;
		array[15] = 1;
	}

	public void mirrorX(float x) {
		identity();
		array[0] = -1;
		array[12] = x;
	}

	public void mirrorY(float y) {
		identity();
		array[5] = -1;
		array[13] = y;
	}

	public void mirrorZ(float z) {
		identity();
		array[10] = -1;
		array[14] = z;
	}


	public void transpose() {
		for (int k = 0; k < 4; k++)
			for (int j = 0; j < 4; j++) {
				apuarray[j * 4 + k] = array[k * 4 + j];
			}

		System.arraycopy(apuarray, 0, array, 0, array.length);
	}

	public void mult(Matrix b) {
		//    coMatrix.jv.GOutput.println("Matrixulting Matrixatrices \nA:" +asStringLines()+"\nB:" + b.asStringLines());
		/*
		*/
		for (int k = 0; k < 4; k++)
			for (int j = 0; j < 4; j++) {
				apuarray[j * 4 + k] =
					array[j * 4] * b.array[k]
						+ array[j * 4
						+ 1] * b.array[k
						+ 4]
						+ array[j * 4
						+ 2] * b.array[k
						+ 8]
						+ array[j * 4
						+ 3] * b.array[k
						+ 12];
			}
		/*
		for (int k = 0; k < 4; k++) 
		for (int j = 0; j < 4; j++) {
			apuarray[k * 4 + j] = array[k * 4] * b.array[j] + array[k * 4 + 1] * b.array[j + 4] + array[k * 4
			+ 2] * b.array[j + 8] + array[k * 4 + 3] * b.array[j + 12];
		}
		*/

		for (int las = 0; las < 16; las++)
			array[las] = apuarray[las];
		//    coMatrix.jv.GOutput.println("\nthis:" +asStringLines());
		/*
		Matrix.array[0]= array[0]*b.array[0] + array[1]*b.array[4] + array[2]*b.array[8] + array[3]*b.array[12];
		Matrix.array[1]= array[0]*b.array[1] + array[1]*b.array[5] + array[2]*b.array[9] + array[3]*b.array[13];
		Matrix.array[2]= array[0]*b.array[2] + array[1]*b.array[6] + array[2]*b.array[10] + array[3]*b.array[14];
		Matrix.array[3]= array[0]*b.array[3] + array[1]*b.array[7] + array[2]*b.array[11] + array[3]*b.array[15];
		Matrix.array[4]= array[4]*b.array[0] + array[5]*b.array[4] + array[6]*b.array[8] + array[7]*b.array[12];
		Matrix.array[5]= array[4]*b.array[1] + array[5]*b.array[5] + array[6]*b.array[9] + array[7]*b.array[13];
		Matrix.array[6]= array[4]*b.array[2] + array[5]*b.array[6] + array[6]*b.array[10] + array[7]*b.array[14];
		Matrix.array[7]= array[4]*b.array[3] + array[5]*b.array[7] + array[6]*b.array[11] + array[7]*b.array[15];
		Matrix.array[8]= array[8]*b.array[0] + array[9]*b.array[4] + array[10]*b.array[8] + array[11]*b.array[12];
		Matrix.array[9]= array[8]*b.array[1] + array[9]*b.array[5] + array[10]*b.array[9] + array[11]*b.array[13];
		Matrix.array[10]= array[8]*b.array[2] + array[9]*b.array[6] + array[10]*b.array[10] + array[11]*b.array[14];
		Matrix.array[11]= array[8]*b.array[3] + array[9]*b.array[7] + array[10]*b.array[11] + array[11]*b.array[15];
		Matrix.array[12]= array[12]*b.array[0] + array[13]*b.array[4] + array[14]*b.array[8] + array[15]*b.array[12];
		Matrix.array[13]= array[12]*b.array[1] + array[13]*b.array[5] + array[14]*b.array[9] + array[15]*b.array[13];
		Matrix.array[14]= array[12]*b.array[2] + array[13]*b.array[6] + array[14]*b.array[10] + array[15]*b.array[14];
		Matrix.array[15]= array[12]*b.array[3] + array[13]*b.array[7] + array[14]*b.array[11] + array[15]*b.array[15];
		*/
	}

	public void mult(Matrix a, Matrix b) {
		copy(a);
		mult(b);
	}

	final public void mult(final V v) {
		final float vx, vy, vz, vw;

		vx = v.x;
		vy = v.y;
		vz = v.z;
		vw = v.w;
		v.x = vx * array[0] + vy * array[1] + vz * array[2] + vw * array[3];
		v.y = vx * array[4] + vy * array[5] + vz * array[6] + vw * array[7];
		v.z = vx * array[8] + vy * array[9] + vz * array[10] + vw * array[11];
		v.w = vx * array[12] + vy * array[13] + vz * array[14] + vw * array[15];
	}

	final public void multw0(final V v) {
		final float vx, vy, vz;

		vx = v.x;
		vy = v.y;
		vz = v.z;
		v.x = vx * array[0] + vy * array[1] + vz * array[2];
		v.y = vx * array[4] + vy * array[5] + vz * array[6];
		v.z = vx * array[8] + vy * array[9] + vz * array[10];
		v.w = 0;
	}

	final public void multw1(final V v) {
		final float vx, vy, vz;

		vx = v.x;
		vy = v.y;
		vz = v.z;
		v.x = vx * array[0] + vy * array[1] + vz * array[2] + array[3];
		v.y = vx * array[4] + vy * array[5] + vz * array[6] + array[7];
		v.z = vx * array[8] + vy * array[9] + vz * array[10] + array[11];
		/*
		v.tx = vx * array[0] + vy * array[1] + vz * array[2] + array[3];
		v.ty = vx * array[4] + vy * array[5] + vz * array[6] + array[7];
		v.tz = vx * array[8] + vy * array[9] + vz * array[10] + array[11];
		*/
		//    w = vx*array[3] + vy*array[7] + vz*array[11] + array[15];
		v.w = 1;
	}

	public static Matrix getOrthographicProjection(
		float left,
		float right,
		float top,
		float bottoMatrix,
		float near,
		float far) {
		Matrix m = new Matrix();
		m.orthographicProjection(left, right, top, bottoMatrix, near, far);
		return m;
	}

	public void orthographicProjection(
		float left,
		float right,
		float top,
		float bottoMatrix,
		float near,
		float far) {
		identity();

		array[0] = 2 / (right - left);
		array[1] = 0;
		array[2] = 0;
		array[3] = - (right + left) / (right - left);

		array[4] = 0;
		array[5] = 2 * (top - bottoMatrix);
		array[6] = 0;
		array[7] = - (top + bottoMatrix) / (top - bottoMatrix);

		array[8] = 0;
		array[9] = 0;
		array[10] = -2 / (far - near);
		array[11] = - (far + near) / (far - near);

		array[12] = 0;
		array[13] = 0;
		array[14] = 0;
		array[15] = 1;

	}

	public void perspectiveProjection(
		float l,
		float r,
		float t,
		float b,
		float n,
		float f) {

		setRow(0, 2 * n * (r - l), 0, (r + l) / (r - l), 0);
		setRow(1, 0, 2 * n / (t - b), (t + b) / (t - b), 0);
		setRow(2, 0, 0, (f + n) / (n - f), 2 * f * n / (n - f));
		setRow(3, 0, 0, -1, 0);
	}

	public void rotAroundVector(V u, float angle) {
		float cosa = (float) Math.cos(angle);
		float sina = (float) Math.sin(angle);

		array[0] = (1 - cosa) * u.x * u.x + cosa;
		array[1] = (1 - cosa) * u.y * u.x - sina * u.z;
		array[2] = (1 - cosa) * u.z * u.x + sina * u.y;
		array[3] = 0;

		array[4] = (1 - cosa) * u.x * u.y + sina * u.z;
		array[5] = (1 - cosa) * u.y * u.y + cosa;
		array[6] = (1 - cosa) * u.z * u.y - sina * u.x;
		array[7] = 0;

		array[8] = (1 - cosa) * u.x * u.y - sina * u.y;
		array[9] = (1 - cosa) * u.y * u.z + sina * u.x;
		array[10] = (1 - cosa) * u.z * u.z + cosa;
		array[11] = 0;

		array[12] = 0;
		array[13] = 0;
		array[14] = 0;
		array[15] = 1;
	}

	public void rotX(float angle) {
		identity();
		float ca = (float) Math.cos(angle);
		float sa = (float) Math.sin(angle);

		array[5] = ca;
		array[6] = -sa;
		array[9] = sa;
		array[10] = ca;
	}

	public void rotY(float angle) {
		identity();
		float ca = (float) Math.cos(angle);
		float sa = (float) Math.sin(angle);

		array[0] = ca;
		array[2] = sa;
		array[8] = -sa;
		array[10] = ca;
	}

	public void rotZ(float angle) {
		identity();
		float ca = (float) Math.cos(angle);
		float sa = (float) Math.sin(angle);

		array[0] = ca;
		array[1] = -sa;
		array[4] = sa;
		array[5] = ca;
	}

	float round(float d, int i) {
		return ((int) (d * Math.pow(10, i))) / (float) Math.pow(10, i);
	}

	public void scale(float x, float y, float z) {
		identity();
		array[0] = x;
		array[5] = y;
		array[10] = z;
		array[15] = 1;
	}

	public void set(Matrix Matrix) {
		copy(Matrix);
	}

	public void set(int j, int k, float f) {
	    array[j * 4 + k] = f; // TODO varmasti v????rin p??in j ja k??
 
	}
	
	public void translate(float x, float y, float z) {
		identity();
		array[3] = x;
		array[7] = y;
		array[11] = z;
	}

	public void translate(V v) {
		translate(v.x, v.y, v.z);
	}

	public void view(
		float w,
		float h,
		float clipw,
		float cliph,
		float clipx,
		float clipy) {
		identity();
		array[0] = w / clipw;

		/*
		array[1] = 0;
		array[2] = 0;
		array[3] = 0;
		array[4] = 0;
		*/
		array[5] = -w / cliph;

		/*
		array[6] = 0;
		array[7] = 0;
		array[8] = 0;
		array[9] = 0;
		*/
		array[10] = 1;
		//        array[11] = 0;
		array[12] = -clipx * w / clipw;
		array[13] = -clipy * h / cliph;
		//        array[14] = 0;
		array[15] = 1;
	}

	public void zero() {
		for (int l = 0; l < 16; l++)
			array[l] = 0;
	}

	public Matrix() {
		identity();
	}
}
