/*
 * Created on Apr 11, 2003
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code Template
 */
package m65eII.engine;

import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * @author juuso
 */
public class TimeMeasure {
	private Hashtable ht = new Hashtable();
	private Block totalblock = new Block();
	private String name;
	
	private long lastPrint, printDelay = 2000;
	
	private int totalcount;
	private float hz;
	
	PrintStream out = m65eII.System.out;
	
    public float getHz() {
        return hz;
    }
    
	public TimeMeasure(String name ){
		this.name = name;	
	}
	
	public void setTotalStartTime() {
		totalblock.st = System.currentTimeMillis();
	}

	public void setTotalEndTime() {
		totalblock.et = System.currentTimeMillis();
		totalblock.total += totalblock.et-totalblock.st;
		totalcount++;
		
		long dt = System.currentTimeMillis() - lastPrint;
		hz = 1000.0f*totalcount/dt;
		
		if(System.currentTimeMillis()-lastPrint > printDelay) {
			out.println(toString());
			lastPrint = System.currentTimeMillis();
			reset();
		}		
	}
	
	public void setStartTime(Object o) {
		if(ht.get(o)==null) ht.put(o, new Block());
		((Block)ht.get(o)).st = System.currentTimeMillis();
	}

	public void setEndTime(Object o) {
		if(ht.get(o)==null) return;
		Block b = (Block)ht.get(o); 
		b.et = System.currentTimeMillis();
		b.total+=b.et-b.st;		
	}
	
	void reset() {
		totalblock.st = 0;
		totalblock.et = 0;
		totalblock.total = 0;
		
		totalcount = 0;
		
		Enumeration keys = ht.keys();
		while(keys.hasMoreElements()) {
			Block b = (Block)ht.get(keys.nextElement());
			b.st = 0;
			b.et = 0;
			b.total = 0;
		}
	}
	
	long getTotal() {
		long total = 0;
		Enumeration keys = ht.keys();
		while(keys.hasMoreElements()) {
			Block b = (Block)ht.get(keys.nextElement());
			total+=b.total;
		}
		return total;
	}
	
	public String toString() {
		long total = totalblock.total;
		if(total==0) return "total 0";
				
		String s = "TM - " + name + " total:"+total + " hz:"+hz +"\n";
		Enumeration keys = ht.keys();
		while(keys.hasMoreElements()) {
			Object key = keys.nextElement();
			Block b = (Block)ht.get(key);
			
			s += key +"["+b.total+"("+(100*b.total/total)+"%)]\n";
		}
		
		return s;
	}
	
	class Block {
		long st, et, total;
	}
}
