package m65eII.engine;

public interface VertexList {
	V addVector(V v); 
	V setVector(int id, V v);
	void getVector(int i, V v);
	
	V setNormal(int id, V v);

	P setTexture(int id, P p);
	void getTexture(int i, P p);
	
	int size();
	
	void copyTo(VertexList vl);
	void reset();
}
