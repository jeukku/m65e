package m65eII.engine;

import java.awt.Image;

public interface TexturePool {
	void addTexture(Image img, String name);
}