/*
 * Created on 18.5.2005
 */
package m65eII.engine;

public interface Transformation {
    public void transform(V v);
    public void transform(V[] v);    
}
