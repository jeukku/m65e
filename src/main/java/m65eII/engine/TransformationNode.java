package m65eII.engine;

public interface TransformationNode {
	void setTransformation(Matrix m);
	
	Object3D addObject3D();
	Light addLight();

	Object3D findObject(String name);
	
	TransformationNode addNode();
	TransformationNode getParent();
	
	void copyTo(TransformationNode node);
}