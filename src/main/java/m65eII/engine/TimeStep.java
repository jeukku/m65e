/*
 * Created on 21.6.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package m65eII.engine;

/**
 * @author juuso
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TimeStep {
	private float sec, dsec;
	
	public TimeStep(float sec, float dsec) {
		this.sec = sec;
		this.dsec = dsec;
	}
	
	public float getSec() {
		return sec;
	}
	
	public float getDSec() {
		return dsec;
	}
}
