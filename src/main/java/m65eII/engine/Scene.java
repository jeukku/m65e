package m65eII.engine;

import m65eII.engine.bsp.BSP;

public interface Scene {
	Camera addCamera(String id);
	Camera selectCamera(String id);
	
	TransformationNode getBaseNode();
	
	void setRenderTarget(RenderTarget rt);
	void clear();
	BSP addBSP();	
}