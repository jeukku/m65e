package m65eII.engine;


public interface RenderTarget {
	int getWidth();
	int getHeight();
}