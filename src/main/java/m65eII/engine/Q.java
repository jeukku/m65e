package m65eII.engine;


public class Q implements java.io.Serializable {
	public float s,i,j,k;
		
	public String toString() {
		return "s:"+s+"{"+i+","+j+","+k+"}["+magnitude()+"]";
	}
	
	public Q(float s, V v) {
		this.s = s;
		this.i = v.x;
		this.j = v.y;
		this.k = v.z;
	}
	
	public Q(float ns, float ni, float nj, float nk) {
		this.s = ns;
		this.i = ni;
		this.j = nj;
		this.k = nk;
	}
	
	public Q(Q nq) {
        this.s = nq.s;
        this.i = nq.i;
        this.j = nq.j;
        this.k = nq.k;
    }

    public Q() {
        // TODO Auto-generated constructor stub
    }

    public void copy(Q q) {
		s = q.s;
		i = q.i;
		j = q.j;
		k = q.k;
	}
	
	public void add(Q q) {
		s+=q.s;
		i+=q.i;
		j+=q.j;
		k+=q.k;
	}
	
	public void rotation(float ang, float ni, float nj, float nk) {
		s = (float) Math.cos(ang / 2);
		float sin = (float) Math.sin(ang / 2);
		
		i = sin * ni;
		j = sin * nj;
		k = sin * nk;
	}
	
	static public Q getRotation(float ang, float vx, float vy, float vz) {
		Q q = new Q(0, 0, 0, 0);
		
		q.rotation(ang, vx, vy, vz);
		return q;
	}
	
	public void toMatrix(Matrix m) {
	    float array[] = m.getArray();
		array[0 ] = 1 - 2*j*j - 2*k*k;
		array[1 ] =     2*i*j - 2*s*k;
		array[2 ] =     2*i*k + 2*s*j;
		array[3 ] = 0;
	
		array[4 ] =     2*i*j + 2*s*k;
		array[5 ] = 1 - 2*i*i - 2*k*k;
		array[6 ] =     2*j*k - 2*s*i;
		array[7 ] = 0;
	
		array[8 ] =     2*i*k - 2*s*j;
		array[9 ] =     2*j*k + 2*s*i;
		array[10] = 1 - 2*i*i - 2*j*j;
		array[15] = 1;
	}
	/*
	*/

	/*
	public void toMatrix(M m) {
		float sqw = s*s;
		float sqx = i*i;
		float sqy = j*j;
		float sqz = k*k;
		array[0 ] =  sqx - sqy - sqz + sqw;
		array[5 ] = -sqx + sqy - sqz + sqw;
		array[10] = -sqx - sqy + sqz + sqw;
		
		float tmp1 = i*j;
		float tmp2 = k*s;
		array[1 ] = 2.0f * (tmp1 + tmp2);
		array[4 ] = 2.0f * (tmp1 - tmp2);
		
		tmp1 = i*k;
		tmp2 = j*s;
		array[2 ] = 2.0f * (tmp1 - tmp2);
		array[8 ] = 2.0f * (tmp1 + tmp2);
		tmp1 = j*k;
		tmp2 = i*s;
		array[7 ] = 2.0f * (tmp1 + tmp2);
		array[11] = 2.0f * (tmp1 - tmp2);      
	}
	*/
		
	public Matrix toMatrix() {
		Matrix m = new Matrix();
		
		toMatrix(m);
		return m;
	}
	
	public float magnitude() {
		return (float) Math.sqrt(s * s + i*i + j*j + k*k);
	}

	public void transpose() {
		i*=-1;
		j*=-1;
		k*=-1;
	}

	public void normalize() {
		final float m = magnitude();
		s/=m;
		i/=m;
		j/=m;
		k/=m;
	}
	
	public Q getMulted(Q q) {
		Q nq = new Q(s, i,j,k);
		nq.mult(q);
		return nq;
	}
	
	public void mult(float f) {
		s*=f;
		i*=f;
		j*=f;
		k*=f;
	}
	
	public void mult(Q q) {
		float ns = s*q.s - i*q.i - j*q.j - k*q.k;
		float ni = s*q.i + i*q.s + j*q.k - k*q.j;
		float nj = s*q.j - i*q.k + j*q.s + k*q.i;
		float nk = s*q.k + i*q.j - j*q.i + k*q.s;
		
		s = ns;
		i = ni;
		j = nj;
		k = nk;
	}

    public void set(Q nq) {
        this.s = nq.s;
        this.i = nq.i;
        this.j = nq.j;
        this.k = nq.k;
    }
}



