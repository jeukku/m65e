package m65eII.engine;

import java.util.Iterator;


public interface Object3D {
	TriangleList addTriangleList();
	
	void copyTo(Object3D o);
	
	void setName(String name);
	String getName();
    VectorList getVectorList();
    VectorList getNormalList();

    PointList getTexturePointList();

	Iterator triangleLists();

	ColorList getColorList();
}