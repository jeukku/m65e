/*
 * Created on 18.5.2005
 */
package m65eII.engine;

public class MatrixTransformation implements Transformation {
    private Matrix m;
    
    public MatrixTransformation(Matrix m) {
        this.m = m;
    }

    public void transform(V v) {
        m.mult(v);
    }

    public void transform(V[] v) {
        for(int i=0; i<v.length; i++) {
            m.mult(v[i]);
        }
    }

}
