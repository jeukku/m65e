package m65eII.engine;

public interface Material {
	void setTexture(String id);
	String getTexture();
	void copyTo(Material mat);
	boolean isTriangleColorsEnabled();
}