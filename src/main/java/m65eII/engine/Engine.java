package m65eII.engine;

public interface Engine {
	Scene addScene(String id);

	Scene selectScene(String id);

	void setImagesResources(Resources3D res);

	View getView();

	void frame();

	void addUpdater(TimeStepper s);

	void start();

	void stop();

	void update(TimeStep step);
}