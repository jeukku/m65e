package m65eII.engine;

import java.util.ArrayList;
import java.util.List;

public class Updaters {
	private List updaters = new ArrayList();
	public void update(TimeStep step) {
		for (int i = 0; i < updaters.size(); i++) {
			TimeStepper s = (TimeStepper) updaters.get(i);
			s.step(step);
		}
	}

	public void addUpdater(TimeStepper s) {
		updaters.add(s);
	}
}
