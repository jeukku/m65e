package m65eII.engine.bsp;

import m65eII.engine.Object3D;

public interface BSP {
	void add(Object3D hm_o);

	BspNode getBaseNode();
}
