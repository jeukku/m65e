package m65eII.engine;



public interface TriangleList {
	int addTriangle(int a, int b, int c);
    void setTextures(int index, int a, int b, int c);
    void setNormals(int index, int a, int b, int c);    
    void setColors(int index, int a, int b, int c);
	Material getMaterial();
	void createNormals(VectorList vl);
	void copyTo(TriangleList tl);
	int size();
}