/*
 * Created on Jun 23, 2005
 */
package m65eII.engine;


public interface PointList {

    void set(int a, P tmp);
    void get(int a, P tmp);
    void add(P p);
}
