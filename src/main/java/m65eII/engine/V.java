package m65eII.engine;

final public class V {
    public float x, y, z, w;

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public void setW(float w) {
        this.w = w;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public float getW() {
        return w;
    }

    public void set(float x, float y, float z, float w) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public V() {
        y = 0;
        x = 0;
        z = 0;
        w = 1;
    }

    public V(V nv) {
        copy(nv);
    }

    public V(float nx, float ny, float nz, float nw) {
        x = nx;
        y = ny;
        z = nz;
        w = nw;
    }

    /**
     * put your documentation comment here
     * 
     * @param float
     *            nx
     * @param float
     *            ny
     * @param float
     *            nz
     */
    public V(float nx, float ny, float nz) {
        x = nx;
        y = ny;
        z = nz;
        w = 1;
    }

    public void copy(V v) {
        x = v.x;
        y = v.y;
        z = v.z;
        w = v.w;
    }

    /**
     * 
     * @param a
     * @param b
     * @return Ei varmaan toimi
     */
    public boolean between(V a, V b) {
        /*
         * V c = a.added(b).multed(0.5f);
         * 
         * float accos = a.dot(c)/a.length()/c.length(); float bccos =
         * b.dot(c)/b.length()/c.length(); float tccos =
         * dot(c)/length()/c.length();
         * 
         * com.jv.GOutput.println("between: ac:" + accos+ " bc:"+bccos+" tc:" +
         * tccos); if(tccos>accos && tccos>bccos) return true;
         */
        float abcos = a.dot(b) / a.length() / b.length();
        float tacos = dot(a) / a.length() / length();
        float tbcos = dot(b) / b.length() / length();
        if (tacos < abcos) return false;
        if (tbcos < abcos) return false;
        return true;
    }

    public void zero() {
        x = 0;
        y = 0;
        z = 0;
        w = 1;
    }

    public void subtract(V v) {
        x -= v.x;
        y -= v.y;
        z -= v.z;
    }

    public void add(V v) {
        x += v.x;
        y += v.y;
        z += v.z;
    }

    public float length() {
        float p = length2pow();
        if (p != 1.0) p = (float) Math.sqrt(p);
        return p;
    }

    public float length2pow() {
        return x * x + y * y + z * z;
    }

    public V normalize() {
        this.mult(1.0f / length());
        return this;
    }

    final public float dot(V b) {
        return x * b.x + y * b.y + z * b.z;
    }

    final public float vectProj(V a) {
        return this.dot(a) / this.dot(this);
    }

    // <A,B,C> X <D,E,F> = <B*F - C*E, C*D - A*F, A*E - B*D>
    // V( y*v.z - v.y*z, z*v.x - v.z*x, x*v.y - v.x*y); };
    public V cross(V b) {
        V r = new V(y * b.z - z * b.y, z * b.x - x * b.z, x * b.y - y * b.x, 1);
        return r;
    }

    /**
     * put your documentation comment here
     * 
     * @return
     */
    public V normal2D() {
        return new V(y, -x, z, 1);
    }

    /**
     * 
     * put your documentation comment here
     * 
     * @param v
     * 
     */
    public void mult(V v) {
        x *= v.x;
        y *= v.y;
        z *= v.z;
    }

    /**
     * 
     * put your documentation comment here
     * 
     * @param d
     * 
     */
    public void mult(float d) {
        x *= d;
        y *= d;
        z *= d;
    }

    /**
     * 
     * put your documentation comment here
     * 
     * @param dx
     * 
     * @param dy
     * 
     * @param dz
     * 
     */
    public void mult(float dx, float dy, float dz) {
        x *= dx;
        y *= dy;
        z *= dz;
    }

    public void homogenize() {
        x /= w;
        y /= w;
        z /= w;
        w = 1.0f / w;
    }

    public void morph(V a, V b, float suhde) {
        float dx = b.x - a.x;
        float dy = b.y - a.y;
        float dz = b.z - a.z;
        dx *= suhde;
        dy *= suhde;
        dz *= suhde;
        x = a.x + dx;
        y = a.y + dy;
        z = a.z + dz;
    }

    float round(float d, int i) {
        return ((int) (d * Math.pow(10, i))) / (float) Math.pow(10, i);
    }

    public String toString() {
        String s = "v{" + round(x, 4) + "\t, " + round(y, 4) + "\t, "
                + round(z, 4) + "\t, " + round(w, 4) + '\t' + "}["
                + round(length(), 3) + "]";
        return s;
    }

    public String toStringMore() {
        String s = new String();
        if (Float.isNaN(x) || Float.isNaN(y) || Float.isNaN(z)) {
            s += "{" + x + y + z;
        } else {
            s += "v{" + round(x, 4) + '\t' + '\t' + ", " + round(y, 4) + '\t'
                    + '\t' + "," + round(z, 4) + '\t' + '\t' + ", "
                    + round(w, 4) + '\t' + '\t' + "}[" + round(length(), 3)
                    + "]";
        }
        return s;
    }

    public String asString(int dec) {
        String s = new String();
        if (Float.isNaN(x) || Float.isNaN(y) || Float.isNaN(z)) {
            s += "{" + x + y + z;
        } else {
            s += "{" + round(x, dec) + "\t\t," + round(y, dec) + "\t\t,"
                    + round(z, dec) + "\t\t," + round(w, dec) + "}\t\t["
                    + round(length(), dec) + "]";
        }
        return s;
    }

    public void set(V v) {
        set(v.x, v.y, v.z, v.w);
    }
}
