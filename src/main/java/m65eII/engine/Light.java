package m65eII.engine;

public interface Light {
    static final int TYPE_DIRECTIONAL = 1;
    
    void setType(int type);
}