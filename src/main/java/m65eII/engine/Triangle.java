package m65eII.engine;

public class Triangle {
	private int a, b, c;

	public Triangle() {
		a = 0;
		b = 0;
		c = 0;
	}

	public Triangle(int a, int b, int c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Triangle#getA()
	 */
	public int getA() {
		return a;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Triangle#getB()
	 */
	public int getB() {
		return b;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Triangle#getC()
	 */
	public int getC() {
		return c;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Triangle#set(int, int, int)
	 */
	public void set(int a, int b, int c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}
}