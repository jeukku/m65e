/*
 * Created on Jun 23, 2005
 */
package m65eII.engine;


public interface VectorList {

    V addVector(V v);

    int size();

    void get(int iv, V v);

    V set(int i, V v);

    void reset();
}
