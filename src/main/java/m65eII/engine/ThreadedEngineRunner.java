package m65eII.engine;

import m65eII.awt.EngineRunner;

public class ThreadedEngineRunner implements EngineRunner {
	private Thread t;
	private boolean stopped;
	private Engine e;
	private long st, lt;
	public void start() {
		t = new Thread(new Runnable() {
			public void run() {
				this_run();
			}
		});
		t.start();
		
	}
	
	public void stop() {
		stopped = true;
	}

	private void this_run() {
		while(!stopped) {
			long ct = System.currentTimeMillis();
			if(st==0) {
					st = ct;
					lt = ct;
			}
			float fdt = (ct-lt)/1000.0f;
			float ft = (ct-st)/1000.0f;
			TimeStep step = new TimeStep(ft,fdt);
			e.update(step);
			e.frame();
		}
	}

	public ThreadedEngineRunner(Engine e, View view2) {
		this.e  = e;
	}
}
