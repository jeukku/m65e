package m65eII.engine;

public interface Camera {
	void setLocation(V v);
	void setTo(V v);
	void setForward(V v);
	void setUp(V v);
	void setFOV(float fov);
	void setNear(float near);
	void setFar(float far);
}