/*
 * Created on 6.9.2004
 *
 */
package m65eII.objects;

import m65eII.engine.ColorList;
import m65eII.engine.Object3D;
import m65eII.engine.TriangleList;
import m65eII.engine.V;
import m65eII.engine.VectorList;
import m65eII.tr.C;

/**
 * @author juuso
 *
 */
public class ObjectHeightmap {
	private float xsize = 1, zsize = 1;
	private int xres = 20, zres = 20;
	private float maxh = 1;
	private Object3D obj;
	private HeightProvider provider;

	private TriangleList tl;
	private VectorList vl;
	private ColorProvider cprovider;
	
	public ObjectHeightmap(Object3D obj, HeightProvider provider, ColorProvider cp) {
		this.obj = obj;
		this.provider = provider;
		this.cprovider = cp;
	}

	public void setSize(int xsize, int zsize) {
		this.xsize = xsize;
		this.zsize = zsize;
	}

	public void setResolution(int xres, int zres) {
		this.xres = xres;
		this.zres = zres;
	}
	
	public void setMaxHeight(float maxh) {
		this.maxh = maxh;
	}

	public void create() {	
		vl = obj.getVectorList();
		tl = obj.addTriangleList();
		ColorList cl = obj.getColorList();
		
		C c = new C();
		//
		for (int ix = 0; ix < xres; ix++) {
			float x = (float) ix / xres * xsize - xsize / 2;
			for (int iz = 0; iz < zres; iz++) {
				float z = (float) iz / zres * zsize - zsize / 2;
				V v = vl.addVector(new V(x,0,z));
				cprovider.getColor(v.x/(xsize/2), v.z/(zsize/2),c);
				cl.addColor(c.r, c.g, c.b);
				//
				if (ix + 1 < xres && iz + 1 < zres) {
					int ibase = ix * zres + iz;
					int ia = tl.addTriangle(
						ibase, ibase + 1, ibase + zres);
					tl.setColors(ia, ibase, ibase+1, ibase+zres);
					int ib = tl.addTriangle(
						ibase + 1, ibase + zres +1, ibase + zres);
					tl.setColors(ib, ibase+1, ibase+1+zres, ibase+zres);
					
				}
			}
		}

		update();
		tl.createNormals(vl);	
	}
	
	public void update() {
		int iv = 0;
		V v = new V();
		
		for (int ix = 0; ix < xres; ix++) {
			for (int iz = 0; iz < zres; iz++) {
				vl.get(iv, v);
				v.y = provider.getHeight(v.x/(xsize/2), v.z/(zsize/2));
				v.y*=maxh;
				vl.set(iv++, v);
			}
		}

		tl.createNormals(vl);
	}

	public interface HeightProvider {
		float getHeight(float x, float y);
	}
}
