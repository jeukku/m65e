package m65eII.objects;

import m65eII.tr.C;

public interface ColorProvider {
	void getColor(float f, float g, C c);
}
