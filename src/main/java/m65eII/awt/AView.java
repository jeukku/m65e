package m65eII.awt;

import m65eII.engine.RenderTarget;
import m65eII.engine.View;

public interface AView extends View {

	RenderTarget getRenderTarget();

	void setUpdater(Runnable runnable);

	void repaint();
}
