/*
 * Created on 20.6.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package m65eII.awt;

import java.util.Hashtable;

import m65eII.engine.Engine;
import m65eII.engine.Resources3D;
import m65eII.engine.Scene;
import m65eII.engine.ThreadedEngineRunner;
import m65eII.engine.TimeMeasure;
import m65eII.engine.TimeStep;
import m65eII.engine.TimeStepper;
import m65eII.engine.Updaters;
import m65eII.engine.View;
import m65eII.tr.TScene;

/**
 * @author juuso
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AEngine implements Engine {
	private Hashtable scenes = new Hashtable();
	private TScene current_scene;
	private AView view;
	private TimeMeasure tm = new TimeMeasure(getClass().getName());
	private boolean enabled = true;
	private EngineRunner erunner;
	private Updaters upd = new Updaters();
	
	public void addUpdater(TimeStepper s) {
		upd.addUpdater(s);	
	}
	
	public void update(TimeStep step) {
		upd.update(step);
	}
	
	public void start() {
		erunner.start();
	}

	public AEngine(boolean onethreaded) {
		if (onethreaded) {
			view = new AAwtThreadView();
			erunner = new PaintThreadEngineRunner(this, view);
		} else {
			view = new AThreadView();
			erunner = new ThreadedEngineRunner(this, view);
		}
	}

	public AEngine() {
		this(false);
	}

	public void setImagesResources(Resources3D res) {
		// TODO
	}

	public void stop() {
		enabled = false;
		erunner.stop();
		view.stop();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.Engine#frame()
	 */
	public void frame() {
		tm.setTotalStartTime();
		if (view.isVisible() && enabled) {
			try {
				tm.setStartTime("startrender");
				getView().startRender();
				tm.setEndTime("startrender");
				tm.setStartTime("draw");
				draw(current_scene);
				tm.setEndTime("draw");
				tm.setStartTime("endrender");
				getView().endRender();
				tm.setEndTime("endrender");
			} catch (Exception e) {
				System.out.println("Pausing for a while because of " + this);
				e.printStackTrace();
				synchronized (view) {
					try {
						view.wait(1000);
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		}
		tm.setTotalEndTime();
	}

	/**
	 * Luo framedrawring, tekee osansa sen asetuksien asettamiseen ja antaa
	 * scene:n jatkaa.
	 * 
	 * @param scene
	 */
	private void draw(TScene scene) {
		AFrameDrawer fd = new AFrameDrawer();
		fd.setRenderTarget(view.getRenderTarget());
		scene.draw(fd);
		fd.finish();
	}

	/**
	 * Mit's luulisit
	 */
	public Scene addScene(String id) {
		System.out.println("addScene " + id);
		TScene scene = new TScene(id);
		scenes.put(id, scene);
		if (current_scene == null) {
			selectScene(id);
		}
		return scene;
	}

	/**
	 * Valitsee scenen, jota ruudulle piirret''n
	 */
	public Scene selectScene(String id) {
		TScene ncurrent_scene = (TScene) scenes.get(id);
		if (ncurrent_scene == null) {
			return null;
		}
		current_scene = ncurrent_scene;
		System.out.println("" + this);
		System.out.println("selectScene " + id + " cs:" + current_scene);
		return current_scene;
	}

	public View getView() {
		return view;
	}
}