package m65eII.awt;

import java.awt.Canvas;

public class BaseView {
	private Canvas canvas;
	private ARenderTarget rt;
	private long st = System.currentTimeMillis(), framecount;

	void updateFrameCount() {
		framecount++;
	}

	BaseView(Canvas c) {
		this.canvas = c;
		rt = new ARenderTarget(canvas);
	}

	public void startRender() {
		framecount++;
		rt.setSize(canvas.getSize().width, canvas.getSize().height);
		rt.clearBufferImage();
	}

	public ARenderTarget getRenderTarger() {
		return rt;
	}
	
	String getFrameString() {
		long dt = System.currentTimeMillis() - st;
		String s;
		if (dt > 5000) {
			framecount = 0;
			st = System.currentTimeMillis();
			dt = 0;
		}
		if (dt > 0) {
			s = "f:" + framecount + " hz:" + (float) framecount * 1000.0f
					/ (dt);
		} else {
			s = "dt:0";
		}
		return s;
	}

}
