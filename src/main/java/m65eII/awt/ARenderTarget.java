/*
 * Created on 24.6.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package m65eII.awt;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Polygon;

import m65eII.engine.RenderTarget;
import m65eII.engine.V;

/**
 * @author juuso
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class ARenderTarget implements RenderTarget {
	private Graphics g;
	private Image img;
	private Component comp;
	private Polygon poly = new Polygon(new int[3], new int[3], 3);
	static private final int reds = 32, greens = 32, blues = 32;
	static private Color cs[] = new Color[reds * greens * blues];

	static int getColorIndex(int r, int g, int b) {
		if (r >= reds)
			r = reds - 1;
		if (g >= greens)
			g = greens - 1;
		if (b >= blues)
			b = blues - 1;
		int i = r * greens * blues + g * blues + b;
		return i;
	}

	static {
		for (int r = 0; r < reds; r++) {
			for (int g = 0; g < greens; g++) {
				for (int b = 0; b < blues; b++) {
					int i = getColorIndex(r, g, b);
					cs[i] = new Color(255 * r / reds, 255 * g / greens, 255 * b / blues);
				}
			}
		}
	}

	void setColor(float fr, float fg, float fb) {
		int ir = (int) (fr * reds);
		int ig = (int) (fg * greens);
		int ib = (int) (fb * blues);
		int i = getColorIndex(ir, ig, ib);
		g.setColor(cs[i]);
	}

	void drawTriangle(V a, V b, V c) {
		drawTriangle(a.x, a.y, b.x, b.y, c.x, c.y);
	}

	void drawTriangle(float ax, float ay, float bx, float by, float cx, float cy) {
		poly.xpoints[0] = (int) ax;
		poly.ypoints[0] = (int) ay;
		poly.xpoints[1] = (int) bx;
		poly.ypoints[1] = (int) by;
		poly.xpoints[2] = (int) cx;
		poly.ypoints[2] = (int) cy;
		g.fillPolygon(poly);
		// g.setColor(Color.black);
		// g.drawPolygon(poly);
	}

	public int getHeight() {
		if (img != null) {
			return img.getHeight(null);
		}
		return 0;
	}

	public int getWidth() {
		if (img != null) {
			return img.getWidth(null);
		}
		return 0;
	}

	ARenderTarget(Component comp) {
		this.comp = comp;
	}

	Image getImage() {
		return img;
	}

	void setSize(int w, int h) {
		if (w <= 0 || h <= 0) {
			throw new IllegalArgumentException(getClass().getName() + " setSize w:" + w + " h:" + h);
		}
		if (img == null || w != img.getWidth(null) || h != img.getHeight(null)) {
			img = comp.createImage(w, h);
			if (img == null)
				throw new RuntimeException(getClass().getName() + " failed to create image");
			g = img.getGraphics();
		}
	}

	Graphics getGraphics() {
		return g;
	}

	void clearBufferImage() {
		if (g != null && img != null) {
			g.clearRect(0, 0, img.getWidth(null), img.getHeight(null));
		}
	}
}
