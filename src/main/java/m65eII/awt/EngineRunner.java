package m65eII.awt;

public interface EngineRunner {

	void start();

	void stop();
}
