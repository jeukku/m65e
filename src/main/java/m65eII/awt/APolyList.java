/**
 * 
 */
package m65eII.awt;

import m65eII.Logger;
import m65eII.LoggerFactory;


class APolyList {
	ATriangle tris[] = new ATriangle[0];

	int poly_index;

	final static int A = 0;

	final static int B = 2;

	final static int C = 4;

	final static int COLOR = 6;

	/**
	 * 
	 */
	public APolyList() {
		grow(1000);
	}

	void qsort(int low, int high) {
		int pivot = (high - low);
		pivot = (int) (Math.random() * pivot);
		pivot += low;
		final float pivot_z = tris[pivot].z;
		// com.jv.GOutput.println("pivot z " + pivot_z);
		int left = low;
		int right = high;
		while (left < right) {
			while (tris[left].z <= pivot_z && left < right)
				left++;
			while (tris[right].z > pivot_z && left < right)
				right--;
			if (left < right) { // swap
				ATriangle tmp_tri = tris[left + 0];
				tris[left + 0] = tris[right + 0];
				tris[right + 0] = tmp_tri;
			}
		}
		// order[right + 0] = pivot_index;
		// order[right + 1] = pivot_z;
		// com.jv.GOutput.println("right " + right);
		// print(low, high);
		if (low < right - 1)
			qsort(low, right - 1);
		if (high > right + 1)
			qsort(right + 1, high);
	}

	void sort() {
		// print(0, order_index);
		qsort(0, poly_index);
		// print(0, order_index);
	}

	void print(Logger log, int low, int high) {
		log.debugln("low:" + low + " high:" + high + ":");
		for (int i = low; i < high; i++) {
			log.debugln(" " + tris[i]);
		}
		log.debugln("");
	}

	ATriangle getNext() {
		if (poly_index + 1 >= tris.length)
			grow();
		return tris[poly_index++];
	}

	// void add(float ax, float ay, float bx, float by, float cx, float cy,
	// float cr, float cg, float cb, float z) {
	//
	// if (poly_index ++ >= tris.length)
	// grow();
	//
	// fs[poly_index + 0] = ax;
	// fs[poly_index + 1] = ay;
	// fs[poly_index + 2] = bx;
	// fs[poly_index + 3] = by;
	// fs[poly_index + 4] = cx;
	// fs[poly_index + 5] = cy;
	// fs[poly_index + 6] = cr;
	// fs[poly_index + 7] = cg;
	// fs[poly_index + 8] = cb;
	//
	// order[order_index + 0] = poly_index;
	// order[order_index + 1] = (int) (z * 0xffffff);
	//
	// poly_index += polysize;
	// order_index += ordersize;
	// }
	private void grow() {
		int nsize = (int) (tris.length * 1.1f);
		grow(nsize);
	}

	private void grow(int nsize) {
		LoggerFactory.createLogger(this).debugln("APolyList grow " + tris.length + " to " + nsize);
		ATriangle ntris[] = new ATriangle[nsize];
		System.arraycopy(tris, 0, ntris, 0, tris.length);
		for (int i = tris.length; i < ntris.length; i++) {
			ntris[i] = new ATriangle();
		}
		tris = ntris;
	}

	void reset() {
		poly_index = 0;
	}
}