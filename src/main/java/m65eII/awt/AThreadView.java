package m65eII.awt;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;

import m65eII.LoggerFactory;
import m65eII.engine.RenderTarget;

import com.jv.pixelgr.MouseEventAdapter;
import com.jv.pixelgr.SCanvasMouseListener;

public class AThreadView implements AView {
	private Canvas canvas = new ACanvas();
	private MouseEventAdapter cmh = new MouseEventAdapter(canvas);
	private boolean repaint_lock = false;
	private Object draw_lock = new Object();
	private boolean stopped;
	private int count_endrender, count_update;
	private BaseView bview = new BaseView(canvas);

	public void repaint() {
		canvas.repaint();
	}

	public int getHeight() {
		return canvas.getHeight();
	}

	public int getWidth() {
		return canvas.getWidth();
	}

	public void setUpdater(Runnable runnable) {
		// TODO Auto-generated method stub
	}

	public void setClear(boolean b) {
		// TODO Auto-generated method stub
	}

	public void addMouseListener(SCanvasMouseListener l) {
		cmh.addMouseListener(l);
	}

	public boolean isVisible() {
		return canvas.isVisible();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.View#startRender()
	 */
	public void startRender() {
		bview.startRender();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.View#stopRender()
	 */
	public void endRender() {
		count_endrender++;
		//log.println("EndRender " + canvas);
		if (!canvas.isVisible())
			throw new RuntimeException("Canvas not visible " + canvas);
		//
		if (stopped)
			return;
		//
		if (!repaint_lock && !stopped) {
			synchronized (draw_lock) {
				repaint_lock = true;
				canvas.repaint();
				try {
					// log.println("EndRender waiting ");
					draw_lock.wait(30000);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			LoggerFactory.createLogger(this).debugln(
					"AView.endRender ERNO!!! repaint_lock true!!!");
		}
	}

	public void attachTo(Container cont) {
		cont.add(canvas);
		canvas.validate();
		canvas.invalidate();
	}

	public RenderTarget getRenderTarget() {
		return bview.getRenderTarger();
	}

	AThreadView() {
		canvas.setBackground(Color.white);
		canvas.addMouseListener(cmh.getMouseListener());
		canvas.addMouseMotionListener(cmh.getMouseMotionListener());
	}

	private class ACanvas extends Canvas {
		private static final long serialVersionUID = 1L;

		public void update(Graphics g) {
			count_update++;
			// log.println("ACanvas update " + count_update);
			synchronized (draw_lock) {
				Graphics bg = bview.getRenderTarger().getGraphics();
				if (bg != null) {
					bview.updateFrameCount();
					bg.drawString(bview.getFrameString(), 20, 30);
					g.drawImage(bview.getRenderTarger().getImage(), 0, 0, null);
					Toolkit.getDefaultToolkit().sync();
				}
				draw_lock.notifyAll();
				repaint_lock = false;
			}
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.awt.Component#minimumSize()
		 */
		public Dimension minimumSize() {
			return new Dimension(20, 20);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.awt.Component#getMinimumSize()
		 */
		public Dimension getMinimumSize() {
			return minimumSize();
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.awt.Component#preferredSize()
		 */
		public Dimension preferredSize() {
			return new Dimension(320, 200);
		}

		public Dimension getPreferredSize() {
			return preferredSize();
		}
	}

	public void stop() {
		synchronized (draw_lock) {
			stopped = true;
			draw_lock.notifyAll();
		}
	}

	public void start() {
		// TODO
	}
}