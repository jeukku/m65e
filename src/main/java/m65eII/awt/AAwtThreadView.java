package m65eII.awt;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;

import m65eII.Logger;
import m65eII.LoggerFactory;
import m65eII.engine.RenderTarget;

import com.jv.pixelgr.SCanvasMouseListener;

public class AAwtThreadView implements AView {
	private ACanvas canvas;
	private Runnable updater;
	private BaseView bview;
	private Logger log = LoggerFactory.createLogger(getClass().getName());
	private boolean stopped;

	AAwtThreadView() {
		canvas = new ACanvas();
		canvas.setBackground(Color.GRAY);
		bview = new BaseView(canvas);
	}

	public int getHeight() {
		return canvas.getHeight();
	}
	public int getWidth() {
		return canvas.getWidth();
	}
	
	public void repaint() {
		canvas.repaint();
	}

	public void setUpdater(Runnable runnable) {
		this.updater = runnable;
	}

	public void addMouseListener(SCanvasMouseListener l) {
		// TODO Auto-generated method stub
	}

	public void attachTo(Container cont) {
		cont.add(canvas);
		canvas.validate();
		canvas.invalidate();
	}

	public void endRender() {
		// not needed
	}

	public RenderTarget getRenderTarget() {
		return bview.getRenderTarger();
	}

	public boolean isVisible() {
		return canvas.isVisible();
	}

	public void setClear(boolean b) {
		// TODO Auto-generated method stub
	}

	public void startRender() {
		bview.startRender();
	}

	public void stop() {
		stopped = true;
	}

	private class ACanvas extends Canvas {
		public void update(Graphics g) {
			updater.run();
			Image img = bview.getRenderTarger().getImage();
			if (img != null) { 
				g.drawImage(img, 0, 0, null);
				g.drawString(bview.getFrameString(), 0, 20);
			} else {
				log.debugln("Rendertarget img null");
				synchronized (bview) {
					try {
						bview.wait(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			if (!stopped)
				repaint();
		}
	}
}
