/*
 * Created on 21.6.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package m65eII.awt;

import m65eII.engine.ColorList;
import m65eII.engine.Light;
import m65eII.engine.Material;
import m65eII.engine.Matrix;
import m65eII.engine.PointList;
import m65eII.engine.RenderTarget;
import m65eII.engine.TimeMeasure;
import m65eII.engine.V;
import m65eII.engine.VectorList;
import m65eII.tr.C;
import m65eII.tr.TFrameDrawer;
import m65eII.tr.TTransformation;
import m65eII.tr.TTriangleList;
import m65eII.tr.TViewPort;

/**
 * @author juuso
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class AFrameDrawer implements TFrameDrawer {
	private ARenderTarget rt;
	private TViewPort viewport = new TViewPort();
	private VectorList vl;
	static final private APolyList pl = new APolyList();
	private TTransformation transformation;
	private static TimeMeasure tm = new TimeMeasure("AFrameDrawer");
	private int ilight = 0;
	private ALight lights[] = new ALight[30];
	private ColorList cl;

	// private VectorList nl;
	// private PointList txl;
	AFrameDrawer() {
		tm.setTotalStartTime();
		pl.reset();
	}

	public void addLight(Light light) {
		lights[ilight++] = (ALight) light;
	}

	/**
	 * TODO miss??h??n vaiheessa valaistukset ja muut tehd????n.
	 */
	public void drawTriangleList(TTriangleList tl) {
		tm.setStartTime("drawTriangleList");
		final V va = new V();
		final V vb = new V();
		final V vc = new V();
		final V trin = new V();
		final C ca = new C();
		final C cb = new C();
		final C cc = new C();
		int size = tl.size();
		int tris[] = tl.getTris();
		V ns[] = tl.getNs();
		int[] cs = tl.getCs();

		final Matrix m_view = transformation.getViewTransformation();
		//
		boolean colors = tl.getMaterial().isTriangleColorsEnabled();
		
		//
		for (int i = 0; i < size; i += 3) {
			trin.copy(ns[i / 3]);
			m_view.multw0(trin);
			if (trin.z < 0) {
				vl.get(tris[i + 0], va);
				vl.get(tris[i + 1], vb);
				vl.get(tris[i + 2], vc);
				//
				m_view.multw1(va);
				m_view.multw1(vb);
				m_view.multw1(vc);
				//
				if (viewport.check(va, vb, vc)!=0) {
					viewport.screenTransform(va);
					viewport.screenTransform(vb);
					viewport.screenTransform(vc);
					//
					ATriangle tri = pl.getNext();
					//
						int il = 0;
					ALight light;
					float ld = 0; // se toinen valaistus
					float ls = 0; // specular valaistus
					while ((light = lights[il++]) != null) {
						light.light(tri, trin, vb);
					}
					//
					// TODO clipping ja muut puuttuu kokonaan
					tri.ax = va.x;
					tri.ay = va.y;
					tri.bx = vb.x;
					tri.by = vb.y;
					tri.cx = vc.x;
					tri.cy = vc.y;
					tri.z = va.z + vb.z + vc.z;
					if (colors) {
						cl.get(tris[i + 0], ca);
						cl.get(tris[i + 1], cb);
						cl.get(tris[i + 2], cc);
						tri.cr = ca.r;
						tri.cg = ca.g;
						tri.cb = ca.b;
					} else {
						tri.cr = trin.z < 0 ? -trin.z : trin.z;
						tri.cg = trin.x < 0 ? -trin.x : trin.x;
						tri.cb = 0.5f;
						tri.cr /= 2;
						tri.cg /= 2;
					}
	
				}
			}
		}
		tm.setEndTime("drawTriangleList");
	}

	void finish() {
		// ATriangle tris[] = pl.tris;
		int size = pl.poly_index;
		tm.setStartTime("pl.sort");
		pl.sort();
		tm.setEndTime("pl.sort");
		tm.setStartTime("finish.loop");
		for (int i = 0; i < size; i++) {
			final ATriangle tri = pl.tris[i];
			rt.setColor(tri.cr, tri.cg, tri.cb);
			rt.drawTriangle(tri.ax, tri.ay, tri.bx, tri.by, tri.cx, tri.cy);
			// fs[pi + APolyList.A + 0], fs[pi + APolyList.A + 1],
			// fs[pi + APolyList.B + 0], fs[pi + APolyList.B + 1], fs[pi
			// + APolyList.C + 0], fs[pi + APolyList.C + 1]);
		}
		tm.setEndTime("finish.loop");
		tm.setTotalEndTime();
	}

	public void setMaterial(Material mat) {
		// TODO
	}

	public void setRenderTarget(RenderTarget rt) {
		this.rt = (ARenderTarget) rt;
		viewport.setSize(rt.getWidth(), rt.getHeight());
	}

	public void setColorList(ColorList cl) {
		this.cl = cl;
	}
	public void setVectorList(VectorList vl) {
		this.vl = vl;
	}

	public void setNormalList(VectorList nl) {
		// TODO this.nl = nl;
	}

	public void setTexturePointList(PointList txl) {
		// this.txl = txl;
	}

	public void setTransformation(TTransformation tr) {
		this.transformation = tr;
		tr.setupViewport(viewport);
	}
}