package m65eII.awt;

import m65eII.engine.Engine;
import m65eII.engine.View;

public class PaintThreadEngineRunner implements EngineRunner {
	private View view;
	private Engine e;
	private boolean stopped;
	
	public PaintThreadEngineRunner(Engine engine, View view) {
		this.view = view;
		this.e = engine;
	}

	private void this_update() {
		//log.println("update " + getClass().getName() );	
		if(!stopped) e.frame();
	}

	public void stop() {
		stopped = true;
		view.stop();
	}
	
	public void start() {
		stopped = false;
		view.setUpdater(new Runnable() {
			public void run() {
				this_update();
			}

		});
		view.repaint();
	}
}
