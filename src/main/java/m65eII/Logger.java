package m65eII;


public class Logger {
	private org.apache.log4j.Logger log;

	public Logger(Object o) {
		log = org.apache.log4j.Logger.getLogger(o.getClass());
	}

	public void debugln(String string) {
		log.debug(string);
	}

	public void print(Exception e) {
		log.info(e);		
	}

	public void debugpush(String string) {
		debugln(string);
	}

	public void pop() {
		// TODO Auto-generated method stub
		
	}

}
