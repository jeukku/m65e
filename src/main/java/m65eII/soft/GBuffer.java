/*
 * Created on 20.10.2004
 * by Juuso
 */
package m65eII.soft;

import m65eII.Logger;
import m65eII.LoggerFactory;

/**
 * @author juuso
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class GBuffer {
	final static SegmentProvider sp;
	Segment first;
	static boolean debug;
	static {
		sp = new SegmentProvider();
	}

	static class Segment {
		Segment next;
		int left, right;

		public String toString() {
			Segment tmp = this;
			String s = "";
			while (tmp != null) {
				s += " S:" + tmp.left + "-" + tmp.right;
				tmp = tmp.next;
			}
			return s;
		}
	}

	void release() {
		while (first != null) {
			Segment next = first.next;
			sp.release(first);
			first = next;
		}
	}

	static class SegmentProvider {
		Segment current = new Segment();
		Segment last = current;
		private Logger log = LoggerFactory.createLogger(this);

		Segment next() {
			Segment ret = current;
			if (current.next == null) {
				current.next = new Segment();
				last = current.next;
				log.debugln(getClass().getName()
						+ " created new segment as first");
			}
			current = current.next;
			ret.next = null;
			ret.left = 1000000;
			ret.right = -1000000;
			return ret;
		}

		void release(Segment s) {
			s.next = null;
			s.left = 1000000;
			s.right = -1000000;
			if (current == null) {
				current = s;
			} else {
				last.next = s;
				last = s;
				last.next = null;
			}
		}
	}
}
