/*
 * Created on 17.10.2004
 * by Juuso
 */
package m65eII.soft;

import java.awt.Image;
import java.util.Hashtable;

import m65eII.engine.Resources3D;
import m65eII.engine.TexturePool;

/**
 * @author juuso
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class STexturePool implements TexturePool {
	private Hashtable ts = new Hashtable();

	private Resources3D res;

	public STexturePool(Resources3D res2) {
		this.res = res2;
	}

	STexture getTexture(String name) {
		if (name == null) {
			// GOutput.println("WARNING name null... returning default
			// texture");
			name = "default.jpg";
		}

		STexture t = (STexture) ts.get(name);
		if (t == null) {
			t = loadTexture(name);
		}
		return t;
	}

	private STexture loadTexture(String name) {
		Image img = res.getImage(name);
		STexture t = new STexture(img);
		ts.put(name, t);
		return t;
	}

	public void addTexture(Image img, String name) {
		STexture t = new STexture(img);
		ts.put(name, t);
	}

}
