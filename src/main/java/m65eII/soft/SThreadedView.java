package m65eII.soft;

import java.awt.Color;
import java.awt.Container;

import com.jv.pixelgr.SCanvas;
import com.jv.pixelgr.SCanvasMouseListener;
import com.jv.pixelgr.SPixelImage;

public class SThreadedView implements SView {
	private SPixelImage pimg;
	private SCanvas canvas;
	private long st = System.currentTimeMillis(), framecount;
	private boolean doclear = true;
	boolean stopped;

	public void repaint() {
		// TODO Auto-generated method stub
	}

	public void setUpdater(Runnable runnable) {
		// TODO Auto-generated method stub
	}

	public void stop() {
		stopped = true;
		canvas.stop();
	}

	public void setClear(boolean b) {
		doclear = b;
	}

	public boolean isVisible() {
		return canvas.isVisible();
	}

	public void addMouseListener(SCanvasMouseListener l) {
		canvas.addMouseListener(l);
	}

	public void startRender() {
		pimg.setSize(canvas.getSize().width, canvas.getSize().height);
		if (doclear)
			pimg.clear();
	}

	public void endRender() {
		canvas.paint();
	}

	public void attachTo(Container cont) {
		canvas.addTo(cont);
	}

	public SThreadedView(SPixelImage npimg) {
		pimg = npimg;
		init();
	}

	public SThreadedView() {
		this(new SPixelImage());
	}

	private void init() {
		canvas = new SCanvas(pimg);
		canvas.setBackground(Color.gray);
	}

	void updateFrameCount() {
		framecount++;
	}

	String getFrameString() {
		long dt = System.currentTimeMillis() - st;
		String s;
		if (dt > 5000) {
			framecount = 0;
			st = System.currentTimeMillis();
			dt = 0;
		}
		if (dt > 0) {
			s = "f:" + framecount + " hz:" + (float) framecount * 1000.0f
					/ (dt);
		} else {
			s = "dt:0";
		}
		return s;
	}

	public int getWidth() {
		return pimg.getWidth();
	}

	public int getHeight() {
		return pimg.getHeight();
	}

	public void setup(Rasterizer ras) {
		ras.setup(pimg.getPixels(), getWidth(), getWidth());
	}
}