/*
 * Created on 14.3.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package m65eII.soft;

import m65eII.Logger;
import m65eII.LoggerFactory;

/**
 * @author juuso
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
final class RasterizerDefault implements Rasterizer {

	private int left_x[], right_x[];

	private int left_z[], right_z[];

	private int left_tu[], right_tu[];

	private int left_tv[], right_tv[];

	private int left_sp[];

	private int left_br[];
	
	private GBuffer glines[] = new GBuffer[0];

	// private int tua, tva;
	private int ibottom;

	// private final int array_line_step = 5;
	private int screen_width, screen_height;

	private final int X = 0;

	private final int Y = 1;

	private final int Z = 2;


	private int tua, tva;

	private int bra, spa;

	private int pixels[];

	private STexture t;

	final private boolean wraptexturecoordinates = false;

	private Logger log = LoggerFactory.createLogger(this);
	
	/*
	 * private final Side abside = new Side(); private final Side acside = new
	 * Side(); private final Side bcside = new Side(); private Side aleft = new
	 * Side(); private Side bleft = new Side(); private Side aright = new
	 * Side(); private Side bright = new Side();
	 */
	public void startFrame() {
		for (int i = 0; i < glines.length; i++) {
			GBuffer gb = glines[i];
			gb.release();
		}
	}

	public final static int asFixedPoint(float d) {
		return (int) (d * 0xffff);
	}

	public final static int toNotFP(int i) {
		return i / 0xffff;
	}

	final int intZMax(float z) {
		return asFixedPoint(z);
	}

	public void drawEdge(int offset, int vertex_size, float[] vx_a, float[] vx_b) {
		int ay = (int) vx_a[Y];
		int by = (int) vx_b[Y];
		int dy = by - ay;
		if (dy == 0)
			return;
		int x = asFixedPoint(vx_a[X]);
		int z = intZMax(vx_a[Z]);
		// int br = asFixedPoint(vx_a[BR]);
		// int sp = asFixedPoint(vx_a[SP]);
		int xstep = asFixedPoint((vx_b[X] - vx_a[X]) / dy);
		int zstep = intZMax((vx_b[Z] - vx_a[Z]) / dy);
		// int brstep = asFixedPoint((vx_b[BR] - vx_a[BR]) / dy);
		// int spstep = asFixedPoint((vx_b[SP] - vx_a[SP]) / dy);
		// int tu = asFixedPoint(vx_a[TXU]);
		// int tv = asFixedPoint(vx_a[TXV]);
		// int tustep = getTextureStep(vx_a[TXU], vx_b[TXU], dy);
		// int tvstep = getTextureStep(vx_a[TXV], vx_b[TXV], dy);
		if (ay < 0) {
			// tu += -ay * tustep;
			// tv += -ay * tvstep;
			// br += -ay * brstep;
			// sp += -ay * spstep;
			x += -ay * xstep;
			z += -ay * zstep;
			ay = 0;
		}
		if (by >= ibottom)
			by = ibottom - 1;
		// com.jv.GOutput.println("edge loop");
		for (int y = ay; y <= by; y++) {
			int ix = toNotFP(x);
			// com.jv.GOutput.println("Y:"+y+" ix:"+ix+ " left:"+left_x[y] + "
			// right:"+right_x[y]);
			if (left_x[y] > ix) {
				left_x[y] = ix;
				left_z[y] = z;
				// left_br[y] = br;
				// left_sp[y] = sp;
				// left_tu[y] = tu;
				// left_tv[y] = tv;
			}
			if (right_x[y] < ix) {
				right_x[y] = ix;
				right_z[y] = z;
				// right_br[y] = br;
				// right_sp[y] = sp;
				// right_tu[y] = tu;
				// right_tv[y] = tv;
			}
			// com.jv.GOutput.println("\tY:"+y+" ix:"+ix+ " left:"+left_x[y] + "
			// right:"+right_x[y]);
			x += xstep;
			z += zstep;
			// br += brstep;
			// sp += spstep;
			// tu += tustep;
			// tv += tvstep;
		}
	}

	GBuffer.Segment checkGBuffer(GBuffer.Segment s, int y, int left, int right) {
		final boolean dg = GBuffer.debug;
		if (dg)
			log.debugln("\nleft checkb " + left + " right " + right);
		if (s == null) {
			s = GBuffer.sp.next();
			s.left = left;
			s.right = right;
			if (dg)
				log.debugln("s==null created " + s);
			drawLine(y, left, right);
		} else {
			if (left < s.left) {
				if (right >= s.left) {
					drawLine(y, left, s.left);
					if (dg)
						log.debugln("draw " + left + "-" + s.left);
					s.left = left;
					if (right > s.right) {
						if (s.next != null && right >= s.next.left) {
							log.debugln("this should never happen");
						} else {
							drawLine(y, s.right, right);
							s.right = right;
						}
					}
				} else {
					if (dg)
						log.debugln("add to left");
					GBuffer.Segment add = GBuffer.sp.next();
					add.next = s;
					add.left = left;
					add.right = right;
					drawLine(y, left, right);
					s = add;
				}
			} else {
				if (left > s.right) {
					if (dg)
						log.debugln("add to right");
					s.next = checkGBuffer(s.next, y, left, right);
					if (dg)
						log.debugln("add to right " + s);
				} else {
					if (dg)
						log.debugln("left>=s.left left<=s.right");
					if (right <= s.right) {
						if (dg)
							log
									.debugln("segmentin sis'll;.. ohitetaan");
					} else {
						left = s.right;
						if (s.next == null) {
							if (dg)
								log.debugln("s.next null");
							drawLine(y, left, right);
							s.right = right;
						} else {
							if (right < s.next.left) {
								if (dg)
									System.out
											.println("left segmentin v??liss?? right<s.next.left");
								drawLine(y, left, right);
								s.right = right;
							} else {
								if (dg)
									System.out
											.println("just drawline right >=s.next.left");
								drawLine(y, left, s.next.left);
								s.right = s.next.right;
								GBuffer.Segment nnext = s.next.next;
								GBuffer.sp.release(s.next);
								s.next = nnext;
								;
							}
						}
						// s.next = checkGBuffer(s.next, y, left, right);
					}
				}
			}
		}
		return s;
		/*
		 * GBuffer.Segment nfirst = GBuffer.sp.next(); nfirst.left = left;
		 * if(right <b.first.right) { nfirst.right = b.first.left; nfirst.next =
		 * b.first; b.first = nfirst; } }
		 */
	}

	final private void drawLine(int y, int left, int right) {
		int tu = left_tu[y];
		int tv = left_tv[y];
		int sp = left_sp[y];
		int br = left_br[y];
		final int tua = this.tua;
		final int tva = this.tva;
		final int bra = this.bra;
		final int spa = this.spa;
		if (left < 0) {
			tu += tua * -left;
			tv += tva * -left;
			left = 0;
		}
		int index = y * screen_width + left;
		right -= left;
		while (right-- > 0) {
			try {
				int p = t.getPixel(tu, tv);
				// int r = (p & 0xff0000) >> 16;
				// int g = (p & 0x00ff00) >> 8;
				// int b = (p & 0x0000ff) >> 0;
				// r = ((r * br) / 0xffff) & 0xff;
				// g = ((g * br) / 0xffff) & 0xff;
				// b = ((b * br) / 0xffff) & 0xff;
				// p = 0xff << 24 | r << 16 | g << 8 | b;
				pixels[index++] = p;
				tu += tua;
				tv += tva;
				br += bra;
				sp += spa;
			} catch (Exception e) {
				log.print(e);
			}
		}
	}

	public void fillTriangle(int starty, int endy) {
		if (starty < 0)
			starty = 0;
		if (endy > ibottom)
			endy = ibottom;
		int midy = (endy + starty) / 2;
		int midw = right_x[midy] - left_x[midy];
		if (midw == 0)
			return;
		tua = (right_tu[midy] - left_tu[midy]) / midw;
		tva = (right_tv[midy] - left_tv[midy]) / midw;
		for (int y = starty; y <= endy; y++) {
			int left = left_x[y];
			int w = right_x[y] - left_x[y];
			if (w > 0) {
				int right = right_x[y];
				if (right >= screen_width)
					right = screen_width - 1;
				GBuffer gb = glines[y];
				// GBuffer.debug = (y == 128);
				GBuffer.debug = false;
				if (GBuffer.debug)
					log.debugln("\n\nfillTriangle " + gb.first
							+ " uusi left:" + left + " right:" + right);
				gb.first = checkGBuffer(gb.first, y, left, right);
				if (GBuffer.debug)
					log.debugln(""+gb.first);
				// drawLine(y, left, right);
			}
		}
	}

	public void draw(int offset, int vertex_size, float[] vx_a, float[] vx_b,
			float[] vx_c) {
		if (vx_a[1] > vx_b[1])
			drawEdge(offset, vertex_size, vx_b, vx_a);
		else
			drawEdge(offset, vertex_size, vx_a, vx_b);
		if (vx_a[1] > vx_c[1])
			drawEdge(offset, vertex_size, vx_c, vx_a);
		else
			drawEdge(offset, vertex_size, vx_a, vx_c);
		if (vx_b[1] > vx_c[1])
			drawEdge(offset, vertex_size, vx_c, vx_b);
		else
			drawEdge(offset, vertex_size, vx_b, vx_c);
		// Etsit??????n pienin y
		int top_y = (int) vx_a[1];
		if (top_y > vx_b[1])
			top_y = (int) vx_b[1];
		if (top_y > (int) vx_c[1])
			top_y = (int) vx_c[1];
		// Etsit??????n isoin y
		int bottom_y = (int) vx_a[1];
		if (bottom_y < vx_b[1])
			bottom_y = (int) vx_b[1];
		if (bottom_y < vx_c[1])
			bottom_y = (int) vx_c[1];
		if (top_y < 0)
			top_y = 0;
		// int midy = (top_y + bottom_y) / 2;
		if (top_y < bottom_y && top_y < left_x.length)
			fillTriangle(top_y, bottom_y); // , midy);
		clearLeftRightArrays(top_y, bottom_y);
	}

	public int getTextureStep(float a, float b, int dy) {
		if (!wraptexturecoordinates) {
			return asFixedPoint((b - a) / dy);
		} else {
			while (a > 1)
				a -= 1;
			while (b > 1)
				b -= 1;
			while (a < 0)
				a += 1;
			while (b < 0)
				b += 1;
			if (a > 0.5f && b > 0.5f) {
				return asFixedPoint((b - a) / dy);
			} else if (a <= 0.5f && b <= 0.5f) {
				return asFixedPoint((b - a) / dy);
			} else if (a <= 0.5f && b > 0.5f) {
				float d = b - a;
				if (d > 0.5f)
					return asFixedPoint(-(d - 0.5f) / dy);
				else
					return asFixedPoint(d / dy);
			} else if (a > 0.5f && b <= 0.5f) {
				float d = b - a;
				if (d < -0.5f)
					return asFixedPoint(-(d + 0.5f) / dy);
				else
					return asFixedPoint(d / dy);
			} else
				return 0;
		}
	}

	public RasterizerDefault() {
		setArraySize(1);
	}

	private void setArraySize(int size) {
		left_x = new int[size];
		right_x = new int[size];
		left_z = new int[size];
		right_z = new int[size];
		left_sp = new int[size];
		left_br = new int[size];
		left_tu = new int[size];
		right_tu = new int[size];
		left_tv = new int[size];
		right_tv = new int[size];
		glines = new GBuffer[size];
		for (int i = 0; i < size; i++)
			glines[i] = new GBuffer();
	}

	public void setup(int pixels[], int w, int h) {
		this.pixels = pixels;
		screen_width = w;
		screen_height = h;
		setupLeftRightArrays(w, h);
		ibottom = h - 1;
		startFrame();
	}

	public void clearLeftRightArrays(int starty, int endy) {
		if (starty < 0)
			starty = 0;
		if (endy >= screen_height)
			endy = screen_height - 1;
		for (int i = starty; i <= endy; i++) {
			left_x[i] = screen_width - 1;
			right_x[i] = 0;
		}
	}

	public void setupLeftRightArrays(int w, int h) {
		setupLeftRightArrays(w, h, 0, h);
	}

	public void setupLeftRightArrays(int w, int h, int starty, int endy) {
		if (h != left_x.length) {
			setArraySize(h);
		}
		for (int i = starty; i < endy; i++) {
			left_x[i] = w - 1;
			right_x[i] = 0;
		}
	}

	public void setTexture(STexture nt) {
		t = nt;
	}

	public void setBumbmap(STexture t) {
		// TODO Auto-generated method stub
	}

	public void setLightmap(STexture t) {
		// TODO Auto-generated method stub
	}

	public boolean bumbmappingEnabled() {
		return false;
	}

	public boolean colorsEnabled() {
		return false;
	}

	public boolean shadingEnabled() {
		return true;
	}

	public boolean envmappingEnabled() {
		return false;
	}

	public boolean texturingEnabled() {
		return true;
	}
}