/*
 * Created on 11.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package m65eII.soft;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import m65eII.LoggerFactory;
import m65eII.engine.ColorList;
import m65eII.engine.Material;
import m65eII.engine.Matrix;
import m65eII.engine.P;
import m65eII.engine.PointList;
import m65eII.engine.RenderTarget;
import m65eII.engine.TimeMeasure;
import m65eII.engine.V;
import m65eII.engine.VectorList;
import m65eII.tr.TFrameDrawer;
import m65eII.tr.TLight;
import m65eII.tr.TTransformation;
import m65eII.tr.TTriangleList;
import m65eII.tr.TViewPort;

/**
 * @author juuso
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class SFrameDrawer implements TFrameDrawer {
	// private SPixelImage renderTarget;
	private Matrix m_view;
	private VectorList vl;
	private TLight lights[] = new TLight[30];
	private TViewPort viewport = new TViewPort();
	private static TimeMeasure tm = new TimeMeasure("SFrameDrawer");
	static private Rasterizer ras = new RasterizerDefault();
	private static SPolyList pl = new SPolyList();
	private STexturePool tpool;
	private PointList txl;

	/**
	 * 
	 */
	public SFrameDrawer() {
		tm.setTotalStartTime();
	}

	public void setColorList(ColorList cl) {
		// TODO Auto-generated method stub	
	}
	
	public void finish() {
		
		tm.setStartTime("pl.sort");
		pl.sort();
		/*
		 * GOutput.push(this, "finish pl.sort"); for (int i = 0; i <
		 * pl.poly_index; i++) { STriangle t = pl.tris[i]; GOutput.println("" +
		 * t.z); } GOutput.pop();
		 */
		tm.setEndTime("pl.sort");
		tm.setStartTime("finish.draw");
		for (int i = 0; i < pl.poly_index; i++) {
			STriangle tri = pl.tris[i];
			ras.draw(0, tri.vertexsize, tri.afs, tri.bfs, tri.cfs);
		}
		tm.setEndTime("finish.draw");
		tm.setStartTime("pl.reset");
		pl.reset();
		tm.setEndTime("pl.reset");
		tm.setTotalEndTime();
	}

	public void drawTriangleList(TTriangleList tl) {
		tm.setStartTime("drawTriangleList");
		final V va = new V();
		final V vb = new V();
		final V vc = new V();
		final V trin = new V();
		final P txa = new P();
		final P txb = new P();
		final P txc = new P();
		int size = tl.size();
		int tris[] = tl.getTris();
		V ns[] = tl.getNs();
		final Matrix m_view1 = this.m_view;
		// STriangle tri = new STriangle();
		final boolean vx_colors = ras.colorsEnabled(); // do we need
		// vertexcolors
		final boolean vx_txs = ras.texturingEnabled(); // do we need
		// texturecoordinates
		final boolean vx_shs = ras.shadingEnabled(); // do we need brightness
		// and specular shaind per
		// vertex
		for (int i = 0; i < size; i += 3) {
			trin.copy(ns[i / 3]);
			m_view1.multw0(trin);
			if (trin.z < 0 || trin.z > 0) {
				STriangle tri = pl.getNext();
				int a_if = 0;
				int b_if = 0;
				int c_if = 0;
				vl.get(tris[i + 0], va);
				vl.get(tris[i + 1], vb);
				vl.get(tris[i + 2], vc);
				int il = 0;
				TLight light;
				float ld = 0; // se toinen valaistus
				float ls = 0; // specular valaistus
				while ((light = lights[il++]) != null) {
					// tri.
				}
				m_view1.multw1(va);
				m_view1.multw1(vb);
				m_view1.multw1(vc);
				// TODO clipping ja muut puuttuu kokonaan
				viewport.screenTransform(va);
				viewport.screenTransform(vb);
				viewport.screenTransform(vc);
				tri.z = -(va.z + vb.z + vc.z);
				tri.afs[a_if++] = va.x;
				tri.afs[a_if++] = va.y;
				tri.afs[a_if++] = va.z;
				tri.bfs[b_if++] = vb.x;
				tri.bfs[b_if++] = vb.y;
				tri.bfs[b_if++] = vb.z;
				tri.cfs[c_if++] = vc.x;
				tri.cfs[c_if++] = vc.y;
				tri.cfs[c_if++] = vc.z;
				if (vx_colors) {
					tri.afs[a_if++] = (trin.x + 1) / 2; // acr
					tri.afs[a_if++] = (trin.y + 1) / 2;
					tri.afs[a_if++] = (trin.z + 1) / 2;
					tri.bfs[b_if++] = (trin.x + 1) / 2; // bcr
					tri.bfs[b_if++] = (trin.y + 1) / 2;
					tri.bfs[b_if++] = (trin.z + 1) / 2;
					tri.cfs[c_if++] = (trin.x + 1) / 2; // ccr
					tri.cfs[c_if++] = (trin.y + 1) / 2;
					tri.cfs[c_if++] = (trin.z + 1) / 2;
				}
				if (vx_shs) {
					tri.afs[a_if++] = (trin.x + 1) / 2; // abr
					tri.afs[a_if++] = (trin.y + 1) / 2; // asp
					tri.bfs[b_if++] = (trin.x + 1) / 2;
					tri.bfs[b_if++] = (trin.y + 1) / 2;
					tri.cfs[c_if++] = (trin.x + 1) / 2;
					tri.cfs[c_if++] = (trin.y + 1) / 2;
				}
				if (vx_txs) {
					txl.get(tris[i + 0], txa);
					txl.get(tris[i + 1], txb);
					txl.get(tris[i + 2], txc);
					tri.afs[a_if++] = txa.u;
					tri.afs[a_if++] = txa.v;
					tri.bfs[b_if++] = txb.u;
					tri.bfs[b_if++] = txb.v;
					tri.cfs[c_if++] = txc.u;
					tri.cfs[c_if++] = txc.v;
				}
				/*
				 * // TODO piirret????n normaalit. debug va.copy(ns[i / 3]);
				 * vl.getVector(tris[i + 1], vb);
				 * 
				 * va.mult(-0.15f); va.add(vb); vc.copy(vb); vc.x += 0.1f;
				 * 
				 * m_view.multw1(va); m_view.multw1(vb); m_view.multw1(vc); //
				 * TODO clipping ja muut puuttuu kokonaan
				 * viewport.screenTransform(va); viewport.screenTransform(vb);
				 * viewport.screenTransform(vc);
				 * 
				 * pl.add(va.x, va.y, vb.x, vb.y, vc.x, vc.y, 0.8f, 0.3f, 0.8f,
				 * va.z + vb.z + vc.z);
				 */
			}
		}
		tm.setEndTime("drawTriangleList");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.tr.TFrameDrawer#setMaterial(m65eII.engine.Material)
	 */
	public void setMaterial(Material mat) {
		tm.setStartTime("setMaterial_getTexture");
		String smat = mat.getTexture();
		tm.setEndTime("setMaterial_getTexture");
		tm.setStartTime("setMaterial_tpool_getTexture");
		STexture t = tpool.getTexture(smat);
		tm.setEndTime("setMaterial_tpool_getTexture");
		tm.setStartTime("setMaterial_ras.setMat");
		ras.setTexture(t);
		tm.setEndTime("setMaterial_ras.setMat");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.tr.TFrameDrawer#setTransformation(m65eII.tr.TTransformation)
	 */
	public void setTransformation(TTransformation tr) {
		tr.setupViewport(viewport);
		m_view = tr.getViewTransformation();
	}

	public void setTexturePool(STexturePool npool) {
		tpool = npool;
	}

	public void setVectorList(VectorList vl) {
		this.vl = vl;
	}

	public void setTexturePointList(PointList ntxl) {
		txl = ntxl;
	}

	public void setNormalList(VectorList nl) {
		// TODO
	}

	/**
	 * @param rendertarger
	 *            The rendertarger to set.
	 */
	public void setRenderTarget(RenderTarget rendertarger) {
		tm.setStartTime("setrendertarget");
		ViewRenderTarget vrt = (ViewRenderTarget) rendertarger;
		// this.renderTarget = (ViewRenderTarget) rendertarger;
		viewport.setSize(rendertarger.getWidth(), rendertarger.getHeight());
		vrt.setup(ras);
		tm.setEndTime("setrendertarget");
	}

	final static private class SPolyList {
		STriangle tris[] = new STriangle[0];
		int poly_index;
		final static int A = 0;
		final static int B = 2;
		final static int C = 4;
		final static int COLOR = 6;
		private final static boolean debug = false;
		static ByteArrayOutputStream baos = new ByteArrayOutputStream();
		static PrintStream out = new PrintStream(baos);
		private static final int ADDSORT_RAJA = 12;

		/**
		 * 
		 */
		private SPolyList() {
			grow(1000);
		}

		final void qsort(final int low, final int high) {
			/*
			 * depth++; if (depth > 200) { com.jv.GOutput.println(baos);
			 * baos.reset(); return; }
			 */
			if (low + ADDSORT_RAJA <= high) {
				final STriangle tris1[] = this.tris; // speedup
				final int pivot = (high + low) / 2;
				final float pivot_z = tris1[pivot].z;
				if (debug)
					print(low, high);
				if (debug)
					out.println("pivot ipivot:" + pivot + " z " + pivot_z);
				int left = low;
				int right = high;
				do {
					while (tris1[left].z < pivot_z)
						left++;
					while (tris1[right].z > pivot_z)
						right--;
					if (left <= right) { // swap
						if (debug)
							out.println("swap l:" + left + " r:" + right);
						final STriangle tmp_tri = tris1[left];
						tris1[left] = tris1[right];
						tris1[right] = tmp_tri;
						left++;
						right--;
					}
				} while (left < right);
				// order[right + 0] = pivot_index;
				// order[right + 1] = pivot_z;
				// com.jv.GOutput.println("right " + right);
				if (debug)
					print(low, high);
				if (debug)
					out.println("sort " + left + "=" + right);
				if (low < right)
					qsort(low, right);
				if (left < high)
					qsort(left, high);
			} else {
				addSort(low, high);
			}
			// depth--;
		}

		// Wiklan muistiin panoista... weissin
		final private void addSort(int vasen, int oikea) {
			for (int i = vasen + 1; i <= oikea; ++i) {
				final STriangle apu = tris[i];
				int j;
				for (j = i; j > vasen && apu.z < tris[j - 1].z; --j)
					tris[j] = tris[j - 1];
				tris[j] = apu;
			}
		}

		/*
		 * Wikla(Tm) private static void pikaJ??rjest??(int[] taulu, int alku,
		 * int loppu) {
		 * 
		 * int jakoAlkio, vasen, oikea;
		 * 
		 * vasen = alku; oikea = loppu;
		 * 
		 * jakoAlkio = taulu[(alku+loppu)/2]; do { while (taulu[vasen]
		 * <jakoAlkio) ++vasen; while (jakoAlkio <taulu[oikea]) --oikea; if
		 * (vasen <=oikea) { int apu = taulu[vasen]; taulu[vasen] =
		 * taulu[oikea]; taulu[oikea] = apu; ++vasen; --oikea; } } while (vasen
		 * <oikea);
		 * 
		 * if (alku < oikea) pikaJ??rjest??(taulu, alku, oikea); if (vasen <
		 * loppu) pikaJ??rjest??(taulu, vasen, loppu); }
		 */
		final void sort() {
			if (debug)
				print(0, poly_index - 1);
			qsort(0, poly_index - 1);
			if (debug)
				print(0, poly_index - 1);
		}

		void print(int low, int high) {
			out.print("low:" + low + " high:" + high + ":");
			for (int i = low; i <= high; i++) {
				// String s = " "+i+":" + (int)(1000*tris[i].z);
				String s = " " + i + ":" + tris[i].z;
				while (s.length() < 4)
					s = " " + s;
				out.print(s);
			}
			out.println("");
		}

		STriangle getNext() {
			if (poly_index + 1 >= tris.length)
				grow();
			return tris[poly_index++];
		}

		private void grow() {
			int nsize = (int) (tris.length * 1.1f);
			grow(nsize);
		}

		private void grow(int nsize) {
			LoggerFactory.createLogger(this).debugln(
					"APolyList grow " + tris.length + " to " + nsize);
			STriangle ntris[] = new STriangle[nsize];
			System.arraycopy(tris, 0, ntris, 0, tris.length);
			for (int i = tris.length; i < ntris.length; i++) {
				ntris[i] = new STriangle();
			}
			tris = ntris;
		}

		void reset() {
			baos.reset();
			poly_index = 0;
		}
	}
}