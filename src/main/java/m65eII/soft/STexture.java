/*
 * Created on 11.10.2004
 *
 */
package m65eII.soft;

import java.awt.Image;
import java.awt.image.ImageObserver;
import java.awt.image.PixelGrabber;

import m65eII.Logger;
import m65eII.LoggerFactory;
import m65eII.engine.RenderTarget;

/**
 * @author juuso
 * 
 */
public class STexture implements RenderTarget {
	private int w, h;
	private int pixels[] = new int[256 * 256];
	private Logger log = LoggerFactory.createLogger(this);

	/**
	 * 
	 */
	public STexture(Image img) {
		w = img.getWidth(null);
		h = img.getHeight(null);
		pixels = new int[w * h];
		PixelGrabber pg = new PixelGrabber(img, 0, 0, w, h, pixels, 0, w);
		try {
			pg.grabPixels();
		} catch (InterruptedException e) {
			log.debugln("interrupted waiting for pixels!");
			return;
		}
		if ((pg.getStatus() & ImageObserver.ABORT) != 0) {
			log.debugln("image fetch aborted or errored");
			return;
		}
	}

	public int getPixel(int u, int v) {
		return 0xff << 24 | pixels[u & 0xff00 | (v >> 8) & 0xff];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.RenderTarget#getWidth()
	 */
	public int getWidth() {
		return w;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see m65eII.engine.RenderTarget#getHeight()
	 */
	public int getHeight() {
		return h;
	}
}