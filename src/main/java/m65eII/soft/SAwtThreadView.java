package m65eII.soft;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;

import m65eII.Logger;
import m65eII.LoggerFactory;

import com.jv.pixelgr.SCanvasMouseListener;
import com.jv.pixelgr.SPixelImage;

public class SAwtThreadView implements SView {
	private Runnable updater;
	private Logger log = LoggerFactory.createLogger(this);
	private boolean stopped;
	private Canvas canvas;
	private long st = System.currentTimeMillis(), framecount;
	private SPixelImage pimg;
	private boolean doclear;

	void updateFrameCount() {
		framecount++;
	}

	String getFrameString() {
		long dt = System.currentTimeMillis() - st;
		String s;
		if (dt > 5000) {
			framecount = 0;
			st = System.currentTimeMillis();
			dt = 0;
		}
		if (dt > 0) {
			s = "f:" + framecount + " hz:" + (float) framecount * 1000.0f / (dt);
		} else {
			s = "dt:0";
		}
		return s;
	}

	SAwtThreadView() {
		canvas = new SCanvas();
		canvas.setBackground(Color.WHITE);
		pimg = new SPixelImage();
		pimg.setComponent(canvas);
	}

	public void setup(Rasterizer ras) {
		ras.setup(pimg.getPixels(), pimg.getWidth(), pimg.getHeight());
	}

	public int getHeight() {
		return pimg.getHeight();
	}

	public int getWidth() {
		return pimg.getWidth();
	}

	public void repaint() {
		canvas.repaint();
	}

	public void setUpdater(Runnable runnable) {
		this.updater = runnable;
	}

	public void addMouseListener(SCanvasMouseListener l) {
	// TODO Auto-generated method stub
	}

	public void attachTo(Container cont) {
		cont.add(canvas);
		canvas.validate();
		canvas.invalidate();
	}

	public void endRender() {
	// ei tarvita
	}

	public boolean isVisible() {
		return canvas.isVisible();
	}

	public void setClear(boolean b) {
	// TODO Auto-generated method stub
	}

	public void startRender() {
		pimg.setSize(canvas.getSize().width, canvas.getSize().height);
		if (doclear)
			pimg.clear();
	}

	public void stop() {
		stopped = true;
	}

	private class SCanvas extends Canvas {
		private static final long serialVersionUID = 1L;

		public void update(Graphics g) {
			framecount++;
			updater.run();
			Image img = pimg.getImage();
			if (img != null) {
				g.drawImage(img, 0, 0, null);
				g.setColor(Color.GREEN);
				g.drawString(getFrameString(), 0, 20);
			} else {
				log.debugln("Rendertarget img null");
				synchronized (pimg) {
					try {
						pimg.wait(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			if (!stopped)
				repaint();
		}

		public void paint(Graphics g) {
			repaint();
		}
	}
}
