/*
 * Created on 11.6.2005
 */
package m65eII.soft;

import m65eII.engine.RenderTarget;

public class ViewRenderTarget implements RenderTarget {
	private SView view;

	public ViewRenderTarget(SView view) {
		this.view = view;
	}

	public int getWidth() {
		return view.getWidth();
	}

	public int getHeight() {
		return view.getHeight();
	}

	public void setup(Rasterizer ras) {
		view.setup(ras);
	}
}
