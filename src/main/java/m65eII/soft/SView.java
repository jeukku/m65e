package m65eII.soft;

import m65eII.engine.View;

public interface SView extends View {

	void setup(Rasterizer ras);

}
