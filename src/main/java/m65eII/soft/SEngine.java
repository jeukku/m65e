/*
 * Created on 11.10.2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package m65eII.soft;

import java.util.Hashtable;

import m65eII.LoggerFactory;
import m65eII.awt.EngineRunner;
import m65eII.awt.PaintThreadEngineRunner;
import m65eII.engine.Engine;
import m65eII.engine.Resources3D;
import m65eII.engine.Scene;
import m65eII.engine.TimeMeasure;
import m65eII.engine.TimeStep;
import m65eII.engine.TimeStepper;
import m65eII.engine.Updaters;
import m65eII.engine.View;
import m65eII.tr.TScene;

/**
 * @author juuso
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class SEngine implements Engine {
	private Hashtable scenes = new Hashtable();
	private TScene current_scene;
	private TimeMeasure tm = new TimeMeasure(getClass().getName());
	private boolean enabled = true;
	private SView view;
	private STexturePool tpool;
	private EngineRunner erunner;
	private Updaters upd = new Updaters();

	public SEngine() {
		view  = new SAwtThreadView();		
	}
	
	public void start() {
		//erunner = new ThreadedEngineRunner(this, view);
		erunner = new PaintThreadEngineRunner(this, view);
		erunner.start();
	}
	
	public void addUpdater(TimeStepper s) {
		upd.addUpdater(s);	
	}
	
	public void update(TimeStep step) {
		upd.update(step);
	}

	public void setImagesResources(Resources3D res) {
		tpool = new STexturePool(res);
	}
	
	public Scene addScene(String id) {
		TScene scene = new TScene(id);
		scenes.put(scene, id);
		if (current_scene == null) {
			current_scene = scene;
		}
		return scene;
	}

	public Scene selectScene(String id) {
		current_scene = (TScene) scenes.get(id);
		return current_scene;
	}

	public View getView() {
		return view;
	}

	public void frame() {
		tm.setTotalStartTime();
		if (view.isVisible() && enabled) {
			try {
				tm.setStartTime("startrender");
				getView().startRender();
				tm.setEndTime("startrender");
				tm.setStartTime("draw");
				draw(current_scene);
				tm.setEndTime("draw");
				tm.setStartTime("endrender");
				getView().endRender();
				tm.setEndTime("endrender");
			} catch (Exception e) {
				LoggerFactory.createLogger(this).print(e);
			}
		}
		tm.setTotalEndTime();
	}

	/**
	 * Luo framedrawring, tekee osansa sen asetuksien asettamiseen ja antaa
	 * scene:n jatkaa.
	 * 
	 * @param scene
	 */
	private void draw(TScene scene) {
		SFrameDrawer fd = new SFrameDrawer();
		fd.setRenderTarget(new ViewRenderTarget(view));
		fd.setTexturePool(tpool);
		scene.draw(fd);
		fd.finish();
	}

	public void stop() {
		enabled = false;
		erunner.stop();
	}
}
