/*
 * Created on 12.3.2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package m65eII.soft;

/**
 * @author juuso
 * 
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public interface Rasterizer {
	void setup(int pixels[], int w, int h);

	void setTexture(STexture t);

	void setBumbmap(STexture t);

	void setLightmap(STexture t);

	boolean colorsEnabled();

	boolean texturingEnabled();

	boolean bumbmappingEnabled();

	boolean envmappingEnabled();

	boolean shadingEnabled();

	void draw(int offset, int vertex_size, float vx_a[], float vx_b[],
			float vx_c[]);
}
