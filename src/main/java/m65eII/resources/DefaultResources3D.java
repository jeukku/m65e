package m65eII.resources;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

import m65eII.Logger;
import m65eII.LoggerFactory;
import m65eII.engine.Resources3D;
import m65eII.engine.TransformationNode;

import com.jv.resources.Resources;
import com.jv.resources.ResourcesInputStreamFactory;
import com.jv.resources.URLResourceInputStreamFactory;

public class DefaultResources3D implements Resources3D {
	private Hashtable nodes = new Hashtable();

	private Hashtable loaders = new Hashtable();

	private Hashtable objects = new Hashtable();

	// private Hashtable apps = new Hashtable();

	private Logger log = LoggerFactory.createLogger(this);

	private Resources res_;

	private Vector geometryloaders;

	// public DefaultResources3D() {
	// this(null);
	// }

	public DefaultResources3D(java.net.URL baseurl) {
		Resources res = new Resources();
		ResourcesInputStreamFactory ris = new URLResourceInputStreamFactory(baseurl);
		res.setInputStreamFactory(ris);
		init(res);
	}

	public DefaultResources3D(URL codeBase, String string) throws MalformedURLException {
		this(new URL(codeBase, string));
	}

	public DefaultResources3D(Resources resources) {
		init(resources);
	}

	private void init(Resources resources) {
		res_ = resources;
		geometryloaders = new Vector();
		geometryloaders.add(new GeometryLoaderFactoryWavefrontObj());
	}

	public InputStream getBufferStream(String string) throws IOException {
		return res_.getBufferStream(string);
	}

	public GeometryLoader getObject(InputStream is, String name) throws IOException {
		loadObject3D(is, name);
		return (GeometryLoader) loaders.get(name);
	}

	public GeometryLoader getObject(String name) {
		GeometryLoader gl = null;
		synchronized (objects) {
			if (objects.get(name) == null) {
				log.debugln(getClass().getName() + " getObject " + name);
				try {
					InputStream is = new BufferedInputStream(getResources().getBufferStream("objects/" + name));
					gl = getObject(is, name);
				} catch (IOException ioe) {
					log.print(ioe);
					gl = null;
				}
			}
		}
		return gl;
	}

	public TransformationNode getTransformationNode(String name) {
		if (nodes.get(name) == null) {
			try {
				loadNode(getResources().getBufferStream(name), name);
			} catch (IOException ioe) {
				return null;
			}
		}
		return (TransformationNode) nodes.get(name);
	}

	public Properties getIni(String string) {
		return res_.getIni(string);
	}

	private Resources getResources() {
		if (res_ == null) {
			throw new RuntimeException("Resources not loaded");
		}
		return res_;
	}

	private void loadObject3D(InputStream is, String name) throws IOException {
		GeometryLoader loader = null;
		Iterator i = geometryloaders.iterator();
		while (i.hasNext()) {
			GeometryLoaderFactory gm = (GeometryLoaderFactory) i.next();
			GeometryLoader gl = gm.newLoader(is);
			if (gl.isOk()) {
				loader = gl;
				break;
			}
		}

		loaders.put(name, loader);
	}

	private void loadNode(InputStream is, String name) throws IOException {
		// TODO
	}

	// private GeometryLoader read(InputStream is, GeometryLoader gl)
	// throws IOException {
	// gl.read(is);
	// return gl;
	// }

	/*
	 * private TransformationNode readWRL(InputStream is) throws IOException {
	 * m65e.io.LoadWRL loader = new m65e.io.LoadWRL();
	 * 
	 * loader.read(new InputStreamReader(is)); TransformationNode snode =
	 * loader.getTransformationNode();
	 * 
	 * if (snode.getChildCount() > 1) { com.jv.GOutput.println("Resources
	 * getObject3DFromWRL shapenode contains " + snode.getChildCount() + "
	 * shapes... picking up first"); }
	 * 
	 * return snode; }
	 * 
	 * 
	 * private TransformationNode importLWO(InputStream is) throws IOException {
	 * m65e.io.ImportLWO loader = new m65e.io.ImportLWO(); TransformationNode n =
	 * loader.load(is); return n; }
	 * 
	 * private TransformationNode import3DS(InputStream is, long length) throws
	 * IOException { m65e.io.Import3DS loader = new m65e.io.Import3DS();
	 * loader.addMessageListener(new com.jv.utils.MessageListener() { public
	 * void messageChanged(com.jv.utils.MessageEvent e) { fireMessageChanged(new
	 * com.jv.utils.MessageEvent(this, "importing 3DS " + e.getMessage())); }
	 * 
	 * public void progressChanged(int current, int min, int max) {
	 * fireProgressChanged(current, min, max); } });
	 * 
	 * TransformationNode n = loader.load(is, length); return n; }
	 */
	// private byte[] readBuffer(InputStream is) throws IOException {
	// byte buffer[] = new byte[1024];
	// byte ret[] = new byte[0];
	// while (true) {
	// int read = is.read(buffer, 0, 1024);
	// if (read == -1)
	// break;
	// byte nret[] = new byte[ret.length + read];
	// System.arraycopy(ret, 0, nret, 0, ret.length);
	// System.arraycopy(buffer, 0, nret, ret.length, read);
	// ret = nret;
	// }
	// return ret;
	// }
	public synchronized Image getImage(String name) {
		log.debugln("**************** getTexture *");
		java.awt.Image img = getResources().getImage("images/" + name);
		if (img == null)
			throw new RuntimeException("Resource3D get null image " + name);
		while (img.getWidth(null) == -1 || img.getHeight(null) == -1) {
			log.debugln("waiting for texture image " + name);
			for (int i = 0; i < 50; i++)
				try {
					wait(20);
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
		}
		return img;
	}
}