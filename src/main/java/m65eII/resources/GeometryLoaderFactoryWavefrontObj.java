package m65eII.resources;

import java.io.IOException;
import java.io.InputStream;

import m65eII.importers.WavefrontObj;

public class GeometryLoaderFactoryWavefrontObj implements GeometryLoaderFactory {
	public GeometryLoader newLoader(InputStream is) {
		try {
			WavefrontObj w = new WavefrontObj();
			w.read(is);
			return w;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
