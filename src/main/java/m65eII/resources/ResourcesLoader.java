package m65eII.resources;

import java.net.URL;
import java.util.Enumeration;

import m65eII.Logger;
import m65eII.LoggerFactory;

public class ResourcesLoader {
	private java.util.Hashtable resourcesfiles = new java.util.Hashtable();
	private java.net.URL codebase;
	private String resourcesbase;
	// private java.awt.Component comp;
	private Logger log = LoggerFactory.createLogger(this);

	public ResourcesLoader(String nresourcesbase) {
		this.resourcesbase = nresourcesbase;
		log.debugln("ResourcesLoader() resourcebase " + resourcesbase);
	}

	public ResourcesLoader(java.net.URL codebase, String nresourcesbase) {
		this.codebase = codebase;
		this.resourcesbase = nresourcesbase;
		log.debugln("ResourcesLoader() res " + codebase);
		log.debugln("ResourcesLoader() resourcebase " + resourcesbase);
	}

	/*
	 * public void setComponent(java.awt.Component ncomp) { this.comp = ncomp; }
	 */

	public Enumeration getResourcesKeys() {
		return resourcesfiles.keys();
	}

	public void removeResources(String file) {
		resourcesfiles.remove(file);
	}

	public DefaultResources3D getResources(String file) {
		return getResources(file, null);
	}

	public DefaultResources3D getResources(String file, com.jv.utils.MessageListener listener) {
		try {
			DefaultResources3D res = (DefaultResources3D) resourcesfiles.get(file);
			if (res != null)
				return res;

			log.debugln("ResourcesLoader3D getResources " + file);
			log.debugln("ResourcesLoader3D codebase " + codebase);
			// res.setComponent(comp);
			if (codebase == null) {
				res = new DefaultResources3D(new URL(resourcesbase + "/" + file));
			} else {
				res = new DefaultResources3D((URL) null);
				//
				java.net.URL resurl = new java.net.URL(codebase, resourcesbase + "/" + file + ".zip");
				log.debugln("getResources cb   " + codebase);
				log.debugln("getResources file " + file);
				log.debugln("getResources url  " + resurl);
				resurl = new java.net.URL(codebase, resourcesbase + "/" + file);
				res = new DefaultResources3D(resurl);
				log.debugln("res " + res);
			}

			resourcesfiles.put(file, res);

			return res;
		} catch (Exception e) {
			log.debugln("com.jv.io.Resources getResources " + file + " " + e);
			log.print(e);
			return null;
		}
	}
}
