package m65eII.resources;

import java.io.InputStream;

public interface GeometryLoaderFactory {

	GeometryLoader newLoader(InputStream is);

}
