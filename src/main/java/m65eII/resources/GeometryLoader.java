package m65eII.resources;

import m65eII.engine.TransformationNode;

public interface GeometryLoader {
    public void read(java.io.InputStream is) throws java.io.IOException;
    public TransformationNode attach(TransformationNode node);
	public boolean isOk();
}



