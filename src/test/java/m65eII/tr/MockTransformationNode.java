package m65eII.tr;

import m65eII.engine.Light;
import m65eII.engine.Matrix;
import m65eII.engine.Object3D;
import m65eII.engine.TransformationNode;

public class MockTransformationNode implements TransformationNode {
	private int objectcount;

	public Light addLight() {
		return null;
	}

	public TransformationNode addNode() {
		return this;
	}

	public Object3D addObject3D() {
		objectcount++;
		return new MockObject3D();
	}

	public void copyTo(TransformationNode node) {
		// mock
	}

	public Object3D findObject(String name) {
		return null;
	}

	public TransformationNode getParent() {
		return null;
	}

	public void setTransformation(Matrix m) {
		// mock
	}

	public int getObjectCount() {
		return objectcount;
	}
}
