package m65e.bsp;

import java.util.Iterator;

import junit.framework.TestCase;
import m65eII.awt.AEngine;
import m65eII.engine.Engine;
import m65eII.engine.Object3D;
import m65eII.engine.Scene;
import m65eII.engine.TriangleList;
import m65eII.engine.bsp.BSP;

public class TestBSP extends TestCase {
	private Engine e;
	private Scene s;
	private BSP bsp;

	protected void setUp() throws Exception {
		e = new AEngine();
		s = e.addScene("def");
		bsp = s.addBSP();
	}

	public void testHeight() {
		Object3D o = s.getBaseNode().addObject3D();
		bsp.add(o);
		Iterator i = o.triangleLists();
		while (i.hasNext()) {
			TriangleList tl = (TriangleList) i.next();
			int h = bsp.getBaseNode().getHeight();
			assertEquals(tl.size(), h);
		}
	}
}
